using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SeasonPassController : IShopItem
{
    [SerializeField] Image chestIcon;
    [SerializeField] TMP_Text title;

    [HideInInspector] public Sprite iconSprite;
    [HideInInspector] public float price;

    public override void OnClickBuy()
    {
        SoundController.Instance.PlayClickButton();
    }
}
