using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class RefreshButton : MonoBehaviour
{
    [SerializeField] protected Button refreshButton;
    public Button RefreshButtonn
    {
        get => refreshButton;
        set => refreshButton = value;
    }
    [SerializeField] protected TextMeshProUGUI refreshCount;
    [SerializeField] protected Image addIcon;
    [SerializeField] ItemBooster_GamePlay itemBooster_GamePlay;
    public UnityAction buttonAction;
    public ItemBooster_GamePlay ItemBooster_GamePlay => itemBooster_GamePlay;
    private void OnEnable()
    {
        UpdateCountBooster();
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdateCountBooster();
        refreshButton.onClick.AddListener(() =>
        {
            buttonAction?.Invoke();
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UpdateCountBooster()
    {
        if (itemBooster_GamePlay && !itemBooster_GamePlay.isLock)
        {
            int numberBooster = UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.Refresh);
            if (numberBooster > 0)
            {
                refreshCount.gameObject.SetActive(true);
                refreshCount.text = numberBooster.ToString();
                addIcon.gameObject.SetActive(false);
            }
            else
            {
                refreshCount.gameObject.SetActive(false);
                addIcon.gameObject.SetActive(true);
            }
        }
        else
        {
            refreshCount.gameObject.SetActive(false);
            addIcon.gameObject.SetActive(false);
        }
    }

    public void UpdateUIRefresh()
    {
        int numberBooster = UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.Refresh);
        if (numberBooster > 0)
        {
            refreshCount.gameObject.SetActive(true);
            refreshCount.text = numberBooster.ToString();
            addIcon.gameObject.SetActive(false);
        }
        else
        {
            refreshCount.gameObject.SetActive(false);
            addIcon.gameObject.SetActive(true);
        }
    }
}
