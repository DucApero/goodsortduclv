﻿public class DataController : Singleton<DataController>
{
    private ItemSkinVO item_skin_VO;
    public ItemSkinVO itemSkinVO
    {
        get
        {
            if (item_skin_VO == null)
                item_skin_VO = new ItemSkinVO();
            return item_skin_VO;
        }
    }

}
