using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StarHub : MonoBehaviour
{
    [SerializeField] bool isGamePlay = false;
    [SerializeField] private TextMeshProUGUI starNumberText;
    private int currentStarNumber = 0;
    public int CurrenStarNumber
    {
        get => currentStarNumber;
        set => currentStarNumber = value;
    }
    private int countStarEarn = 0;
    private void Awake()
    {
        Observer.Instance.AddObserver(ObserverKey.UpdateStar, UpdateStar);
    }
    private void OnDestroy()
    {
        Observer.Instance.RemoveObserver(ObserverKey.UpdateStar, UpdateStar);
    }
    private void OnEnable()
    {
        UpdateCurrentStar();
    }
    public void UpdateStar(object data)
    {
        UpdateCurrentStar();
    }

    public void UpdateCurrentStar()
    {
        if (!isGamePlay)
        {
            currentStarNumber = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Star);
        }
        else
        {
            currentStarNumber = 0;
        }
        SetTextStar();
    }


    private void SetTextStar()
    {
        starNumberText.text = currentStarNumber.ToString();
    }
    public void AddStarFake(int amount)
    {
        if (!isCountdown && this.gameObject.active)
        {
            isCountdown = true;
            StartCoroutine(DelayCount(timeDelay));
        }
       
    }
    private Coroutine coroutine;
    private float timeDelay = 0.1f;
    private bool isCountdown = false;
    private IEnumerator DelayCount(float time)
    {
        if (currentStarNumber < (LevelManager.Instance.CurrentStarEarn))
        {
            currentStarNumber++;
            SetTextStar();
            yield return new WaitForSeconds(time/ LevelManager.Instance.CurrentStarEarn);
            StartCoroutine(DelayCount(timeDelay));
        }
        else
        {
            isCountdown = false;
            yield break;
        }
        
    }
}
