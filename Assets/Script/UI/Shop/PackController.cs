using CommonEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static ShopQuyNX.ShopItemData;

public class PackController : IShopItem
{
    [SerializeField] TMP_Text titleTxt, priceTxt, goldAmountTxt, countDown;
    [SerializeField] Image adIcon;
    [SerializeField] Transform itemGroup;
    [SerializeField] GameObject itemPrefab;
    [SerializeField] Button buyButton;

    [HideInInspector] public bool isLimited;
    [HideInInspector] public string title;
    [HideInInspector] public ItemShop[] items;
    [HideInInspector] public int goldAmount;


    void Start()
    {
        titleTxt.text = title;
        goldAmountTxt.text = "x " + goldAmount;

        if (isIAP)
        {
            countDown.gameObject.SetActive(false);
            priceTxt.gameObject.SetActive(true);
            priceTxt.text = priceIAP + " $";

            adIcon.gameObject.SetActive(false);
        }
        else
        {
            if (IsReadyToBuy()) return;
            buyButton.interactable = false;
        }

        SetDataListItem();
    }

    void SetDataListItem()
    {
        foreach(ItemShop item in items)
        {
            GameObject itemChild = Instantiate(itemPrefab, itemGroup);
            ItemInListController itemInListController = itemChild.GetComponent<ItemInListController>();
            itemInListController.id = item.id;
            itemInListController.amount = item.amountText;
        }
    }

    public override void OnClickBuy()
    {
        if (!isIAP && !isReadyToBuy) return;
        SoundController.Instance.PlayClickButton();
        UserDatas.Instance.AddCoin(goldAmount);

        foreach (ItemShop item in items)
        {
            UserDatas.Instance.AddItem(item.id, item.amount);
        }

        ShowPopupReward();

        if (isLimited)
        {
            UserDatas.Instance.IsHideLimitedBox = true;
            Destroy(gameObject);
        }

        if (isIAP) return;
        UserDatas.Instance.AddRewardAdsCountDown(idRewardAds, totalMinuteToUnlock);
        isReadyToBuy = false;
    }

    void ShowPopupReward()
    {
        List<ShopQuyNX.ItemData.Item> itemsReward = new();
        foreach (ItemShop item in items)
        {
            ShopQuyNX.ItemData.Item itemShop = ShopGenerateUIController.Instance.ItemData.items.Where(itemData => item.id == itemData.itemId).First();
            itemShop.value = item.amount;
            itemsReward.Add(itemShop);
        }

        List<UserDatas.Currency> currencies = new();
        currencies.Add(new UserDatas.Currency(Currency.Gold, goldAmount));

        UIController.Instance.ShowPopupReward(itemsReward, currencies);
    }

    bool IsReadyToBuy()
    {
        DateTime currentTime = DateTime.Now;

        string nextTimeText = UserDatas.Instance.GetRewardAdsCountDownById(idRewardAds);
        if (nextTimeText != null)
        {
            DateTime nextTime = DateTime.Parse(nextTimeText);
            TimeSpan timeDifference = nextTime - currentTime;
            double totalSeconds = timeDifference.TotalSeconds;

            return totalSeconds <= 0;
        }

        return true;
    }

    private void Update()
    {
        if (isIAP || isReadyToBuy) return;


        if (IsReadyToBuy())
        {
            buyButton.interactable = true;
            priceTxt.gameObject.SetActive(false);
            countDown.gameObject.SetActive(false);
            adIcon.gameObject.SetActive(true);
            isReadyToBuy = true;
        }
        else
        {
            buyButton.interactable = false;
            priceTxt.gameObject.SetActive(false);
            adIcon.gameObject.SetActive(false);
            countDown.gameObject.SetActive(true);

            DateTime currentTime = DateTime.Now;
            string nextTimeText = UserDatas.Instance.GetRewardAdsCountDownById(idRewardAds);
            if (DateTime.TryParse(nextTimeText, out var nextTime))
            {
                TimeSpan timeDifference = nextTime - currentTime;
                float totalSeconds = (float)timeDifference.TotalSeconds;

                float second = totalSeconds % 60f;
                float minute = totalSeconds / 60;
                float hour = minute / 60;

                string hourText = hour > 0f ? hour + "h" : "";
                string minuteText = minute > 0f ? minute + "m" : "";
                string secondText = second + "s";

                countDown.text = hourText + " " + minuteText + " " + secondText;
            }
        }
    }
    }
