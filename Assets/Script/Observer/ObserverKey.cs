﻿public class ObserverKey
{
    public const string CheckLoseLevel = "CheckLoseLevel";
    public const string CheckWinLevel = "CheckWinLevel";
    public const string AddToListItemFisrt = "AddToListItemFisrt";
    public const string DecreaseItemInListLayerFirst = "DecreaseItemInListLayerFirst";
    public const string ReplaceItem = "ReplaceItem";
    public const string UpdateItemSpecial = "UpdateItemSpecial";
    public const string ChangeAvatar = "ChangeAvatar";
    public const string CheckMatch = "CheckMatch";
    public const string AutoMatchItem = "AutoMatchItem";
    public const string AutoMatchThreeItem = "AutoMatchThreeItem";
    public const string SetTimeForLevel = "SetTimeForLevel";
    public const string SetStatusTimeCounter = "SetStatusTimeCounter";
    public const string UpdateListSortItem = "UpdateListSortItem";
    public const string ShowTutorial = "Tutorial";
    public const string UpdateStar = "UpdateStar";
    public const string UpdateGold = "UpdateGold";
    public const string UpdateHeart = "UpdateHeart";
    public const string DecreaseNumberLock = "DecreaseNumberLock";
    public const string UpdateCountBooster = "UpdateCountBooster";
    public const string CountDownHeart = "CountDownHeart";
    public const string WinLevel = "WinLevel";
    public const string LoseLevel = "LoseLevel";
}
