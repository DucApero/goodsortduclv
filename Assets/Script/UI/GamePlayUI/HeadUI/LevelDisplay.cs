using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI levelText;

    // Start is called before the first frame update
    void Start()
    {
        UpdateLevelText();
    }
    private void OnEnable()
    {
        UpdateLevelText();
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateLevelText()
    {
        levelText.text = $"Lv.{(UserDatas.Instance.CurrentLevel + 1).ToString()}";
    }
}
