using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class CheatLevel : MonoBehaviour
{
    [SerializeField] protected TMP_InputField inputField;
    [SerializeField] protected Button loadlevelBtn;
    // Start is called before the first frame update
    void Start()
    {
        loadlevelBtn.onClick.AddListener(() => {
            UserDatas.Instance.CurrentLevel = Int32.Parse(inputField.text.ToString()) - 1;
            LevelManager.Instance.ReplayLevel();
            UIController.Instance.TurnOffSettingInGame();
            UIController.Instance.UpdateLevelText();
            GameController.Instance.IsSelected = false;
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
