﻿using UnityEngine;


#if UNITY_EDITOR
public class RenameItem : MonoBehaviour
{
    public GameObject[] prefabsToRename; // Kéo và thả các Prefab cần đổi tên vào đây
  

    public  void Start()
    {

        // Kiểm tra nếu đã kéo Prefabs vào mảng prefabsToRename
        if (prefabsToRename != null && prefabsToRename.Length > 0)
        {
            for (int i = 0; i < prefabsToRename.Length; i++)
            {
                // Đặt tên mới cho Prefab
                prefabsToRename[i].name = (i + 1).ToString();
                prefabsToRename[i].GetComponent<ItemElement>().ID = i + 1;
                // Cập nhật tên Prefab trong Project Window
                UnityEditor.AssetDatabase.RenameAsset(
                    UnityEditor.AssetDatabase.GetAssetPath(prefabsToRename[i]),
                    (i + 1).ToString()
                );
            }

            // Làm mới cửa sổ Project Window để cập nhật tên
            UnityEditor.AssetDatabase.Refresh();
        }
        else
        {
            Debug.LogWarning("Chưa kéo Prefabs vào mảng prefabsToRename.");
        }
    }
}
#endif