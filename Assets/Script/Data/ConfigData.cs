using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ConfigData
{
    public bool OnSound;
    public bool OnMusic;
    public bool OnVibrate;


    public ConfigData(){
        OnSound = true;
        OnMusic = true;
        OnVibrate = true;

    }
}