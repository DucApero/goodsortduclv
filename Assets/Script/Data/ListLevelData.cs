using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "GameConfiguration/LevelData", order = 1)]
public class ListLevelData : ScriptableObject
{
    public List<LevelData> ListLevel;
}

[System.Serializable]
public class LevelData
{

    public float timeDuration;

    public int Id;
    public int CountBox;
    public List<int> lockedBoxValue;
    public int CountItem;
    public int CountLayer;
    public List<int> lstItemSpecial;
    public int[] MaxTypeItemLayer;
    public int[] EmptyInLayer;
    public List<ItemElement> LstIdItemRandom;

    public List<int> BoxOneItem = new List<int>();
    public int[] maxTypeItemBoxOneInLayer;
}