using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardDailyController : MonoBehaviour
{
    [SerializeField] protected RewardDailyElement[] lstReward;
    protected List<int> selectedReward;
    protected int lastDayPlayGame;
    [SerializeField] protected SORewardDaily soRewardDaily;

    protected void OnEnable()
    {
        InitDataDailyReward();
        SetStatusDailyReward();
    }

    protected void OnDisable()
    {
        UserDatas.Instance.SetLastDayPlayGame();
    }

    protected void InitDataDailyReward()
    {
        selectedReward = UserDatas.Instance.GetListRewardSelected();
        lastDayPlayGame = UserDatas.Instance.GetLastDayPlayGame();

        foreach(RewardDailyElement rewardElement in lstReward)
        {
            rewardElement.RewardDailyData = soRewardDaily.Reward[rewardElement.ID];
        }
    }

    public void SetStatusDailyReward()
    {
        int currentDay = GameController.Instance.GetCurrentDay();
        int dayInterval = currentDay - UserDatas.Instance.GetFirstDayPlayGame();
        Debug.Log("duclv daily current day " + currentDay);
        Debug.Log("duclv daily first play game " + UserDatas.Instance.GetFirstDayPlayGame());
        Debug.Log("duclv daily day interval: " + dayInterval);
        /*if (dayInterval > 7)
        {
            UserDatas.Instance.SetFirstDayPlayGame(GameController.Instance.GetCurrentDay());
            foreach (RewardDailyElement rewardElement in lstReward)
            {
                if (rewardElement.ID != 0)
                {
                    rewardElement.SetFeatureReward();
                }
                else
                {
                    rewardElement.SetCurrentReward();
                }
            }
        }*/

        foreach (RewardDailyElement rewardElement in lstReward)
        {
            if(dayInterval < 7)
            {
                if (dayInterval > rewardElement.ID)
                {
                    if (selectedReward.Contains(rewardElement.ID))
                    {
                        rewardElement.SetSelectedReward();
                        rewardElement.Status = RewardDailyStatus.Selected;
                    }
                    else
                    {
                        rewardElement.SetNotSelectedReward();
                        rewardElement.Status = RewardDailyStatus.NotSelected;
                    }
                }
                else if (dayInterval == rewardElement.ID)
                {
                    if (selectedReward.Contains(rewardElement.ID))
                    {
                        rewardElement.SetSelectedReward();
                        rewardElement.Status = RewardDailyStatus.Selected;
                    }
                    else
                    {
                        rewardElement.SetCurrentReward();
                        rewardElement.Status = RewardDailyStatus.Current;
                    }
                }
                else
                {
                    rewardElement.SetFeatureReward();
                    rewardElement.Status = RewardDailyStatus.Feature;
                }
            }
            else
            {
                UserDatas.Instance.SetFirstDayPlayGame(GameController.Instance.GetCurrentDay());
                UserDatas.Instance.GetListRewardSelected().Clear();
                foreach (RewardDailyElement item in lstReward)
                {
                    if (item.ID != 0)
                    {
                        item.Status = RewardDailyStatus.Feature;
                        item.SetFeatureReward();
                    }
                    else
                    {
                        item.Status = RewardDailyStatus.Current;
                        item.SetCurrentReward();
                    }
                }
            }
        }
    }
}
