using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TQT
{
    public class TutorialControler : Singleton<TutorialControler>
    {
        [SerializeField]Tutorial_Level tutorial_level;
        [SerializeField]Tutorial_Booster tutorial_booster;
        [SerializeField] Tutorial_PopupPlay tutorial_PopupPlay;
        public Tutorial_Level Tutorial_Level => tutorial_level;

        public void SetState_TutorialLevel()
        {
            tutorial_level.GameStart();
        }
        public void SetState_TutorialBotPlay()
        {
            tutorial_booster.Show();
        }
        public void SetState_TutorialPopupPlay()
        {
            tutorial_PopupPlay.Show();
        }
        public void GameEnd()
        {
            tutorial_level.GameEnd();
        }
    }
}

