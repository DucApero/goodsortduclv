using CommonEnum;
using ShopQuyNX;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemInListController : MonoBehaviour
{
    [SerializeField] ItemData _data;
    [SerializeField] Image icon;
    [SerializeField] TMP_Text amountTxt;

    [HideInInspector] public ItemGameplay id;
    [HideInInspector] public string amount;

    private void Start()
    {
        icon.sprite = GetDataById().icon;
        amountTxt.text = amount;
    }

    ItemData.Item GetDataById()
    {
        ItemData.Item itemData = _data.items.Where(item => id == item.itemId).FirstOrDefault();
        return itemData;
    }
}
