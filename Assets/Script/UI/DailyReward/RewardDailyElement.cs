using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RewardDailyElement : MonoBehaviour
{
    [SerializeField] protected int id;
    public int ID
    {
        get => this.id;
        set => id = value;
    }
    [SerializeField] protected Image panel;
    [SerializeField] protected Image selectedImage;
    [SerializeField] protected GameObject hub;
    [SerializeField] protected int price;
    [SerializeField] protected TextMeshProUGUI priceText;
    [SerializeField] Button btn;

    protected RewardDailyStatus status;
    public RewardDailyStatus Status
    {
        set => this.status = value;
        get => this.status;
    }

    protected RewardDaily rewardDaily;
    public RewardDaily RewardDailyData
    {
        get => rewardDaily;
        set => rewardDaily = value;
    }

    protected void OnEnable()
    {
        InitRewardElement();
    }

    protected void Start()
    {
        btn.onClick.AddListener(() => OnClickRewardItemDaily()); 
    }

    protected void InitRewardElement()
    {
        switch (this.status)
        {
            case RewardDailyStatus.Current:
                SetCurrentReward();
                break;
            case RewardDailyStatus.NotSelected:
                SetNotSelectedReward();
                break;
            case RewardDailyStatus.Selected:
                SetSelectedReward();
                break;
            case RewardDailyStatus.Feature:
                SetFeatureReward();
                break;
            default:
                break;
        }
    }

    public void OnClickRewardItemDaily()
    {
        switch (this.status)
        {
            case RewardDailyStatus.Current:
                SetSelectedReward();
                UserDatas.Instance.AddSelectedRewardDay(id);
                status = RewardDailyStatus.Selected;
                UIController.Instance.ShowPopupReward(rewardDaily.lstRewardBooster, rewardDaily.lstRewardCurrency, null);
                GetRewardDaily(rewardDaily.lstRewardBooster, rewardDaily.lstRewardCurrency);
                break;
            case RewardDailyStatus.NotSelected:
                if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Gold) < price) UIController.Instance.NotEnoughCoin();
                else
                {
                    UserDatas.Instance.AddSelectedRewardDay(id);
                    status = RewardDailyStatus.Selected;
                    SetSelectedReward();
                    UIController.Instance.ShowPopupReward(rewardDaily.lstRewardBooster, rewardDaily.lstRewardCurrency, null);
                    GetRewardDaily(rewardDaily.lstRewardBooster, rewardDaily.lstRewardCurrency);
                    UserDatas.Instance.SetCurrency(CommonEnum.Currency.Gold, -price);
                }
                break;
            default:
                break;
        }
    }

    public void GetRewardDaily(List<ShopQuyNX.ItemData.Item> items = null, List<UserDatas.Currency> currencies = null)
    {
        if (items.Count > 0)
        {
            foreach (ShopQuyNX.ItemData.Item item in items)
            {
                UserDatas.Instance.AddItem(item.itemId, item.value);
            }
        }
        if (currencies.Count > 0)
        {
            foreach (UserDatas.Currency currency in currencies)
            {
                UserDatas.Instance.SetCurrency(currency.Id, currency.Value);
            }
        }
    }

    public void SetSelectedReward()
    {
        panel.enabled = false;
        hub.SetActive(false);
        selectedImage.enabled = true;
    }

    public void SetNotSelectedReward()
    {
        panel.enabled = false;
        hub.SetActive(true);
        priceText.text = price.ToString();
        selectedImage.enabled = false;
    }

    public void SetCurrentReward()
    {
        panel.enabled = false;
        hub.SetActive(false);
        selectedImage.enabled = false;
    }

    public void SetFeatureReward()
    {
        panel.enabled = true;
        hub.SetActive(false);
        selectedImage.enabled = false;
    }
}

public enum RewardDailyStatus
{
    Selected,
    NotSelected,
    Current,
    Feature
}
