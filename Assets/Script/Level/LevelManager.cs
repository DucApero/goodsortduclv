﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TQT;

public class LevelManager : Singleton<LevelManager>
{
    [SerializeField] private RandomItems randomItems;
    [SerializeField] private List<LevelUnit> lstLevel;
    [SerializeField]
    private LevelUnit currentLevel;

    [SerializeField] int level = 1;

    [SerializeField] bool stopTest = false;
    [SerializeField] float timeTest = 0.1f;
    [SerializeField] int countTest = 0;
    [SerializeField] private Button Nextlevel;
    [SerializeField] private Button ReplayLevelButton;

    private bool isReplay;
    public bool IsReplay
    {
        get => isReplay;
    }
    public GameObject winPopup;
    private int currentStarEarn = 0;
    public int CurrentStarEarn
    {
        get => currentStarEarn;
        set => currentStarEarn = value;
    }
    private bool isSort;
    public bool IsSort
    {
        get => isSort;
    }
    private List<int> currentValueLock = new();
    public List<int> CurrentValueLock
    {
        get => currentValueLock;
        set => currentValueLock = value;
    }
    private bool isWinLevel = false;
    public bool IsWinLevel
    {
        get => isWinLevel;
        set => isWinLevel = value;
    }
    private bool isLoseLevel = false;
    public bool IsLoseLevel
    {
        get => isLoseLevel;
        set => isLoseLevel = value;
    }
    Camera _mainCam;
    private bool isNewLevel = false;
    public bool IsNewLevel => isNewLevel;
    private List<ItemElement> lstItem = new();
    private void Awake()
    {
    }
    private void Start()
    {
        // Nextlevel.onClick.AddListener(ClickNextLevel);
        // ReplayLevelButton.onClick.AddListener(ReplayLevel);
        //PlayLevel();
        Debug.Log("Levell currrent: " + UserDatas.Instance.CurrentLevel);
        _mainCam = Camera.main;
    }

    private IEnumerator DelayCall(float time)
    {
        yield return new WaitForSeconds(time);
        if (stopTest)
        {
            yield break;
        }
        SpawnLevel(true);
        StartCoroutine(DelayCall(timeTest));
    }

    public void ClickNextLevel()
    {
        LoadNextLevel();
    }
    public void SortItem()
    {
        SoundController.Instance.PlayActiveBooster();
        SpawnLevel(true,true);
    }
    public void ReplayLevel()
    {
        SpawnLevel(true);
    }
    public LevelUnit CurrentLevel
    {
        get => currentLevel;
    }

    public RandomItems RandomItem
    {
        get => randomItems;
    }

    public int IndexLevel
    {
        get => UserDatas.Instance.CurrentLevel;
        set => UserDatas.Instance.CurrentLevel = value;
    }

    public List<int> GetDataLockValue()
    {
        if (isSort)
        {
            Debug.Log("curent value lock " + currentValueLock.Count);
            return currentValueLock;
        }
        else
        {
            currentValueLock.Clear();
            return randomItems.levelsData.ListLevel[IndexLevel].lockedBoxValue;
        }
    }

    public void LoadNextLevel()
    {
        UIController.Instance.UpdateTextLevel();
        if (IndexLevel >= lstLevel.Count) IndexLevel = 0;
        SpawnLevel(false);
        UIController.Instance.UpdateLevelText();
        GameController.Instance.ActiveBaner(true);
        isNewLevel = true;
        GameController.Instance.CountLoseShowInter = 0;
    }



    public void Replay()
    {
        StartCoroutine(DelayCall(timeTest));
    }
    public List<LayerInfo> GetLayer(int index)
    {
        return randomItems.GetLayerWithIndexBox(index);
    }
    public void UpdateStarEarn()
    {
        CurrentStarEarn += GameController.Instance.StarEarn * GameController.Instance.GetStarEarn();
    }
    public void PlayLevel()
    {
        SpawnLevel(false);
        isNewLevel = true;
        GameController.Instance.ActiveBaner(true);
        GameController.Instance.CountLoseShowInter = 0;
    }
    public void PlayCheat()
    {
        IndexLevel++;
        if (IndexLevel >= lstLevel.Count) IndexLevel = 0;
        SpawnLevel(false);
        isNewLevel = true;
        GameController.Instance.ActiveBaner(true);
        GameController.Instance.CountLoseShowInter = 0;
    }
    private void SpawnLevel(bool isReplay, bool isSort = false)
    {
        //IndexLevel = level;
        this.isSort = isSort;
        this.isReplay = isReplay;
        DestroyLevel();
        if (!isSort)
        {
            currentStarEarn = 0;
            GameController.Instance.IsStartLevel = false;
            isLoseLevel = false;
            isWinLevel = false;
            UIController.Instance.ResetStarHub();
            UIController.Instance.GamePlayUI.ResetSliderCombo();
            dicId.Clear();
        }
        randomItems.Run(lstLevel[IndexLevel].Id, isReplay, isSort);
        lstItem = randomItems.lstItem(lstLevel[IndexLevel].Id);
        currentLevel = Instantiate(lstLevel[IndexLevel], this.transform);
        currentLevel.TimeDuration = randomItems.GetTimeLevelData();
        //currentLevel.TimeDuration = GameController.Instance.TimeLevel;
        Debug.Log("time: " + currentLevel.timeDuration);
        if (!isSort)
        {
            GameController.Instance.CountDownController.UpdateCountDownData(currentLevel.TimeDuration);
            GameController.Instance.ResetCombo();
        }
        
        currentLevel.GetInfoBoxElement();
        
        //skill 1,2
        currentLevel.GetTopThreeItem();
        currentLevel.GetItemIDCanMatch();

        currentLevel.LockBox();

        setCameraFitLevel();
        if(!isSort)
        StartCoroutine(ActionBooster(.5f));

        //TQT
        UIController.Instance.GamePlayUI.SetInfoItem(); // check unlock item booster khi spawn level mới
        CheckTutorial();
    }

    void setCameraFitLevel()
    {
        float height1 = (currentLevel.Height + 2);
        float height2 = (currentLevel.Width + 0.7f) / _mainCam.aspect;

        float perfectHeight = height1 > height2 ? height1 : height2;

        _mainCam.orthographicSize = (perfectHeight * 1.4f) / 2;

        Vector2 camSize = _mainCam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
        float cmaAngle = _mainCam.transform.rotation.eulerAngles.x * Mathf.Deg2Rad;

        float fixAngle = Mathf.Tan(cmaAngle) * Mathf.Abs(_mainCam.transform.position.z);

        float tmp = camSize.y * 0.05f;
        _mainCam.transform.position = new Vector3(0, currentLevel.CenterY + Mathf.Abs(fixAngle) * 0.8f, _mainCam.transform.position.z);
    }


    void CheckTutorial()
    {
        if (IndexLevel == 0)
        {
            TutorialControler.Instance.SetState_TutorialLevel();
        }
        TutorialControler.Instance.SetState_TutorialBotPlay();
    }
    
    IEnumerator ActionBooster(float timeDelay)
    {
        yield return new WaitForSeconds(timeDelay);
        currentLevel.ActionItemBegin();
    }

    public void DestroyLevel()
    {
        if (currentLevel != null)
            Destroy(currentLevel.gameObject);
    }

    public ItemElement SpawnItem(int id, Transform transform)
    {
        ItemElement item = null;
        for (int i = 0; i < lstItem.Count; i++)
        {
            if(id == lstItem[i].ID)
            {
                item = Instantiate(lstItem[i], transform);
                item.transform.DOScale(item.transform.localScale, 0.25f).From(0f).SetEase(Ease.OutBack);
                return item;
            }
        }
        Debug.Log("Null --- Id "+id);
        return item;
    }

    public void OnClickMagicWand()
    {
        SoundController.Instance.PlayActiveBooster();
        currentLevel.ReplaceItem();
    }
    public void AutoMatchItem()
    {
        SoundController.Instance.PlayActiveBooster();
        //SoundController.Instance.PlayMatchSound(1f);
        currentLevel.AutoMatchItem();
    } 
    public void AutoMatchThreeItem()
    {
        SoundController.Instance.PlayActiveBooster();
        //SoundController.Instance.PlayMatchSound(1f);
        currentLevel.AutoMatchThreeItem();

    }
    private List<int> lstIdItemMatch = new List<int>();
    private Dictionary<int, int> dicId = new Dictionary<int, int>();
    public void CheckMatchItem(int id = 0 ,Action actionCalback = null)
    {
        if (dicId.ContainsKey(id))
        {
            dicId[id]++;
            if (dicId[id] == 3)
            {
                dicId.Remove(id);
            }
        }
        else
        {
            dicId.Add(id,1);
            if(IndexLevel == 0)
            {
                CurrentStarEarn = 10;
            }
            if (IndexLevel != 0 && isWinLevel == false)
            {
                GameController.Instance.UpdateCombo();
                Observer.Instance.Notify(ObserverKey.DecreaseNumberLock);
                UpdateStarEarn();
                actionCalback?.Invoke();
            }
            else 
            {
                actionCalback?.Invoke();
                GameController.Instance.ResetCombo();
            }
        }
    }
}