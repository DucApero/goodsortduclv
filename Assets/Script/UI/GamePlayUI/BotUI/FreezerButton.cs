using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FreezerButton : MonoBehaviour
{
    [SerializeField] protected Button freezerButton;
    public Button FreezerButtonn
    {
        get => freezerButton;
        set => freezerButton = value;
    }
    [SerializeField] protected TextMeshProUGUI freezerCount;
    [SerializeField] protected Image addIcon;
    [SerializeField] ItemBooster_GamePlay itemBooster_GamePlay;
    public ItemBooster_GamePlay ItemBooster_GamePlay => itemBooster_GamePlay;
    // Start is called before the first frame update
    void Start()
    {
        UpdateCountBooster();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void UpdateCountBooster()
    {
        if (!itemBooster_GamePlay.isLock)
        {
            int numberBooster = UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.Freeze);
            if (numberBooster > 0)
            {
                freezerCount.gameObject.SetActive(true);
                freezerCount.text = numberBooster.ToString();
                addIcon.gameObject.SetActive(false);
            }
            else
            {
                freezerCount.gameObject.SetActive(false);
                addIcon.gameObject.SetActive(true);
            }
        }
        else
        {
            freezerCount.gameObject.SetActive(false);
            addIcon.gameObject.SetActive(false);
        }

    }
}
