using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MagicWandButton : MonoBehaviour
{
    [SerializeField] protected Button magicwandButton;
    public Button MagicwandButton
    {
        get => magicwandButton;
        set => magicwandButton = value;
    }
    [SerializeField] protected TextMeshProUGUI magicwandCount;
    [SerializeField] protected Image addIcon;

    [SerializeField] ItemBooster_GamePlay itemBooster_GamePlay;
    public ItemBooster_GamePlay ItemBooster_GamePlay => itemBooster_GamePlay;
    // Start is called before the first frame update
    void Start()
    {
        UpdateCountBooster();
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void UpdateCountBooster()
    {
        if (!itemBooster_GamePlay.isLock)
        {
            int numberBooster = UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.MagicWand);
            if (numberBooster > 0)
            {
                magicwandCount.gameObject.SetActive(true);
                magicwandCount.text = numberBooster.ToString();
                addIcon.gameObject.SetActive(false);
            }
            else
            {
                magicwandCount.gameObject.SetActive(false);
                addIcon.gameObject.SetActive(true);
            }
        }

        else
        {
            magicwandCount.gameObject.SetActive(false);
            addIcon.gameObject.SetActive(false);
        }

    }
}
