using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonEnum
{
    public enum ItemShopType
    {
        SeasonPack, NoAds, NormalPack, LimitedPack, SmallItem
    }
    
    public enum ItemGameplay
    {
        SmallHammer, Refresh, MagicWand, Freeze, BigHammer, GreatFreeze, DoubleStar, Heart
    }

    public enum Currency 
    {
        Gold = 0,
        Star = 1,
        Heart= 2
    }

    public enum DirectionBox
    {
        Left, Center, Right
    }

    public enum TypeHub
    {
        UpdateGold,
        UpdateStar,
        UpdateHeart
    }
    public enum GameState
    { 
        GameMenu,
        GamePlay,
        PauseGame,
        WinLevel,
        LoseLevel,
        StuckLevel,
        SelectItem,
    }

}

