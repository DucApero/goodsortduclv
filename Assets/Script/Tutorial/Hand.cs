using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.XR;

namespace TQT
{
    public class Hand : MonoBehaviour
    {
        private void OnEnable()
        {
        }
        public void PosCurrent(Vector3 posCurrent)
        {
            transform.position = posCurrent;
           

        }
        public void MoveHand(Vector3 target,float delay)
        {
            transform.DOMove(target, delay);
        }
    }
}

