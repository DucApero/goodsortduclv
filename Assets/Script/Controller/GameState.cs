using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEnum;

public class GameState : Singleton<GameState>
{
    private CommonEnum.GameState gameState;
    public void SetState(CommonEnum.GameState gameState)
    {
        this.gameState = gameState;
        switch (gameState)
        {
            case CommonEnum.GameState.GameMenu:
                break; 
            case CommonEnum.GameState.GamePlay:
                break;
            case CommonEnum.GameState.PauseGame:
                break;
            case CommonEnum.GameState.StuckLevel:
                break;
            case CommonEnum.GameState.WinLevel:
                break; 
            case CommonEnum.GameState.LoseLevel:
                break;


        }
    }
    public CommonEnum.GameState GetSate()
    {
        return gameState;
    }
}
