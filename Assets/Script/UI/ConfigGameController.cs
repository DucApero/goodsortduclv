using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DVAH;

public class ConfigGameController : Singleton<ConfigGameController>
{
    [Header("Config Data")]
    [SerializeField] Sprite musicOn, musicOff, soundOn, soundOff, vibrateOn, vibrateOff;
    [SerializeField] Image musicImg, soundImg, vibrateImg;

    [Header("User Data")]
    [SerializeField] private TextMeshProUGUI userName;
    [SerializeField] private Image flagCountry;
    [SerializeField] private FlagDataContainer flagContainer;
    [SerializeField] Button btnClose;
    [SerializeField] Button btnRePlay;
    [SerializeField] Button btnHome;
    [SerializeField] Button btnContinue;
    [SerializeField] private bool isHome;
    private void Awake()
    {
        if(btnHome)
        btnHome.onClick.AddListener(()=> 
        {
            SoundController.Instance.PlayClickButton();
            MoveToHome();

        });
        if (btnRePlay)
        btnRePlay.onClick.AddListener(()=> 
        {
            SoundController.Instance.PlayClickButton();
            UIController.Instance.ShowpoupRewardPlay(Replay, false);

        });
        if (btnContinue)
        {
            btnContinue.onClick.AddListener(()=> 
            {
                SoundController.Instance.PlayClickButton();
                ActivePopup(false);
               /* if(GameController.Instance.CountDownController.TimeFreeze > 0)
                {

                    GameController.Instance.SetAmountTimeFree(GameController.Instance.CountDownController.TimeFreeze);
                }*/
            });
        }
        if (btnClose)
        btnClose.onClick.AddListener(()=> 
        {
            SoundController.Instance.PlayClickButton();
            ActivePopup(false);
            if (GameController.Instance.CountDownController.TimeFreeze > 0)
            {
                GameController.Instance.SetAmountTimeFree(GameController.Instance.CountDownController.TimeFreeze);

            }
            string scene = "";
            scene = isHome ? "home" : "gameplay";
            FireBaseManager.Instant.LogEventWithParameterAsync("btn_back", new Hashtable {
                {
                    "id_screen",scene
                }
        });
        });
       
    }
    private void OnEnable()
    {
        string scene = "";
        scene = isHome ? "home" : "gameplay";
        FireBaseManager.Instant.LogEventWithParameterAsync("setting_start", new Hashtable {
                {
                    "id_screen",scene
                }
        });
        GameController.Instance.IsStartLevel = false;
        InitSettingData();
        
    }
    private void OnDisable()
    {
        GameController.Instance.IsStartLevel = true;
    }
    private void Start()
    {
        InitSettingData();
    }
    public void ActivePopup(bool isActive)
    {
        this.gameObject.SetActive(isActive);
        GameController.Instance.IsSelected = isActive;
    }
    public void MoveToHome()
    {
        GameController.Instance.ResetCountDownHub();
        UIController.Instance.MoveToHome();
        ActivePopup(false);
    }
    private void Replay()
    {
        GameController.Instance.ReplayLevel();
        ActivePopup(false);
    }
    public void OnClickSetting(int id)
    {
        string scene = "";
        scene = isHome ? "home" : "gameplay";
        Hashtable hash = new();
        switch (id)
        {
            case 0:
                SoundController.Instance.ChangeMusicSetting();
                bool musicState = SessionPref.GetValueMusic();
                if (musicState)
                {
                    musicImg.sprite = musicOff;
                    TurnOffMusic();
                }
                else
                {
                    musicImg.sprite = musicOn;
                    TurnOnMusic();
                }
                hash.Add("id_set", !musicState);
                hash.Add("id_screen", scene);
                FireBaseManager.Instant.LogEventWithParameterAsync("setting_btn_music", hash);
                SoundController.Instance.PlayClickButton();
                SessionPref.SetValueMusic(!musicState);
                break;
            case 1:
                SoundController.Instance.ChangeSFXSetting();
                bool soundState = SessionPref.GetValueSound();
                if (soundState)
                {
                    soundImg.sprite = soundOff;
                    
                }
                else
                {
                    soundImg.sprite = soundOn;
                }
                hash.Add("id_set", !soundState);
                hash.Add("id_screen", scene);
                FireBaseManager.Instant.LogEventWithParameterAsync("setting_btn_sound", hash);
                SoundController.Instance.PlayClickButton();
                SessionPref.SetValueSound(!soundState);
                break;
            case 2:
                bool vibrateState = SessionPref.GetValueVibrate();
                if (vibrateState)
                {
                    vibrateImg.sprite = vibrateOff;
                }
                else
                {
                    vibrateImg.sprite = vibrateOn;
                }
                hash.Add("id_set", !vibrateState);
                hash.Add("id_screen", scene);
                FireBaseManager.Instant.LogEventWithParameterAsync("setting_btn_vibrate", hash);
                SoundController.Instance.PlayClickButton();
                SessionPref.SetValueVibrate(!vibrateState);
                break;
        }
    }
    private void InitSettingData()
    {
        InitMusic();
        InitSound();
        InitVibrate();
        InitUserData();
    }
    private void InitMusic()
    {
        bool musicState = SessionPref.GetValueMusic();
        if (musicState)
        {
            musicImg.sprite = musicOn;
        }
        else
        {
            musicImg.sprite = musicOff;
        }
    }
    private void InitSound()
    {
        bool soundState = SessionPref.GetValueSound();
        if (soundState)
        {
            soundImg.sprite = soundOn;
        }
        else
        {
            soundImg.sprite = soundOff;
        }
    }
    private void InitVibrate()
    {
        bool vibrateState = SessionPref.GetValueVibrate();
        if (vibrateState)
        {
            vibrateImg.sprite = vibrateOn;
        }
        else
        {
            vibrateImg.sprite = vibrateOff;
        }
    }

    private void InitUserData()
    {
        if(userName!=null)
        userName.text = UserDatas.Instance.GetUserData().Name;
        SetAvatar(UserDatas.Instance.GetUserData().IDAvatar);
    }

    public void SetAvatar(int index)
    {
        if (flagContainer && flagCountry)
        {
            Sprite userFlag = flagContainer.FlagContainer[index].FlagIcon;
            flagCountry.sprite = userFlag;
        }
        
    }

    public void SetName(string name)
    {
        if(userName)
        userName.text = name;
    }

    private void TurnOnMusic()
    {
        //SoundManager.Instance.BackGroundMusic.Play();
    }
    private void TurnOffMusic()
    {
        //SoundManager.Instance.BackGroundMusic.Stop();
    }
}
