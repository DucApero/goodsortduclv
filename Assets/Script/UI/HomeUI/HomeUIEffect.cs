using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeUIEffect : MonoBehaviour
{
    [SerializeField] Transform startButton;

    private void OnEnable()
    {
        startButton.DOScale(1f, 0.5f).From(0f).SetEase(Ease.OutBack);
    }
}
