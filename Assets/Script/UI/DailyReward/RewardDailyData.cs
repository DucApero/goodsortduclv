using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardDailyData
{
    public int lastDayPlay;
    public int firstDayPlay;
    public List<int> selectedReward;

    public RewardDailyData()
    {
        this.lastDayPlay = GameController.Instance.GetCurrentDay();
        this.firstDayPlay = GameController.Instance.GetCurrentDay();
        this.selectedReward = new();
    }
}
