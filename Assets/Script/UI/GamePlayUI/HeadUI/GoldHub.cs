using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DVAH;

public class GoldHub : HubCurrency
{
    [SerializeField] Button btnGold;
    private void Start()
    {
        if (btnGold)
        {
            btnGold.onClick.AddListener(() =>
                    {
                        UIController.Instance.ClickBtnShop();
                        FireBaseManager.Instant.LogEventWithParameterAsync("home_btn_gold", new Hashtable { {
                                "id_screen",1
                            } }) ;
                    });
        }
        
        UpdateValue(UserDatas.Instance.GetCurrency(CommonEnum.Currency.Gold));
    }
}
