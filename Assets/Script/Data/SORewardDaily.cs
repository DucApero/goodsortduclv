using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SORewardDaily", menuName = "GameConfiguration/SORewardDaily")]
public class SORewardDaily : ScriptableObject
{
    public RewardDaily[] Reward;
}

[System.Serializable]
public class RewardDaily
{
    public List<ShopQuyNX.ItemData.Item> lstRewardBooster;
    public List<UserDatas.Currency> lstRewardCurrency;
}