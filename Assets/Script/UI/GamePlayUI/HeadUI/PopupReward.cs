using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupReward : PopupUI
{
    [SerializeField] Button btnClaim;
    [SerializeField] Transform tfSpawnIemBooster;
    [SerializeField] Transform tfSpawnCurrency;
    [SerializeField] GameObject itemPrefabBooster;
    [SerializeField] GameObject itemPrefabCurrency;
    [SerializeField] Transform singleContainer;
    [SerializeField] AnimationComponent animationComponent;
    [SerializeField] Transform tfPopup;
    [SerializeField] TMP_Text txtProgress;
    [SerializeField] Image imgProgress;
    private Action actionCallback;
    private List<GameObject> lstItem = new();
    private void Awake()
    {
        btnClaim.onClick.AddListener(()=> 
        {
            mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                ActivePopup(false);

            });
        });
    }
    private void OnEnable()
    {
        UpdateProgress();
    }
    public void ShowAndSetInfo(List<ShopQuyNX.ItemData.Item> items = null, List<UserDatas.Currency> currencies = null, Action action = null)
    {
        ActivePopup(true);
        actionCallback = action;
        animationComponent.gameObject.SetActive(true);
        tfPopup.gameObject.SetActive(false);
        StartCoroutine(DelayShowAnimationComponent(items, currencies));

    }

    IEnumerator DelayShowAnimationComponent(List<ShopQuyNX.ItemData.Item> items = null, List<UserDatas.Currency> currencies = null)
    {
        
        yield return new WaitForSeconds(.5f);
        
        animationComponent.SetStateSkeUI(0, "animation", false, false, () =>
        {
            SoundController.Instance.PlayOpenRewardBox();
            tfPopup.gameObject.SetActive(true);
            tfPopup.DOScale(1f, 0.5f).From(0f).SetEase(Ease.OutBack).OnComplete(() =>
            {
                animationComponent.gameObject.SetActive(false);
                StartCoroutine(ShowReward(items, currencies));
            });
        });
    }

    IEnumerator ShowReward(List<ShopQuyNX.ItemData.Item> items, List<UserDatas.Currency> currencies)
    {
        
        yield return new WaitForSeconds(0.15f);
        if (currencies != null)
        {
            Debug.Log("currencies = " + currencies.Count);
           
            Transform container = tfSpawnIemBooster;
            //if (items == null || items.Count == 0) container = singleContainer;

            foreach (var item in currencies)
            {
                GameObject itemChild = Instantiate(itemPrefabCurrency, container);
                itemChild.transform.DOScale(1f, 0.5f).From(0f).SetEase(Ease.OutBack);

                ItemCurrency itemCurrency = itemChild.GetComponent<ItemCurrency>();
                itemCurrency.type = item.Id;
                itemCurrency.amount = item.Value.ToString();
                lstItem.Add(itemChild);

                yield return new WaitForSeconds(0.15f);
            }
        }
        if (items != null)
        {

            Debug.Log("items = " + items.Count);
            Transform container = tfSpawnIemBooster;
            //if (currencies == null || currencies.Count == 0) container = singleContainer;

            foreach (var item in items)
            {
                GameObject itemChild = Instantiate(itemPrefabBooster, container);
                itemChild.transform.DOScale(1f, 0.5f).From(0f).SetEase(Ease.OutBack);
                ItemInListController itemInListController = itemChild.GetComponent<ItemInListController>();
                itemInListController.id = item.itemId;
                if(item.itemId == CommonEnum.ItemGameplay.Heart)
                {
                    itemInListController.amount = (item.value / 3600).ToString() +"h";
                }
                else
                {
                    itemInListController.amount = item.value.ToString();
                }
                lstItem.Add(itemChild);

                yield return new WaitForSeconds(0.15f);
            }
        }
    }

    private void ResetPopup()
    {
        foreach(var item in lstItem)
        {
            Destroy(item.gameObject);
        }
        lstItem.Clear();
    }
    private void ActivePopup(bool isActive)
    {
        this.gameObject.SetActive(isActive);
        if (!isActive)
        {
            actionCallback?.Invoke();
            actionCallback = null;
            ResetPopup();
        }
    }
    private void UpdateProgress() 
    {

        int current = GameController.Instance.CurrentStar;
        int max = GameController.Instance.MaxStar;
        txtProgress.text = current.ToString() + " / " + max.ToString();
        imgProgress.fillAmount = (float)current / (float)max;

        DOTween.To(() => current, x => current = x, max, 2f)
            .OnUpdate(() =>
            {
                txtProgress.text = Mathf.FloorToInt(current).ToString() + " / " + max.ToString();
                imgProgress.fillAmount = (float)current / (float)max;
            }).SetEase(Ease.OutCirc);
    }
}
