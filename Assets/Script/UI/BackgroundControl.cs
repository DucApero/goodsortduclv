using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundControl : MonoBehaviour
{
    [SerializeField] GameObject bgMenu;
    [SerializeField] GameObject bgGamePlay;
    public void MoveToGamePlay()
    {
        ActiveBgMenu(false);
        ActiveBgGamePlay(true);
    }
    public void MoveToLobby()
    {
        ActiveBgMenu(true);
        ActiveBgGamePlay(false);
    }
    private void ActiveBgMenu(bool isActive)
    {
        bgMenu.SetActive(isActive);
    }
    private void ActiveBgGamePlay(bool isActive)
    {
        if(bgGamePlay)
        bgGamePlay.SetActive(isActive);
    }
}
