using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemBoosterPlayGame : MonoBehaviour
{
    [SerializeField] private Button btnClick;
    [SerializeField] private bool isClick = false;
    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI txtValue;
    [SerializeField] private Sprite[] sptStateBtn;
    [SerializeField] private GameObject counter;
    [SerializeField] private GameObject goSelected;

    //locked
    [SerializeField] private Sprite lockSprite;
    [SerializeField] private TextMeshProUGUI levelText;

    private ShopQuyNX.ItemData.Item currentItem;
    public ShopQuyNX.ItemData.Item CurrentItem => currentItem;
    private void Awake()
    {
        btnClick.onClick.AddListener(ActionClick);
    }
    public void SetInfo(ShopQuyNX.ItemData.Item item)
    {
        currentItem = item;
        icon.sprite = currentItem.icon;
        UpdateTextValue();
        SetItemStateUI();
    }

    private void OnEnable()
    {
        SetItemStateUI();
    }
    private void ActionClick()
    {
        SoundController.Instance.PlayClickButton();
        if (isClick)
        {
            isClick = false;
            
            if (GameController.Instance.LstBoosterInLevel.Contains(currentItem.itemId))
            {
                GameController.Instance.LstBoosterInLevel.Remove(currentItem.itemId);
            }
            if(goSelected)
            goSelected.SetActive(false);

        }
        else
        {
            
            if (UserDatas.Instance.GetCurrentItemData(currentItem.itemId) <= 0) return;
            isClick = true;
            if (!GameController.Instance.LstBoosterInLevel.Contains(currentItem.itemId))
            {
                GameController.Instance.LstBoosterInLevel.Add(currentItem.itemId);
            }
            if(goSelected)
            goSelected.SetActive(true);

        }
        UpdateTextValue();
    }
    public void ClickTutorial()
    {
        if (!GameController.Instance.LstBoosterInLevel.Contains(currentItem.itemId))
        {
            GameController.Instance.LstBoosterInLevel.Add(currentItem.itemId);
        }
        if (goSelected)
            goSelected.SetActive(true);
    }
    private void UpdateTextValue() 
    {
        txtValue.text = UserDatas.Instance.GetCurrentItemData(currentItem.itemId).ToString();
    }

    public void SetItemStateUI()
    {
        if (currentItem == null) return;
        if(UserDatas.Instance.CurrentLevel + 1 >= currentItem.requiredLevel)
        {
            icon.sprite = currentItem.icon;
            counter.gameObject.SetActive(true);
            levelText.gameObject.SetActive(false);
            btnClick.enabled = true;
        }
        else
        {
            btnClick.enabled = false;
            icon.sprite = lockSprite;
            counter.gameObject.SetActive(false);
            levelText.gameObject.SetActive(true);
            levelText.text = "Lv." + currentItem.requiredLevel.ToString();
        }
        UpdateTextValue();
    }
    private void OnDisable()
    {
        SetDefaultUI();
    }

    void SetDefaultUI()
    {
        isClick = false;
        goSelected.SetActive(false);
    }
}


