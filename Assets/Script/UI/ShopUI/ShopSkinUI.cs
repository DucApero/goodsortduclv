﻿using DG.Tweening;
using DVAH;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSkinUI : Singleton<ShopSkinUI>
{
    [SerializeField] protected SkinContainer backgroundContainer;
    [SerializeField] private ShopItem shopItemPrefab;
    [SerializeField] private Transform itemShopContainer;
    [SerializeField] private Sprite[] lstBackground;
    [SerializeField] private Image backgroundPreview;
    [SerializeField] private Button btnClose;
    private int itemNumber;

    ShopItem itemSelected;

    private RecordItemSkin recordSkin = default;
    public RecordItemSkin RecordSkin
    {
        get
        {
            return recordSkin;
        }
        set
        {
            recordSkin = value;
        }
    }

    private RecordItemSkin recordSkinReward = default;
    public void ActivePopup(bool isActive)
    {
        this.gameObject.SetActive(isActive);
    }
    private void Awake()
    {
        btnClose.onClick.AddListener(()=> 
        {
            SoundController.Instance.PlayClickButton();
            transform.DOScale(0f, 0.25f).From(1f).SetEase(Ease.InBack).OnComplete(() =>
            {
                ActivePopup(false);
            });
        });
    }

    private void OnEnable()
    {
        transform.DOScale(1f, 0.25f).From(0f).SetEase(Ease.OutBack);
        FireBaseManager.Instant.LogEventWithParameterAsync("shop_start", new Hashtable {
                {
                    "id_screen","home"
                }
        });
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdateBackgroundPreview(UserDatas.Instance.GetItemIsSelected());
        SpawnItemShop();
    }
    private void GetDataFormJson()
    {
        if (recordSkin.Equals(default(RecordItemSkin)))
        {
            recordSkin.itemSkin = DataController.Instance.itemSkinVO.GetDatasByName<RecordItemSkin.ItemInfoSkin>("itemskin");
            Debug.Log(recordSkin.itemSkin.Length + "count");
        }
        if (recordSkinReward.Equals(default(RecordItemSkin)))
        {
            recordSkinReward.itemSkin = DataController.Instance.itemSkinVO.GetDatasByName<RecordItemSkin.ItemInfoSkin>("item_skin_reward");
        }
        Debug.Log(recordSkin.itemSkin.Length + "number skin");
    }

    private void SpawnItemShop()
    {
        
        Debug.Log("selected " + UserDatas.Instance.GetItemIsSelected());
        foreach(int id in UserDatas.Instance.GetOwnedItem())
        {
            Debug.Log("owned: " + id);

        }
        
        GetDataFormJson();
        for (int i = 0; i < recordSkin.itemSkin.Length; i++)
        {
            ShopItem item = Instantiate(shopItemPrefab, itemShopContainer);
            item.CallbackSelectItem = (item) =>
            {
                if (itemSelected)
                {
                    itemSelected.UnSelectItem();
                    // chuyển thành trạng thái is Select
                }
                itemSelected = item;
                UpdateBackgroundPreview(item.id);
            };

            if (UserDatas.Instance.GetOwnedItem().Contains(i))
            {
                item.HubItem.enabled = false;
                item.ID = i;
                item.ItemType = recordSkin.itemSkin[i].type;
                item.Cost = recordSkin.itemSkin[i].cost;
                item.SetStatusItemUI();
                if (UserDatas.Instance.GetItemIsSelected() == item.id)
                {
                    itemSelected = item;
                }
            }
            else
            {
                item.ID = i;
                item.ItemType = recordSkin.itemSkin[i].type;
                item.Cost = recordSkin.itemSkin[i].cost;
                item.SetStatusItemUI();

            }

            item.SetSkinItem(backgroundContainer);
        }
    }

    private void SetItemStatus(ShopItemBase item,int id)
    {
        switch (recordSkin.itemSkin[id].type)
        {
            case 1:
                item.Cost.text = $"{UserDatas.Instance.GetWatchAdsCount(id)}/ {recordSkin.itemSkin[id].cost}";
                break;
            case 2:
                item.Cost.text = $"{recordSkin.itemSkin[id].cost}";
                break;
            default:
                break;
        }
    }

    private void UpdateBackgroundPreview(int id)
    {
        if(id != -1)
        backgroundPreview.sprite = backgroundContainer.listBackground[id].backgroundPreview;
    }
}
