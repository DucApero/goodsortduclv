using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NoAdsController : IShopItem
{
    [HideInInspector] public Sprite iconSprite;
    [HideInInspector] public float price;
    [HideInInspector] public int coin;

    [SerializeField] Image iconImage;
    [SerializeField] TMP_Text priceTxt;

    private void Start()
    {
        isIAP = true;
        iconImage.sprite = iconSprite;
        priceTxt.text = price + " $";
    }

    public override void OnClickBuy()
    {
        SoundController.Instance.PlayClickButton();
        UserDatas.Instance.IsRemoveAds = true;
        GameController.Instance.BuyRemoveAds();
        ShopGenerateUIController.Instance.ShowRewardAnItemPopup(null, coin);
        Destroy(gameObject);
    }
}
