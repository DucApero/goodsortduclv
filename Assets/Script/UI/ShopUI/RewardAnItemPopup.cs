using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RewardAnItemPopup : PopupUI
{
    [SerializeField] Image icon;
    [SerializeField] TMP_Text amountTxt;

    [HideInInspector] public bool isGold;
    [HideInInspector] public Sprite iconSprite;
    [HideInInspector] public int amount;

    private void Start()
    {
        amountTxt.text = "x" + amount;
        if (isGold) return;
        icon.sprite = iconSprite;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) Destroy(gameObject);
    }
}
