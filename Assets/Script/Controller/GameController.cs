using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using DVAH;
using UnityEngine.Events;
using CommonEnum;
using System.Threading.Tasks;

public class GameController : Singleton<GameController>
{
    [SerializeField] private ShopQuyNX.ItemData itemDataGamePlay;
    [SerializeField] private SORewardStar sORewardStar;
    [SerializeField] private ConditionCombo conditionCombo;
    public ShopQuyNX.ItemData ItemDataGamePlay => itemDataGamePlay;
    [SerializeField] private CountDownHub countDownHub;
    public CountDownHub CountDownController
    {
        get => countDownHub;
    }
    [SerializeField] private StarHub starHub;
    public StarHub StarHub
    {
        get => starHub;
        set => starHub = value;
    }
    //Star open reward
    int maxStar, currentStar;
    public int MaxStar => maxStar;
    public int CurrentStar
    {
        get { return currentStar; }
        set { currentStar = value; }
    }
    //
    string conditionShowRate = "";
    private List<int> lstShowRateRemote = new List<int> { 5, 10 };
    public List<int> LstShowRateRemote
    {
        get => lstShowRateRemote;
    }
    private int timeheart = 300;
    public int TimeHeart => timeheart;
    private int timelevel = 100;
    public int TimeLevel => timelevel;
    private int countWinShowInterRemote = 1;
    private int conditionLevelShowInterRemote = 1;
    private int countLoseShowInter = 0;
    string loseShowInterRemote = "";
    private List<int> lstLoseShowInterRemote = new List<int> { 1, 3, 5, 7, 9 };
    public List<int> LstLoseShowInterRemote
    {
        get => lstLoseShowInterRemote;
    }

    private int countWinShowInter = 0;
    public int CountWinShowInter
    {
        get => countWinShowInter;
        set => countWinShowInter = value;
    }
    
    public int CountLoseShowInter
    {
        get => countLoseShowInter;
        set => countLoseShowInter = value;
    }

    [SerializeField] private HeartBar heartBar;
    public HeartBar HeartBarr
    {
        get => heartBar;
    }
    public List<CommonEnum.ItemGameplay> lstBoosterInLevel = new List<CommonEnum.ItemGameplay>(); 
    public List<CommonEnum.ItemGameplay> LstBoosterInLevel 
    {
        get => lstBoosterInLevel;
        set => lstBoosterInLevel = value;
    }
    private int countCombo = 0;
    public int CountCombo
    {
        get => countCombo;
        set => countCombo = value;
    }
    private bool isSelected = false;
    public bool IsSelected
    {
        get
        {
            return isSelected;
        }
        set
        {
            isSelected = value;
        }

    }

    private bool isStartLevel;
    public bool IsStartLevel
    {
        get => isStartLevel;
        set => isStartLevel = value;
    }

    public int StarEarn => X2Star();

    private int timeInfinityRemaining;
    public int TimeInfinityRemaining
    {
        get => timeInfinityRemaining;
        set => timeInfinityRemaining = value;
    }
    public int targetTime;

    private void Start()
    {
        
        timeInfinityRemaining = GetTimeInfinityRemaining();
        targetTime = GetCurrentTimeSecond() + timeInfinityRemaining;
        Debug.Log("time remaing "+ timeInfinityRemaining);
        StartCoroutine(WaitFetchFirebaseDone());
        Observer.Instance.AddObserver(ObserverKey.UpdateCountBooster, AddInfi);
    }
    
    private void OnDestroy()
    {
        Observer.Instance.RemoveObserver(ObserverKey.UpdateCountBooster, AddInfi);
    }

    IEnumerator WaitFetchFirebaseDone()
    {
        yield return new WaitUntil(() => FireBaseManager.Instant.isFetchDOne);
        DataRemote();
    }
    public async void DataRemote()
    {
        try
        {

            Task[] task = new Task[6];

            task[0] = FireBaseManager.Instant.GetValueRemote("conditionLevelShowInter", (value) => {

                conditionLevelShowInterRemote = (int)value.LongValue;

            });
            task[1] = FireBaseManager.Instant.GetValueRemote("countWinShowInterRemote", (value) => {

                countWinShowInterRemote = (int)value.LongValue;

            });
            task[2] = FireBaseManager.Instant.GetValueRemote("lstLoseShowInterRemote", (value) => {
                loseShowInterRemote = value.StringValue;
                ConvertStringRemoteToListLoseShowInter();
            });
            task[3] = FireBaseManager.Instant.GetValueRemote("timeheart", (value) => {

                timeheart = (int)value.LongValue;

            });
            task[4] = FireBaseManager.Instant.GetValueRemote("timelevel", (value) => {

                timelevel = (int)value.LongValue;

            });
            task[5] = FireBaseManager.Instant.GetValueRemote("conditionLevelShowRate", (value) => {
                conditionShowRate = value.StringValue;
                ConvertStringRemoteToListShowRate();
            });
            await Task.WhenAll(task);

        }
        catch (Exception e)
        {

            Debug.LogError("e = " + e.ToString());
        }

    }
    public void AddInfi(object data)
    {
        
        ItemGameplay item = (ItemGameplay)data;
        if (item == CommonEnum.ItemGameplay.Heart)
        {
            
            AddInfinityTest(UserDatas.Instance.GetCurrentItemData(ItemGameplay.Heart));
            
        }
    }
    public void AddInfinityTest(int value)
    {
        timeInfinityRemaining = value;
        targetTime = GetCurrentTimeSecond() + timeInfinityRemaining;
    }

    private void ConvertStringRemoteToListLoseShowInter()
    {
        lstLoseShowInterRemote.Clear();
        int[] nums = Array.ConvertAll(loseShowInterRemote.Split(','), int.Parse);
        for (int i = 0; i < nums.Length; i++)
        {
            lstLoseShowInterRemote.Add(nums[i]);

        }

    }
    private void ConvertStringRemoteToListShowRate()
    {
        lstShowRateRemote.Clear();
        int[] nums = Array.ConvertAll(conditionShowRate.Split(','), int.Parse);
        for (int i = 0; i < nums.Length; i++)
        {
            lstShowRateRemote.Add(nums[i]);
        }

    }


    public int GetTimeInfinityRemaining()
    {
        if (UserDatas.Instance.GetTimeOut() != 0)
        {
            int timeTemp = GetCurrentTimeSecond() - UserDatas.Instance.GetTimeOut() - UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.Heart);

            if (timeTemp < 0)
            {
                return Mathf.Abs(timeTemp);
            }
            else return 0;
            
        }
        else
        {
            return timeInfinityRemaining;
        }
    }

   /* private void OnApplicationQuit()
    {
        Debug.Log(timeInfinityRemaining);
        UserDatas.Instance.SetTimeOut(GetCurrentTimeSecond());
        if (timeInfinityRemaining > 0)
            UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Heart, timeInfinityRemaining - UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.Heart));
        else UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Heart, 0);
    }*/
    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            UserDatas.Instance.SetTimeOut(GetCurrentTimeSecond());
            if (timeInfinityRemaining > 0)
                UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Heart, timeInfinityRemaining - UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.Heart));
            else UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Heart, 0);

            //set time reward daily
        }
        else
        {
            timeInfinityRemaining = GetTimeInfinityRemaining();
            targetTime = GetCurrentTimeSecond() + timeInfinityRemaining;
        }
    }

    private void OnApplicationQuit()
    {
    }

    public int GetCurrentTimeSecond()
    {
        int day = DateTime.UtcNow.Day;
        int hour = DateTime.UtcNow.Hour;
        int minute = DateTime.UtcNow.Minute;
        int second = DateTime.UtcNow.Second;
        return ((day * 86400)+(hour * 3600)+(minute * 60) + second);
    }

    public int GetCurrentDay()
    {
        return DateTime.UtcNow.Minute;
    }

    public void MoveToGamePlay()
    {
        LevelManager.Instance.PlayLevel();
        
    }
    public void MoveToHome()
    {
        ActiveBaner(false);
        LevelManager.Instance.DestroyLevel();
        
    }
    public void ResetCountDownHub()
    {
        CountDownController.ResetHub();
    }
    public void ReplayLevel()
    {
        ResetCountDownHub();
        LevelManager.Instance.ReplayLevel();

    }
    public void NextLevel()
    {
        ResetCountDownHub();
        LevelManager.Instance.ClickNextLevel();
    }
    public int X2Star()
    { 
        if (lstBoosterInLevel.Contains(CommonEnum.ItemGameplay.DoubleStar))
        {
            return itemDataGamePlay.items.Where(item => item.itemId == CommonEnum.ItemGameplay.DoubleStar).FirstOrDefault().value;
        }
        else
        {
            return 1;
        }
        
    }

    public int TimeGreatFreeze()
    {
        if (lstBoosterInLevel.Contains(CommonEnum.ItemGameplay.GreatFreeze))
        {
            return itemDataGamePlay.items.Where(item => item.itemId == CommonEnum.ItemGameplay.GreatFreeze).FirstOrDefault().value;
        }
        else
        {
            return 0;
        }

    }
    public int TimeFreeze()
    {
        return itemDataGamePlay.items.Where(item => item.itemId == CommonEnum.ItemGameplay.Freeze).FirstOrDefault().value;
    }
    public void SetTimeFree()
    {
        SoundController.Instance.PlayActiveBooster();
        countDownHub.SetTimeFreeze(TimeFreeze());
        Debug.LogError("pause");
    }
    public void SetAmountTimeFree(float time)
    {
        SoundController.Instance.PlayActiveBooster();
        GamePlayUIEffect.Instance.SpawnFreezeEffect(time);
    }


    // Chỉ dành cho tutorial vì đang fix cứng
    public void SetTimeFree60()
    {
        //GamePlayUIEffect.Instance.SpawnFreezeEffect();
        countDownHub.SetTimeFreeze(TimeGreatFreeze());
    }
    //

    public void CheckAndShowRewardStar(Action action )
    {
        int countStar = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Star);
        bool isShowReward = false;
        List<ShopQuyNX.ItemData.Item> lstRewardBooster = new();
        List<UserDatas.Currency> lstRewardCurrency = new();
        maxStar = GetConditionStarRewar();
        while (countStar >= GetConditionStarRewar())
        {
            UserDatas.Instance.SetCurrency(CommonEnum.Currency.Star, -GetConditionStarRewar());
            if (UserDatas.Instance.IndexGetRewardStar < sORewardStar.Reward.Length)
            {
                foreach (ShopQuyNX.ItemData.Item booster in sORewardStar.Reward[UserDatas.Instance.IndexGetRewardStar].lstRewardBooster)
                {
                    UserDatas.Instance.AddItem(booster.itemId, booster.value);
                    if (lstRewardBooster.Count == 0)
                    {
                        lstRewardBooster.Add(booster);
                        Debug.Log("add ="+ booster.itemId + " value = "+ booster.value);
                        continue;
                    }
                    int count = lstRewardBooster.Count;
                    for (int i = 0; i < count; i++)
                    {
                        if (lstRewardBooster[i].itemId == booster.itemId)
                        {
                            lstRewardBooster[i].value += booster.value;
                            Debug.Log("add =" + booster.itemId + " value = " + booster.value);
                            break;
                        }
                        if (i == lstRewardBooster.Count - 1)
                        {
                            lstRewardBooster.Add(booster);
                            Debug.Log("add =" + booster.itemId + " value = " + booster.value);
                        }
                    }

                }
                foreach (UserDatas.Currency currency in sORewardStar.Reward[UserDatas.Instance.IndexGetRewardStar].lstRewardCurrency)
                {
                    UserDatas.Instance.SetCurrency(currency.Id, currency.Value);
                    if (lstRewardCurrency.Count == 0)
                    {
                        lstRewardCurrency.Add(currency);
                        continue;
                    }
                    int count = lstRewardCurrency.Count;
                    for (int i = 0; i< count; i++ )
                    {
                        if(lstRewardCurrency[i].Id == currency.Id)
                        {
                            lstRewardCurrency[i].Value += currency.Value;
                            break;
                        }
                        if(i == lstRewardCurrency.Count - 1)
                        {
                            lstRewardCurrency.Add(currency);
                        }
                    }
                }
                UserDatas.Instance.IndexGetRewardStar++;
                if (!isShowReward)
                    isShowReward = true;
                countStar = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Star);
            }
            else
            {
                break;
            }
            


        }
        if (isShowReward)
        {
            UIController.Instance.ShowPopupReward(lstRewardBooster, lstRewardCurrency, action);
        }
        else
        {
            action?.Invoke();
        }
           
        
    }
    public int GetConditionStarRewar()
    {
        if (UserDatas.Instance.IndexGetRewardStar >= sORewardStar.Reward.Length) return 1;
        return sORewardStar.Reward[UserDatas.Instance.IndexGetRewardStar].conditionStarUnlock;
    }
    public int GetStarEarn()
    {
        if (countCombo >= conditionCombo.ListCombo.Count)
        {
            return conditionCombo.ListCombo[conditionCombo.ListCombo.Count -1 ].star;
        }
        return conditionCombo.ListCombo[countCombo].star;
    }
    public float GetTimeCombo()
    {
        if (countCombo >= conditionCombo.ListCombo.Count)
        {
            return conditionCombo.ListCombo[conditionCombo.ListCombo.Count - 1].time;
        }
        return conditionCombo.ListCombo[countCombo].time;
    }
    public void UpdateCombo()
    {
        
        countCombo++;
        SoundController.Instance.PlayMatchSound(0f,countCombo);
        UIController.Instance.UpdateHubCombo();
    }
    public void ResetCombo()
    {
        countCombo = 0;
        UIController.Instance.UpdateHubCombo();
    }
    public void ActiveX2Star()
    {
        lstBoosterInLevel.Add(CommonEnum.ItemGameplay.DoubleStar);
    }
    public void ResetBoosterPlayGame()
    {
        lstBoosterInLevel.Clear();
    }
    public void WinShowInter(UnityAction callBack = null)
    {
        this.callBack = callBack;
        if (LevelManager.Instance.IndexLevel < conditionLevelShowInterRemote - 1)
        {
            this.callBack?.Invoke();
            return;
        }
        else if(LevelManager.Instance.IndexLevel == conditionLevelShowInterRemote - 1)
        {
            countWinShowInter = countWinShowInterRemote;
        }
        countWinShowInter++;
        if(countWinShowInter >= countWinShowInterRemote)
        {
            ShowInterAds(callBack);
            countWinShowInter = 0;
        }
        else
        {
            callBack?.Invoke();
        }
    }
    public void LoseShowInter(UnityAction callBack = null)
    {
        this.callBack = callBack;
        if (LevelManager.Instance.IndexLevel < conditionLevelShowInterRemote - 1)
        {
            this.callBack?.Invoke();
            return;
        }
        countLoseShowInter++;
        if (lstLoseShowInterRemote.Contains(countLoseShowInter))
        {
            
            ShowInterAds(callBack);

        }
        else
        {
            callBack?.Invoke();
        }
    }
    UnityAction callBack;
    public void ShowInterAds(UnityAction callBack = null)
    {
        
        this.callBack = callBack;
        if(UserDatas.Instance.IsRemoveAds)
        {
            this.callBack?.Invoke();
            this.callBack = null;
        }
        Action<InterVideoState> callbackInter = (state) =>
        {
            if (state == InterVideoState.Closed || state == InterVideoState.None)
            {
                Debug.Log("state = "+ state);
                this.callBack?.Invoke();
                this.callBack = null;
            }
        };
        AdManager.Instant.ShowInterstitial(callbackInter);
    }
    public void ActiveBaner(bool isShow)
    {
        if (UserDatas.Instance.IsRemoveAds) return;
        if (isShow)
        {
            AdManager.Instant.ShowBanner();
        }
        else
        {
            AdManager.Instant.DestroyBanner();
        }
    }
    public void BuyRemoveAds()
    {
        DVAH.AdManager.Instant.setOffAdPosition(true, DVAH.AD_TYPE.banner, DVAH.AD_TYPE.native,
               DVAH.AD_TYPE.open, DVAH.AD_TYPE.resume, DVAH.AD_TYPE.inter).
               DestroyBanner();
        UIController.Instance.ActionBuyNoAds();
    }
    public IEnumerator DelayAction(UnityAction action, float time)
    {
        yield return new WaitForSeconds(time);
        action?.Invoke();
    }
    public void CheckAndShowRate(Action<bool> action)
    {
        int level = UserDatas.Instance.CurrentLevel + 1;
        if (lstShowRateRemote.Contains(level) && !PlayerPrefs.HasKey(CONSTANT.RATE_CHECK))
        {
            DVAH3rdLib.Instant.ShowPopUpRate(true, action);
        }
        else
        {
            action?.Invoke(true);
        }
    }
}
