using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpShop : MonoBehaviour
{
    public void ActivePopup(bool isActive)
    {
        this.gameObject.SetActive(isActive);
    }
}
