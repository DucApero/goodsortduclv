using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Spine;
using Spine.Unity;

public class AnimationComponent : MonoBehaviour
{
    public SkeletonDataAsset[] skeletonDataAsset;
    [SerializeField] private SkeletonGraphic skeletonUI;
    public SkeletonAnimation skeletonAnimationUI
    {
        get
        {
            return skeleton;
        }
    }
    [SerializeField]
    private SkeletonAnimation skeleton;
    public SkeletonAnimation skeletonAnimation
    {
        get
        {
            return skeleton;
        }
    }
    private MeshRenderer mesh_renderer;
    private MeshRenderer meshRenderer
    {
        get
        {
            if (mesh_renderer == null)
            {
                mesh_renderer = skeletonAnimation.GetComponent<MeshRenderer>();
            }
            return mesh_renderer;
        }
    }
    private TrackEntry trackEntry;
    private TrackEntry trackEntry1;
    private Quaternion rotate;
    private bool is_first = true;

    private void ResetSkeletonData()
    {
        if (skeleton != null)
        {
            skeleton.ClearState();
            skeleton.skeletonDataAsset.Clear();
        }
    }
    public void SetStateSkeUI(int index_track, string name, bool is_loop, bool isClearState = false,
        Action action_complete = null )
    {
        if(isClearState)
        skeletonUI.Clear();
        
        TrackEntry trackEntryCache = skeletonUI.AnimationState.SetAnimation(index_track, name, is_loop);

        trackEntryCache.Complete += delegate
        {
            action_complete?.Invoke();
        };
    }
    public void SetState(int index_track, string name, bool is_loop,
        Action action_complete = null , Action actionEvent = null, Action[] actionSound = null)
    {
        skeleton.ClearState();
        //skeleton.initialSkinName = path;
        skeleton.Initialize(true);
        TrackEntry trackEntryCache = skeleton.AnimationState.SetAnimation(index_track, name, is_loop);

        trackEntryCache.Complete += delegate
        {
            action_complete?.Invoke();
        };
        trackEntryCache.Event += ((TrackEntry trackEntry, Spine.Event e) =>
        {
            if(e.Data.Name == "events")
            {
                actionEvent?.Invoke();
            }
            else if (e.Data.Name == "events_sound1")
            {
                actionSound[0]?.Invoke();
            }
            else if (e.Data.Name == "events_sound2")
            {
                actionSound[1]?.Invoke();
            }

        });

    }
    public void SetStateAnimation(int index_track, string name, bool is_loop, Action actionComplete)
    {
        skeleton.state.ClearTracks();
        trackEntry = skeleton.state.SetAnimation(index_track, name, is_loop);
        trackEntry.Complete += delegate 
        {
            actionComplete?.Invoke(); 
        };

    }

    public void SetRotate(bool is_rotate)
    {
        if (is_rotate)
        {
            skeleton.transform.localRotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            skeleton.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        if (is_first)
        {
            is_first = false;
            rotate = skeleton.transform.localRotation;
        }
    }
    public void SetTimeScale(float time_scale)
    {
        skeletonAnimation.timeScale = time_scale;
    }
    

    public void SetLayerSke(int index_gate)
    {
        if (meshRenderer == null) return;
        if (index_gate > 1 && index_gate < 6)
        {
            meshRenderer.sortingOrder = 5;
        }
        else
        {
            meshRenderer.sortingOrder = 1;
        }
    }
    public void SetSkeletonDataAsset(int index)
    {
        if(index < skeletonDataAsset.Length && index >= 0)
        {
            skeletonAnimation.skeletonDataAsset = skeletonDataAsset[index];
            skeletonAnimation.Initialize(true);
            ChangeStateAnim(AnimState.idle);
        }
    }
    public void SetSkinSkeletonDataAsset(int index)
    {
        string nameskin = "";
        switch (index)
        {
            case (int)NameSkin.skin1:
                nameskin = "skin1";
                break;
            case (int)NameSkin.skin2:
                nameskin = "skin2";
                break;
            case (int)NameSkin.skin3:
                nameskin = "skin3";
                break;
        }
        if (nameskin != "")
        {
            skeletonAnimation.initialSkinName = nameskin;
            skeletonAnimation.Initialize(true);
            ChangeStateAnim(AnimState.idle);
        }
        else
        {
            Debug.LogError("nameskin = None");
        }
    }

    public void ChangeStateAnim( AnimState state, int indexTrack = 0, bool isLoop = false, Action actionComplete = null, Action actionEvent = null, Action[] actionSound = null)
    {
        SetState(indexTrack, state.ToString(), isLoop, actionComplete, actionEvent, actionSound);
    }
    public void PauseAnim()
    {
        skeleton.AnimationState.TimeScale = 0;
    }
    public void UnPauseAnim()
    {
        skeleton.AnimationState.TimeScale = 1;
    }
    public string GetNameAnim()
    {
        if(skeleton != null)
        {
            return skeleton.AnimationName;
        }
        else
        {
            return "None";
        }
    }
    public enum NameSkin
    {
        skin1 = 0,
        skin2 = 1,
        skin3 = 2,
    }
    public enum AnimState
    {
        idle,

    }

}
