using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ShopQuyNX;
using CommonEnum;

[CreateAssetMenu(fileName = "ItemBoosterData", menuName = "BoosterInGame/ItemBoosterData", order = 1)]
[System.Serializable]
public class ItemBoosterData : ScriptableObject
{
    
    public List<ItemBooster> lstBooster;
    
}

[System.Serializable]
public class ItemBooster
{
    public Sprite SpriteBooster;
    public int price;
    public ItemGameplay type;
}
