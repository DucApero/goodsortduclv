using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlagSpawner : MonoBehaviour
{
    [SerializeField] private FlagDataContainer flagDataContainer;

    [SerializeField] private Flag flagObject;

    [SerializeField] private Transform flagContainer;

    protected Flag flagSelected;
    
    // Start is called before the first frame update
    void Start()
    {
        SpawnFlag();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SpawnFlag()
    {
        for(int i = 0; i < flagDataContainer.FlagContainer.Count; i++)
        {
            Flag flagObj = Instantiate(flagObject, flagContainer);
            flagObj.CallBackSelectFlag = (flagObj) =>
            {
                if (flagSelected)
                {
                    flagSelected.UnSelectFlag();
                }
                flagSelected = flagObj;
            };
            flagObj.Icon.sprite = flagDataContainer.FlagContainer[i].FlagIcon;
            flagObj.ID = flagDataContainer.FlagContainer[i].ID;
        }
    }
}
