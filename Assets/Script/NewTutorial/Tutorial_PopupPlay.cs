using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace TQT
{
    public class Tutorial_PopupPlay : MonoBehaviour
    {
        public GameObject bg_Tutorial, info_Booster;
        public Button closeTutorial;
        public ItemBoosterPlay_Tutorial skillA,skillB,skillC;
        public ItemBoosterPlay_Tutorial target;
        PopupBoosterPlay popupBooster;
        public TMP_Text titleBooster;
        public Transform arrow;
        public int indexTarget ;
        Tween tween;
        public void Start()
        {
            skillA.Init(this);
            skillB.Init(this);
            skillC.Init(this);
            popupBooster = GetComponent<PopupBoosterPlay>();
            closeTutorial.onClick.AddListener(HideTutorial);
            info_Booster.SetActive(false);
            bg_Tutorial.SetActive(false);
            closeTutorial.gameObject.SetActive(false);
        }
        public void Show()
        {
            if (UserDatas.Instance.CurrentLevel + 1 == skillA.levelUnlock)
                target = skillA;
            if (UserDatas.Instance.CurrentLevel + 1 == skillB.levelUnlock)
                target = skillB;
            if (UserDatas.Instance.CurrentLevel + 1 == skillC.levelUnlock)
                target = skillC;
            if (target!=null && target.IsTutorial)
            {
                if (!GameController.Instance.IsSelected)
                    GameController.Instance.IsSelected = true;
                info_Booster.SetActive(true);
                bg_Tutorial.SetActive(true);
                closeTutorial.gameObject.SetActive(true);
                target.StartTutorial(indexTarget);
                titleBooster.text = target.Name;
                AnimArrow();
            }
        }

        public void HideTutorial()
        {
            if (target != null && target.IsTutorial)
            {
                target.EndTutorial();
                //popupBooster.Onclick_ContinueTutorial();
                info_Booster.SetActive(false);
                bg_Tutorial.SetActive(false);
                closeTutorial.gameObject.SetActive(false);
                tween.Kill();
                target = null;
                popupBooster.Onclick_PlayGameTutorial();
                if (GameController.Instance.IsSelected)
                    GameController.Instance.IsSelected = false;
            }
              
        }
        public void AnimArrow()
        {
            arrow.localPosition = new Vector3(target.PosCurr.x, target.PosCurr.y, target.PosCurr.z);
            tween= arrow.DOLocalMove(target.PosNew, 0.5f)
                  .SetLoops(-1, LoopType.Yoyo);
        }




    }
}



