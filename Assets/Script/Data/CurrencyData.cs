using CommonEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "CurrencyData", menuName = "GameConfiguration/CurrencyData")]
public class CurrencyData : ScriptableObject
{
    [Serializable]
    public class Item
    {
        public Currency type;
        public Sprite icon;
        public int value;
    }

    public Item[] items;
}


