using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HubCurrency : MonoBehaviour
{
    [SerializeField] protected CommonEnum.TypeHub typeHub;
    [SerializeField] protected TextMeshProUGUI starNumberText;

    protected virtual void Awake()
    {
        Observer.Instance.AddObserver(typeHub.ToString(), UpdateValue);
    }
    private void OnEnable()
    {
    }
    protected virtual void OnDestroy()
    {
        Observer.Instance.RemoveObserver(typeHub.ToString(), UpdateValue);
    }
    public virtual void UpdateValue(object data)
    {
        string value = data.ToString();
        starNumberText.text = value;
    }
}
