using CustomAd;
using DG.Tweening;
using DVAH;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpLose : PopupUI
{
    [SerializeField] Button btnClose;
    [SerializeField] Button goldPurchaseButton;
    [SerializeField] Button adsPurchaseButton;
    [SerializeField] private int price;

    bool blockClick;
    private void OnEnable()
    {
        base.OnEnable();
        SoundController.Instance.PlayLoseSound();
        GameController.Instance.IsSelected = true;
        FireBaseManager.Instant.LogEventWithParameterAsync("fail_start", new Hashtable {
                {
                    "id_screen","gameplay"
                }
            });
    }

    private void OnDisable()
    {
        GameController.Instance.IsSelected = false;
    }

    private void Awake()
    {
        goldPurchaseButton.onClick.AddListener(() =>
        {
            if (blockClick) return;
            
            blockClick = true;
            SoundController.Instance.PlayClickButton();
            if(UserDatas.Instance.GetCurrency(CommonEnum.Currency.Gold) < price)
            {
                UIController.Instance.NotEnoughCoin();
                blockClick = false;
            }
            else
            {
                mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
                            {
                                blockClick = false;
                                GetTimeByGold(price);
                                ActivePopup(false);
                            });
                FireBaseManager.Instant.LogEventWithParameterAsync("fail_btn_gold", new Hashtable {
                {
                    "id_screen","gameplay"
                },{
                    "id_level",LevelManager.Instance.CurrentLevel.Id
                },
               
            });
            }
            
        });

        btnClose.onClick.AddListener(() =>
        {
            if (blockClick) return;
            blockClick = true;
            SoundController.Instance.PlayClickButton();

            mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                blockClick = false;
                ActivePopup(false);
                UIController.Instance.MoveToHome();
            });
        });
        adsPurchaseButton.onClick.AddListener(() =>
        {
            SoundController.Instance.PlayClickButton();
            if (blockClick) return;

            CustomAdManager.Intance.ShowRewardAds(() => blockClick = true,
                () =>
                {
                    mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
                    {
                        blockClick = false;
                        GetTimeByAds();
                        ActivePopup(false);
                    });
                    FireBaseManager.Instant.LogEventWithParameterAsync("fail_btn_reward", new Hashtable {
                    {
                    "id_screen","gameplay"
                    }
            });
                }
                , () => blockClick = false);
        });
        Debug.Log("init button");
     }

    public void ActivePopup(bool isActive)
    {
        this.gameObject.SetActive(isActive);
    }

    public void GetTimeByGold(int amount)
    {
        if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Gold) >= price)
        {
            UserDatas.Instance.SetCurrency(CommonEnum.Currency.Gold, -price);
            GameController.Instance.CountDownController.AddMoreTime(60);
            LevelManager.Instance.CurrentLevel.TimeLoseLevel += 60;
        }
        
        Debug.Log("click buy time");
        GameController.Instance.ActiveBaner(true);
    }

    public void GetTimeByAds()
    {
        GameController.Instance.CountDownController.AddMoreTime(60);
        LevelManager.Instance.IsLoseLevel = false;
        GameController.Instance.ActiveBaner(true);
        LevelManager.Instance.CurrentLevel.TimeLoseLevel += 60;
    }
    private void Update()
    {

    }
}
