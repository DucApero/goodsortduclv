using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "FlagDataContainer", menuName = "GameConfiguration/FlagDataContainer", order = 1)]

[System.Serializable]
public class FlagDataContainer : ScriptableObject
{
    public List<FlagData> FlagContainer;
}

[System.Serializable]
public struct FlagData
{
    public int ID;
    public Sprite FlagIcon;
}
