﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Spine.Unity;
using UnityEngine.UI;
using CustomAd;

public class TripleBox : BoxElement
{
    public SkinnedMeshRenderer boxMesh;
    public Button unlockBoxButton;
    private bool blockClick = false;
    public GameObject blackPanel;
    public void OnValidate()
    {
        boxType = BoxType.triple;
    }
    public AnimationComponent animationComponent;
    private void Start()
    {
        unlockBoxButton.onClick.AddListener(OnClickUnlockByAds);
    }

    public void Awake()
    {
        Observer.Instance.AddObserver(ObserverKey.ReplaceItem, ReplaceItemInfo);
        Observer.Instance.AddObserver(ObserverKey.AutoMatchItem, AutoMatchItem);
        Observer.Instance.AddObserver(ObserverKey.AutoMatchThreeItem, AutoMatchThreeTypeItem);
    }

    public void OnDestroy()
    {
        Observer.Instance.RemoveObserver(ObserverKey.ReplaceItem, ReplaceItemInfo);
        Observer.Instance.RemoveObserver(ObserverKey.AutoMatchItem, AutoMatchItem);
        Observer.Instance.RemoveObserver(ObserverKey.AutoMatchThreeItem, AutoMatchThreeTypeItem);
    }

    public override void SetLayerData(bool firstSpawn = false)
    {
        if (layers == null) return;
        if (layers.Count >= 1)
        {
            layerFist.LayerInfo = layers[0];
            if (firstSpawn)
            {
                layerFist.SpawnItem(true);
            }
            else
            {

            }
        }
        if(layers.Count >= 2)
        {
            layerSecond.LayerInfo = layers[1];
            layerSecond.SpawnItem(false);
            SetColliderLayerSecondOff();
        }
       
        
    }

    public override void ChangeLayerInfo()
    {
        CheckLayerInfoEmpty();
        layerFist.ChangeLayer(LayerSecond.ListItemContainer);
        if (layers == null) return;
        if (layers.Count > 0)
            layers.RemoveAt(0);
        
        if (layers.Count >= 2)
        {

            SetLayerData();
        }
            
    }

    public void DeleteLayerData(int idTarget)
    {
        if (layers == null) return;
        for (int i = 2; i < layers.Count; i++)
        {
            for (int j = 0; j < layers[i].itemSkin.Length; j++)
            {
                if (layers[i].itemSkin[j].id == idTarget)
                {
                    layers[i].itemSkin[j].id = 0;
                }
            }
        }
    }

    public void DeleteThreeLayerData(List<int> idTarget)
    {
        if (layers == null) return;
        for (int i = 2; i < layers.Count; i++)
        {
            for (int j = 0; j < layers[i].itemSkin.Length; j++)
            {
                foreach (int id_target in idTarget)
                {
                    if (layers[i].itemSkin[j].id == id_target)
                    {
                        layers[i].itemSkin[j].id = 0;
                    }
                }

            }
        }
    }

    public void CheckLayerInfoEmpty()
    {
        bool checkEmpty = false;
        if (layers == null) return;
        if (layers.Count > 2)
        {
            for (int i = 2; i < layers.Count; i++)
            {
                foreach (ItemInfo itemInfo in layers[i].itemSkin)
                {
                    if (itemInfo.id != 0)
                    {
                        checkEmpty = false;
                        break;
                    }
                    else checkEmpty = true;
                }
                if (checkEmpty)
                {
                    layers.RemoveAt(i);
                }
            }
        }

    }


    public void ChangeLayerData(int idItem, int value)
    {
        if (layers == null) return;
        foreach (LayerInfo layer in layers)
        {
            for(int i = 0; i < 3; i++)
            {
                if(layer.itemSkin[i].id == idItem)
                {
                    layer.itemSkin[i].id = value;
                }
            }
        }
    }

    public override void AddItem(ItemElement item)
    {
        if (LayerFisrt.IsLayerContainEmpty())
        {
            item.UpdateCurrentBox(this);
            LayerFisrt.AddItem(item);

            //UpdateLayer();
        }
        else
        {
            item.MoveToPrePos();
        }
    }

    public override void RemoveItem(ItemElement item)
    {
        LayerFisrt.RemoveItem(item);
    }

    public override void ChangeLayer()
    {
        ChangeLayerInfo();
    }

    public override void SetColliderLayerSecondOff()
    {
        foreach (ItemElement item in layerSecond.ListItemContainer)
        {
            if (item)
            {
                item.SetColliderOff();
                item.SetMaterialOff();
            }

        }
    }

    public void ReplaceItemInfo(object data)
    {
        Dictionary<int, int> Data = (Dictionary<int, int>)data;
        List<int> top3Item = Data.Keys.ToList();
        //bat ki index trong Data nào cũng giống nhau, vì đã get bên levelunit
        int targetReplaceId = Data.ElementAt(1).Value;

        layerFist.ReplaceItem(top3Item , targetReplaceId);
        layerSecond.ReplaceItem(top3Item , targetReplaceId);
        if (layers == null) return;
        if (layers.Count <= 2) return;
        if(layers.Count > 2)
        {
            for(int i = 2; i < layers.Count; i++)
            {
                foreach(int iditem in Data.Keys)
                {
                    for(int j = 0; j < layers[i].itemSkin.Length; j++)
                    {
                        if(iditem == layers[i].itemSkin[j].id)
                        {
                            
                            layers[i].itemSkin[j].id = targetReplaceId;
                        }
                    }
                }
            }
        }
    }

    public void AutoMatchItem(object data)
    {
        int targetItemID = (int)data;
        layerFist.AutoMatchItem(targetItemID);
        layerSecond.AutoMatchItem(targetItemID);
        DeleteLayerData(targetItemID);
        if (layerFist.IsLayerEmpty()) ChangeLayer();
        if (layerSecond.IsLayerEmpty()) ChangeLayer();
    } 

    public void AutoMatchThreeTypeItem(object data)
    {
        List<int> targetItemID = (List<int>)data;
        layerFist.AutoMatchThreeItem(targetItemID);
        layerSecond.AutoMatchThreeItem(targetItemID);
        DeleteThreeLayerData(targetItemID);
        if (layerFist.IsLayerEmpty()) ChangeLayer();
        if (layerSecond.IsLayerEmpty()) ChangeLayer();
    }

    public override void LockBox(bool lockBox)
    {
        if (lockBox)
        {
            lockObj.gameObject.SetActive(true);
            this.GetComponent<BoxCollider>().enabled = false;
            foreach (ItemElement item in layerFist.ListItemContainer)
            {
                if (item)
                {
                    item.SetColliderOff();
                }

            }
        }
        else
        {
            this.GetComponent<BoxCollider>().enabled = true;
            foreach (ItemElement item in layerFist.ListItemContainer)
            {
                if (item)
                {
                    item.SetColliderOn();
                }
            }
            IsLock = false;
        }
    }

    public override void SetLock(int value)
    {
        valueLockedNumber = value;
        valueLocked.text = valueLockedNumber.ToString();
        if(value <= 0)
        {
            LockBox(false);
        }
        UpdateLockBoxAnim(valueLockedNumber);
    }

    protected void UpdateLockBoxAnim(int index)
    {
        switch (index)
        {
            case 4:
                animationComponent.SetStateSkeUI(0,"2_xich",false, true);
                break;
            case 3:
                animationComponent.SetStateSkeUI(0, "1_xich", false, true);
                break;
            case 2:
                animationComponent.SetStateSkeUI(0, "0_xich", false, true);
                break;
            case 1:
                animationComponent.SetStateSkeUI(0, "nut_kinh", false, true);
                break;
            case 0:
                blackPanel.SetActive(false);
                animationComponent.SetStateSkeUI(0, "kinh_vo", false,true,()=> {
                    lockObj.gameObject.SetActive(false);
                });
                break;
            default:
                animationComponent.SetStateSkeUI(0, "2_xich", false, true);
                break;
        }
    }

    public override void DecreaseLockNumber()
    {
        valueLockedNumber--;
        if(valueLockedNumber <= 0)
        {
            LockBox(false);
        }
        if (valueLockedNumber == 0) valueLocked.gameObject.SetActive(false);
        valueLocked.text = valueLockedNumber.ToString();
        UpdateLockBoxAnim(valueLockedNumber);
    }
    
    public void OnClickUnlockByAds()
    {
        if (blockClick) return;

        CustomAdManager.Intance.ShowRewardAds(()=> 
        {
            blockClick = true;
        },()=> 
        {
            blockClick = false;
            valueLockedNumber = 0;
            LockBox(false);
            lockObj.gameObject.SetActive(false);
        },()=> { blockClick = false; });
    }

    public override void UpdateLayer()
    {
        if (LayerFisrt.IsLayerEmpty()) ChangeLayer();
    }

    public override bool IsLayerFirstContainEmpty()
    {
        return layerFist.IsLayerContainEmpty();
    }

    public override bool IsLayerFirstEmpty()
    {
        return layerFist.IsLayerEmpty();
    }

    public override void UpdateMeshBox(Material[] material)
    {
        boxMesh.material = material[1];
    }
}