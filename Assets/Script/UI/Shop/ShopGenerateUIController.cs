﻿using CommonEnum;
using DG.Tweening;
using ShopQuyNX;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ShopQuyNX.ShopItemData;

public class ShopGenerateUIController : MonoBehaviour
{
    [SerializeField] ShopQuyNX.ShopItemData shopData;
    [SerializeField] ShopQuyNX.ItemData itemData;
    [SerializeField] GameObject seasonPackPrefab, packPrefab, packPremiumPrefab, noAdsPrefab, smallItemPrefab, rewardAnItemPrefab;
    [SerializeField] Transform container;

    ItemShopToShow[] allItem;

    public static ShopGenerateUIController Instance;

    public ItemData ItemData { get => itemData; set => itemData = value; }

    private void OnEnable()
    {
        Instance = this;
        allItem = shopData.allItemShops;
        ClearChild();
        StartCoroutine(GenerateUI());
    }

    void ClearChild()
    {
        int totalChild = container.childCount;
        if (totalChild > 1 )
        {
            for(int i = 1; i < totalChild; i++)
            {
                Destroy(container.GetChild(i).gameObject);
            }
        }
    }

    IEnumerator GenerateUI()
    {
        foreach (ItemShopToShow itemShopToShow in allItem)
        {
            GameObject obj = null;

            switch (itemShopToShow.type)
            {
                case ItemShopType.SeasonPack:
                    GameObject seasonPack = Instantiate(seasonPackPrefab, container);
                    SeasonPassController seasonPassController = seasonPack.GetComponent<SeasonPassController>();
                    obj = seasonPack;
                    //Chưa làm

                    break;
                case ItemShopType.NoAds:
                    if (UserDatas.Instance.IsRemoveAds) break;
                    GameObject noAds = Instantiate(noAdsPrefab, container);
                    NoAdsController noAdsController = noAds.GetComponent<NoAdsController>();
                    noAdsController.iconSprite = itemShopToShow.noAdsInformation.icon;
                    noAdsController.price = itemShopToShow.noAdsInformation.priceIAP;
                    noAdsController.idIAP = itemShopToShow.noAdsInformation.idIAP;
                    noAdsController.coin = itemShopToShow.noAdsInformation.amountCoint;
                    obj = noAds;

                    break;
                case ItemShopType.LimitedPack:
                    if (UserDatas.Instance.IsHideLimitedBox) break;
                    GameObject packPremium = Instantiate(packPremiumPrefab, container);
                    PackController packPremiumController = packPremium.GetComponent<PackController>();
                    packPremiumController.title = itemShopToShow.limitedPack.title;
                    packPremiumController.isIAP = itemShopToShow.limitedPack.isIAP;
                    packPremiumController.priceIAP = itemShopToShow.limitedPack.priceIAP;
                    packPremiumController.idIAP = itemShopToShow.limitedPack.idIAP;
                    packPremiumController.idRewardAds = itemShopToShow.limitedPack.idRewardAds;
                    packPremiumController.totalMinuteToUnlock = itemShopToShow.limitedPack.totalMinuteToUnlock;
                    packPremiumController.items = itemShopToShow.limitedPack.itemShop;
                    packPremiumController.goldAmount = itemShopToShow.limitedPack.amountGold;
                    packPremiumController.isLimited = true;

                    obj = packPremium;

                    break;
                case ItemShopType.NormalPack:
                    GameObject pack = Instantiate(packPrefab, container);
                    PackController packController = pack.GetComponent<PackController>();
                    packController.title = itemShopToShow.normalPack.title;
                    packController.isIAP = itemShopToShow.normalPack.isIAP;
                    packController.priceIAP = itemShopToShow.normalPack.priceIAP;
                    packController.idIAP = itemShopToShow.normalPack.idIAP;
                    packController.idRewardAds = itemShopToShow.normalPack.idRewardAds;
                    packController.totalMinuteToUnlock = itemShopToShow.normalPack.totalMinuteToUnlock;
                    packController.items = itemShopToShow.normalPack.itemShop;
                    packController.goldAmount = itemShopToShow.normalPack.amountGold;

                    obj = pack;

                    break;
                default:
                    GameObject smallItem = Instantiate(smallItemPrefab, container);
                    SmallItemController smallItemController = smallItem.GetComponent<SmallItemController>();
                    smallItemController.isIAP = itemShopToShow.smallItem.isIAP;
                    smallItemController.isGold = itemShopToShow.smallItem.isGold;
                    smallItemController.idRewardAds = itemShopToShow.smallItem.idRewardAds;
                    smallItemController.totalMinuteToUnlock = itemShopToShow.smallItem.totalMinuteToUnlock;
                    smallItemController.idIAP = itemShopToShow.smallItem.idIAP;
                    smallItemController.priceIAP = itemShopToShow.smallItem.priceIAP;
                    smallItemController.itemInformation = itemShopToShow.smallItem.item;
                    smallItemController.amountCoin = itemShopToShow.smallItem.amountCoin;
                    obj = smallItem;

                    break;
            }

            if(obj != null) obj.transform.DOScale(.9f, 0.5f).From(0f);
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void ShowRewardAnItemPopup(Sprite iconSprite, int amount, bool isGold = true)
    {
        GameObject popup = Instantiate(rewardAnItemPrefab);
        RewardAnItemPopup rewardAnItemPopup = popup.GetComponent<RewardAnItemPopup>();
        rewardAnItemPopup.isGold = isGold;
        rewardAnItemPopup.amount = amount;
        rewardAnItemPopup.iconSprite = iconSprite;
    }
}
