using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "SkinContainer", menuName = "ShopUI/SkinContainer", order = 1)]
public class SkinContainer : ScriptableObject
{
    public BackGroundData[] listBackground;
}

[System.Serializable]
public class BackGroundData
{
    public Sprite backgroundPreview;
    public Material backgroundMaterial;
    public Material[] boxMaterial;
    public Sprite iconBg;
}
