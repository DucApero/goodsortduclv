using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountDownHub : MonoBehaviour
{
    public float countdownDuration; 
    public float CountDownDuration
    {
        get => countdownDuration;
        set => countdownDuration = value;
    }

    private float timeFreeze;
    public float TimeFreeze
    {
        get => timeFreeze;
        set => timeFreeze = value;
    }

    public TextMeshProUGUI countdownText; 

    private float currentTime;
    private bool statusCounter = true;
    private  bool isLose = false;
    private bool isActiveFreezeEffect;
    void Awake()
    {
        
    }

    void Update()
    {
        if (!GameController.Instance.IsStartLevel) return;

        if (currentTime > 0 && statusCounter && timeFreeze <= 0)
        {
            if(isLose)
            isLose = false;
            currentTime -= Time.deltaTime;
            UpdateCountdownText();
            if (isActiveFreezeEffect)
            {
                GamePlayUIEffect.Instance.HideFreezeEffect();
                isActiveFreezeEffect = false;
            }

        }
        else if(currentTime <= 0 && !isLose)
        {
            isLose = true;
            UIController.Instance.LoseLevel();
        }

        if(timeFreeze > 0)
        {
            if (!isActiveFreezeEffect)
            {
                GamePlayUIEffect.Instance.SpawnFreezeEffect(10f);
                isActiveFreezeEffect = true;
            }
            timeFreeze -= Time.deltaTime;
        }
    }
    public void ResetHub()
    {
        isActiveFreezeEffect = false;
        timeFreeze = 0;
        GamePlayUIEffect.Instance.DesTroyFreezeEffect();
    }
    private void OnDestroy()
    {
        
    }

    public void SetTimeFreeze(float amount)
    {
        timeFreeze = amount;
    }

    public void SetStatusCounter(object data)
    {
        statusCounter = (bool)data;
    }

    public void UpdateCountDownData(object data)
    {
        Debug.Log("update counter");
        float timeDuration = (float)data;
        countdownDuration = timeDuration;
        currentTime = timeDuration;
        UpdateCountdownText();
    }

    public void AddMoreTime(float amount)
    {
        currentTime += amount;
    }

    void UpdateCountdownText()
    {
        int minutes = Mathf.FloorToInt(currentTime / 60);
        int seconds = Mathf.FloorToInt(currentTime % 60);
        countdownText.text = string.Format("{0:00}:{1:00}", minutes, seconds);

        if (currentTime <= 0)
        {
            countdownText.text = "Time's up!";
        }
    }
}
