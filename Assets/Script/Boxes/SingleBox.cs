﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using TMPro;

public class SingleBox : BoxElement
{

    [SerializeField] private MeshRenderer boxMesh;
    [SerializeField] private Image slider;
    public int preCountItem;
    public int currentCountItem;
    public TextMeshProUGUI counter;
    public void OnValidate()
    {
        boxType = BoxType.single;
    }

    public void Awake()
    {
        Observer.Instance.AddObserver(ObserverKey.ReplaceItem, ReplaceItemInfo);
        Observer.Instance.AddObserver(ObserverKey.AutoMatchItem, AutoMatchItem);
        Observer.Instance.AddObserver(ObserverKey.AutoMatchThreeItem, AutoMatchThreeTypeItem);
    }

    private void Start()
    {
        SetPreAmountItem();
    }

    private void SetPreAmountItem()
    {
        foreach(LayerInfo layerInfoTemp in layers)
        {
            foreach(ItemInfo iteminfo in layerInfoTemp.itemSkin)
            {
                if(iteminfo.id != 0)
                {
                    preCountItem++;
                }
            }
        }
        currentCountItem = preCountItem;
        counter.text = currentCountItem.ToString();
    }

    public void UpdateSlider()
    {
        currentCountItem = lstIDItemHolder.Count;
        slider.fillAmount = (float)currentCountItem / preCountItem ;
        counter.text = currentCountItem.ToString();
    }

    public void OnDestroy()
    {
        Observer.Instance.RemoveObserver(ObserverKey.ReplaceItem, ReplaceItemInfo);
        Observer.Instance.RemoveObserver(ObserverKey.AutoMatchItem, AutoMatchItem);
        Observer.Instance.RemoveObserver(ObserverKey.AutoMatchThreeItem, AutoMatchThreeTypeItem);
    }

    public override void SetLayerData(bool firstSpawn = false)
    {
        if (layers == null) return;
        if (layers.Count >= 1)
        {
            layerFist.LayerInfo = layers[0];
            if (firstSpawn)
            {
                layerFist.SpawnItem(true);
            }
            else
            {

            }
        }
        if (layers.Count >= 2)
        {
            layerSecond.LayerInfo = layers[1];
            layerSecond.SpawnItem(false);
            SetColliderLayerSecondOff();
        }
    }
    public void ReplaceListHolder(List<int> lstTop3, int target) { 
        foreach(int value in lstTop3)
        {
            for(int i = 0; i < lstIDItemHolder.Count; i++)
            {
                if(lstIDItemHolder[i] == value)
                {
                    lstIDItemHolder[i] = target;
                }
            }
        }
    }
    public void RemoveIDInListHolder(int target)
    {
        while (lstIDItemHolder.Contains(target))
        {
            lstIDItemHolder.Remove(target);
        }
    }
    public void ReplaceItemInfo(object data)
    {
        Debug.Log("ReplaceItemInfo ----- ");

        Dictionary<int, int> Data = (Dictionary<int, int>)data;
        List<int> top3Item = Data.Keys.ToList();
        //bat ki index trong Data nào cũng giống nhau, vì đã get bên levelunit
        int targetReplaceId = Data.ElementAt(1).Value;
        ReplaceListHolder(top3Item, targetReplaceId);
        layerFist.ReplaceItem(top3Item, targetReplaceId);
        layerSecond.ReplaceItem(top3Item, targetReplaceId);
        if (layers == null) return;
        if (layers.Count <= 2) return;
        if (layers.Count > 2)
        {
            for (int i = 2; i < layers.Count; i++)
            {
                foreach (int iditem in Data.Keys)
                {
                    for (int j = 0; j < layers[i].itemSkin.Length; j++)
                    {
                        if (iditem == layers[i].itemSkin[j].id)
                        {

                            layers[i].itemSkin[j].id = targetReplaceId;
                        }
                    }
                }
            }
        }
    }

    public override void ChangeLayerInfo()
    {
        CheckLayerInfoEmpty();
        layerFist.ChangeLayer(LayerSecond.ListItemContainer);
        if (layers == null) return;
        if (layers.Count > 0)
            layers.RemoveAt(0);

        if (layers.Count >= 2)
        {

            SetLayerData();
        }

    }
    public void CheckLayerInfoEmpty()
    {
        if (layers == null) return;
        bool checkEmpty = false;
        if (layers.Count > 2)
        {
            for (int i = 2; i < layers.Count; i++)
            {
                foreach (ItemInfo itemInfo in layers[i].itemSkin)
                {
                    if (itemInfo.id != 0)
                    {
                        checkEmpty = false;
                        break;
                    }
                    else checkEmpty = true;
                }
                if (checkEmpty)
                {
                    layers.RemoveAt(i);
                }
            }
        }

    }

    public void DeleteLayerData(int idTarget)
    {
        if (layers == null) return;
        for (int i = 2; i < layers.Count; i++)
        {
            for (int j = 0; j < layers[i].itemSkin.Length; j++)
            {
                if (layers[i].itemSkin[j].id == idTarget)
                {
                    layers[i].itemSkin[j].id = 0;
                }
            }
        }
    }

    public void ChangeLayerData(int idItem, int value)
    {
        if (layers == null) return;
        foreach (LayerInfo layer in layers)
        {
            for (int i = 0; i < 3; i++)
            {
                if (layer.itemSkin[i].id == idItem)
                {
                    layer.itemSkin[i].id = value;
                }
            }
        }
    }
    public void AutoMatchThreeTypeItem(object data)
    {
        List<int> targetItemID = (List<int>)data;
        foreach(int idtarget in targetItemID)
        {
            RemoveIDInListHolder(idtarget);
        }
        layerFist.AutoMatchThreeItem(targetItemID);
        layerSecond.AutoMatchThreeItem(targetItemID);
        DeleteThreeLayerData(targetItemID);
        if (layerFist.IsLayerEmpty()) ChangeLayer();
        if (layerSecond.IsLayerEmpty()) ChangeLayer();
        UpdateSlider();
    }
    public void DeleteThreeLayerData(List<int> idTarget)
    {
        if (layers == null) return;
        for (int i = 2; i < layers.Count; i++)
        {
            for (int j = 0; j < layers[i].itemSkin.Length; j++)
            {
                foreach (int id_target in idTarget)
                {
                    if (layers[i].itemSkin[j].id == id_target)
                    {
                        layers[i].itemSkin[j].id = 0;
                    }
                }

            }
        }
    }
    public override void AddItem(ItemElement item)
    {
        if (LayerFisrt.IsLayerContainEmpty())
        {
            item.UpdateCurrentBox(this);
            LayerFisrt.AddItem(item);

            //UpdateLayer();
        }
        else
        {
            item.MoveToPrePos();
        }
    }


    public override void RemoveItem(ItemElement item)
    {
        LayerFisrt.RemoveItem(item);
        lstIDItemHolder.Remove(item.ID);
        UpdateSlider();
    }

    public override void ChangeLayer()
    {
        ChangeLayerInfo();
    }

    public override void SetColliderLayerSecondOff()
    {
        foreach (ItemElement item in layerSecond.ListItemContainer)
        {
            if (item)
            {
                item.SetColliderOff();
                item.gameObject.SetActive(false);
            }

        }
    }

    public void AutoMatchItem(object data)
    {
        int targetItemID = (int)data;
        RemoveIDInListHolder(targetItemID);
        layerFist.AutoMatchItem(targetItemID);
        layerSecond.AutoMatchItem(targetItemID);
        DeleteLayerData(targetItemID);
        if (layerFist.IsLayerEmpty()) ChangeLayer();
        if (layerSecond.IsLayerEmpty()) ChangeLayer();
        UpdateSlider();
    }

    public override void UpdateLayer()
    {
        if (LayerFisrt.IsLayerEmpty()) ChangeLayer();
    }

    public override bool IsLayerFirstContainEmpty()
    {
        return false;
    }

    public override bool IsLayerFirstEmpty()
    {
        return layerFist.IsLayerEmpty();
    }

    public override void UpdateMeshBox(Material[] material)
    {
        boxMesh.material = material[0];
    }
}
