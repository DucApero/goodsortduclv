using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CommonEnum;
using TMPro;
using DG.Tweening;
using CustomAd;

public class BuyBoosterInGame : PopupUI
{
    [SerializeField] private ItemBoosterData listBosterData;

    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private Image itemIcon;
    [SerializeField] private TextMeshProUGUI noti;

    [SerializeField] private int price;
    public int Price
    {
        get => price;
        set => price = value;
    }

    [SerializeField] private Button GoldButtonPurchase;
    [SerializeField] private TextMeshProUGUI GoldButtonPurchaseText;
    [SerializeField] private Button AdsButtonPurchase;
    [SerializeField] private Button btnClose;

    bool blockClick;

    private void Awake()
    {
        btnClose.onClick.AddListener(() =>
        {
            if (blockClick) return;
            blockClick = true;

            mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                blockClick = false;
                this.gameObject.SetActive(false);
            });
        });
    }
    bool isSelected;
    bool isStarLevel;
    private void OnEnable()
    {
        base.OnEnable();
        if (!GameController.Instance.IsSelected)
        {
            GameController.Instance.IsSelected = true;
            isSelected = true;
        }
        isStarLevel = GameController.Instance.IsStartLevel;
        GameController.Instance.IsStartLevel = false;

    }
    private void OnDisable()
    {
        if (isSelected)
        {
            GameController.Instance.IsSelected = false;
            isSelected = false;
        }
        GameController.Instance.IsStartLevel = isStarLevel;

    }

    public void SetUpPopup(CommonEnum.ItemGameplay typeBooster)
    {

        foreach (ItemBooster itemBooster in listBosterData.lstBooster)
        {
            if (typeBooster == itemBooster.type)
            {
                price = itemBooster.price;
                itemIcon.sprite = itemBooster.SpriteBooster;
            }
        }
        GoldButtonPurchase.onClick.RemoveAllListeners();
        AdsButtonPurchase.onClick.RemoveAllListeners();
        switch (typeBooster)
        {


            case ItemGameplay.SmallHammer:
                title.text = "Hint";
                noti.text = "Collects 3 tiles";
                GoldButtonPurchase.onClick.AddListener(BuyHintGold);
                AdsButtonPurchase.onClick.AddListener(BuyHintAds);
                break;
            case ItemGameplay.MagicWand:
                title.text = "Change";
                noti.text = "Change 9 Models";
                GoldButtonPurchase.onClick.AddListener(BuyChangeGold);
                AdsButtonPurchase.onClick.AddListener(BuyChangeAds);
                break;
            case ItemGameplay.Freeze:
                title.text = "Freeze";
                noti.text = "Freeze for 10 seconds";
                GoldButtonPurchase.onClick.AddListener(BuyFreezerGold);
                AdsButtonPurchase.onClick.AddListener(BuyFreezerAds);
                break;
            case ItemGameplay.Refresh:
                title.text = "Refresh";
                noti.text = "Refresh all models";

                GoldButtonPurchase.onClick.AddListener(BuyRefreshGold);
                AdsButtonPurchase.onClick.AddListener(BuyRefreshAds);
                break;
        }
        GoldButtonPurchaseText.text = price.ToString();
    }

    public void BuyHintGold()
    {
        SoundController.Instance.PlayClickButton();
        if (blockClick) return;

        if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Gold) - price > 0)
        {
            blockClick = true;
            mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                blockClick = false;

                UserDatas.Instance.SetCurrency(CommonEnum.Currency.Gold, -price);
                UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.SmallHammer, 1);
                UIController.Instance.ShowPopupReward(SetUpItemForShowReward(CommonEnum.ItemGameplay.SmallHammer), null, () =>
                {
                    this.gameObject.SetActive(false);
                });
            });
        }
        else
        {
            UIController.Instance.NotEnoughCoin();
        }
        Debug.Log("buy hint gold");

    }
    public void BuyHintAds()
    {
        SoundController.Instance.PlayClickButton();
        if (blockClick) return;

        CustomAdManager.Intance.ShowRewardAds(
            () => blockClick = true,
            () =>
            {
                mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
                {
                    blockClick = false;

                    Debug.Log("buy hint ads");
                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.SmallHammer, 1);

                    UIController.Instance.ShowPopupReward(SetUpItemForShowReward(CommonEnum.ItemGameplay.SmallHammer), null, () =>
                    {
                        this.gameObject.SetActive(false);
                    });
                });
            }
            , () => blockClick = false);
    }

    public List<ShopQuyNX.ItemData.Item> SetUpItemForShowReward(CommonEnum.ItemGameplay type)
    {
        List<ShopQuyNX.ItemData.Item> lstItem = new();
        ShopQuyNX.ItemData.Item item = new();
        item.itemId = type;
        item.value = 1;
        lstItem.Add(item);
        return lstItem;
    }

    public void BuyChangeGold()
    {
        SoundController.Instance.PlayClickButton();
        if (blockClick) return;

        if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Gold) - price > 0)
        {
            blockClick = true;
            mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                blockClick = false;

                Debug.Log("buy Change gold");
                UserDatas.Instance.SetCurrency(CommonEnum.Currency.Gold, -price);
                UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.MagicWand, 1);
                UIController.Instance.ShowPopupReward(SetUpItemForShowReward(CommonEnum.ItemGameplay.MagicWand), null, () =>
                {
                    this.gameObject.SetActive(false);
                });
            });
        }
        else
        {
            UIController.Instance.NotEnoughCoin();
        }
    }
    public void BuyChangeAds()
    {
        SoundController.Instance.PlayClickButton();
        if (blockClick) return;

        CustomAdManager.Intance.ShowRewardAds(
            () => blockClick = true,
            () =>
            {
                mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
                {
                    blockClick = false;

                    Debug.Log("buy Change ads");
                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.MagicWand, 1);
                    UIController.Instance.ShowPopupReward(SetUpItemForShowReward(CommonEnum.ItemGameplay.MagicWand), null, () =>
                    {
                        this.gameObject.SetActive(false);
                    });
                });
            }
            , () => blockClick = false);
    }
    public void BuyFreezerGold()
    {
        SoundController.Instance.PlayClickButton();
        if (blockClick) return;

        Debug.Log("buy Freezer gold");
        if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Gold) - price > 0)
        {
            blockClick = true;

            mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                blockClick = false;

                UserDatas.Instance.SetCurrency(CommonEnum.Currency.Gold, -price);
                UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Freeze, 1);
                UIController.Instance.ShowPopupReward(SetUpItemForShowReward(CommonEnum.ItemGameplay.Freeze), null, () =>
                {
                    this.gameObject.SetActive(false);
                });
            });
        }
        else
        {
            UIController.Instance.NotEnoughCoin();
        }
    }
    public void BuyFreezerAds()
    {
        SoundController.Instance.PlayClickButton();
        if (blockClick) return;

        CustomAdManager.Intance.ShowRewardAds(() => blockClick = true,
            () =>
            {
                mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
                {
                    blockClick = false;

                    Debug.Log("buy Freezer ads");
                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Freeze, 1);
                    UIController.Instance.ShowPopupReward(SetUpItemForShowReward(CommonEnum.ItemGameplay.Freeze), null, () =>
                    {
                        this.gameObject.SetActive(false);
                    });
                });
            }
            , () => blockClick = false);


    }
    public void BuyRefreshGold()
    {
        SoundController.Instance.PlayClickButton();
        if (blockClick) return;

        Debug.Log("buy refresh gold");
        if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Gold) - price > 0)
        {
            blockClick = true;
            mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                blockClick = false;
                UserDatas.Instance.SetCurrency(CommonEnum.Currency.Gold, -price);
                UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Refresh, 1);
                UIController.Instance.ShowPopupReward(SetUpItemForShowReward(CommonEnum.ItemGameplay.Refresh), null, () =>
                {
                    this.gameObject.SetActive(false);
                });
            });
        }
        else
        {
            UIController.Instance.NotEnoughCoin();
            UIController.Instance.GamePlayUI.RefreshButton.UpdateCountBooster();
            UIController.Instance.GamePlayUI.StuckPopup.UpdateCountBooster();
        }

    }
    public void BuyRefreshAds()
    {
        SoundController.Instance.PlayClickButton();
        if (blockClick) return;

        CustomAdManager.Intance.ShowRewardAds(() => blockClick = true,
            () =>
            {
                mainUI.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
                {
                    blockClick = false;

                    Debug.Log("buy refresh ads");
                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Refresh, 1);
                    UIController.Instance.ShowPopupReward(SetUpItemForShowReward(CommonEnum.ItemGameplay.Refresh), null, () =>
                    {
                        this.gameObject.SetActive(false);
                        UIController.Instance.GamePlayUI.RefreshButton.UpdateCountBooster();
                        UIController.Instance.GamePlayUI.StuckPopup.UpdateCountBooster();
                    });
                });
            }
            , () => blockClick = false);
    }
}
public enum TypeBooster
{
    Hint,
    Change,
    Freeze,
    Refresh
}
