using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    protected List<ItemElement> itemContainer;
    
    [SerializeField]
    protected int id;
    public int ID
    {
        get => id;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected virtual void AddItem(ItemElement item)
    {

    }

    protected virtual bool IsEmpty()
    {
        return false;
    }

    protected virtual void DeleteItem(int index)
    {

    }

    protected virtual void SortItemLayer()
    {

    }
}
