using DVAH;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CustomLib : MonoBehaviour
{
    Scene _currentScene;
    // Start is called before the first frame update
    void Start()
    {

        _currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive).allowSceneActivation = true;
        LoadingManager.Instant.DoneConditionSelf(0, () => SceneManager.GetSceneByName("GamePlay").isLoaded);
        LoadingManager.Instant.SetMaxTimeLoading(30).Init(1, (condition) =>
        {
            if (UserDatas.Instance.IsRemoveAds)
            {
                RemoveAds();
            }
            else
            {
                RestorePurchased();
                if (!UserDatas.Instance.IsRemoveAds)
                {
                    AdManager.Instant.InitializeBannerAdsAsync();

                    /* AdManager.Instant.ShowAdOpen(0, true, (id, state) =>
                     {
                         //SceneManager.UnloadSceneAsync(_currentScene);

                     });*/
                }
            }
            SceneManager.UnloadSceneAsync(_currentScene);
        });
    }
    void RestorePurchased()
    {
        _ = IAPManager.Instant.TryAddRestoreEvent("triplesort.iap.removeads", () =>
        {
            UserDatas.Instance.IsRemoveAds = true;
            RemoveAds();
            Debug.Log("ProcessPurchase_______Done");
        }, true);

    }
    public void RemoveAds()
    {
        GameController.Instance.BuyRemoveAds();
    }
}
