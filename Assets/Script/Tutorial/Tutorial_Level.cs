﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace TQT
{
    public class Tutorial_Level : MonoBehaviour
    {
        public const string Key_Level = "Level";
        [SerializeField] Transform hand, target;
        public Transform Hand => hand;
        [SerializeField] float delay;
        [SerializeField]
        List<ItemElement> items = new List<ItemElement>();
        Tween tween;
        float offset = 0.4f;
        bool isCanTut,isNextStep;
        public bool IsCanTut
        {
            get { return isCanTut; }
            set
            {
                isCanTut = value;
                PlayerPrefs.SetInt(Key_Level, isCanTut ? 1 : 0);
            }
        }
        private void Start()
        {
            isCanTut = PlayerPrefs.GetInt(Key_Level,1) != 0;
        }
        public void GameStart()
        {
            if (!IsCanTut)
                return;
            Observer.Instance.Notify(ObserverKey.ShowTutorial, true);
            SetListItem();
            Set_UIGame(true);
            StepFirst();
        }
      
        public void GameEnd()
        {
            if (!IsCanTut)
                return;
            IsCanTut = false;
            Set_UIGame(false);
            Observer.Instance.Notify(ObserverKey.ShowTutorial, false);
        }
        public void StepFirst()
        {
            target = items[1].transform;
            items[1].gameObject.GetComponent<BoxCollider>().enabled = true;
            SetUpHand();
        }

        public void NextStep()
        {
            if (!IsCanTut)
                return;
            isNextStep = true;
            target = items[0].transform;
            items[0].gameObject.GetComponent<BoxCollider>().enabled = true;
            SetUpHand();
        }
        public void Set_UIGame(bool show)
        {
            hand.gameObject.SetActive(show);
            UIController.Instance.GamePlayUI.Set_Object(!show);
        }
        public void SetListItem()
        {
            ItemElement[] obstacles = LevelManager.Instance.CurrentLevel.transform.GetComponentsInChildren<ItemElement>();
            for (int i = 0; i < obstacles.Length; i++)
            {
                if (!items.Contains(obstacles[i]))
                {
                    items.Add(obstacles[i]);
                }
            }
            for (int i = 0; i < items.Count; i++)
                items[i].gameObject.GetComponent<BoxCollider>().enabled = false;
        }
       

        public void SetUpHand()
        {
            if (!IsCanTut)
                return;
            StopAllCoroutines();
            tween.Kill();
            PosCurrent(target.position);

            if (!isNextStep)
            {
               StartCoroutine( MoveHand(new Vector3(PosItem(1, 0).x, (PosItem(1, 0).y), -3)));
            }
            else
            {
                StartCoroutine(MoveHand(new Vector3(PosItem(2, 2).x, (PosItem(2, 2).y), -3)));
            }
        }

        public void PosCurrent(Vector3 posCurrent)
        {
            hand.position = new Vector3(posCurrent.x+ offset, posCurrent.y,-3);
        }
        IEnumerator MoveHand(Vector3 targets)
        {
            yield return new WaitForSeconds(0.15f);
            tween = hand.DOMove(targets, delay)
                .OnComplete(() =>
                {
                    hand.DOMove(new Vector3(this.target.position.x+ offset, this.target.position.y ,-3), delay)
                    .OnComplete(() =>
                    {
                        SetUpHand();
                        //if (!isNextStep)
                        //{
                        //    StartCoroutine(MoveHand(new Vector3(PosItem(1, 0).x, (PosItem(1, 0).y), -3)));
                        //}
                        //else
                        //{
                        //    StartCoroutine(MoveHand(new Vector3(PosItem(2, 2).x, (PosItem(2, 2).y), -3)));
                        //}
                    });
                   
                });
        }

        public Vector3 PosItem(int indexBox, int indexPos)
        {
            return LevelManager.Instance.CurrentLevel.ListBoxElement[indexBox].LayerFisrt.ListItemPositions[indexPos].position;
        }
        public void SetActiveHand(bool show)
        {
            if (!IsCanTut)
                return;
            hand.gameObject.SetActive(show);
        }

    }
}

