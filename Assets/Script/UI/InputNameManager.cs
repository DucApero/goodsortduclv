using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InputNameManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI placeHolderText;
    [SerializeField] private TextMeshProUGUI textInner;
    [SerializeField] private TMP_InputField inputField;

    // Start is called before the first frame update
    void Start()
    {
        ChangeCharacterName();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangePlaceHolder(string text)
    {
        placeHolderText.text = text;
    }

    public void ChangeText(string text)
    {
        textInner.text = text;
    }

    public void ChangeCharacterName()
    {
        inputField.onValueChanged.AddListener(delegate { 
            ConfigGameController.Instance.SetName(inputField.text);
            
        });
        inputField.text = UserDatas.Instance.GetUserData().Name;
        inputField.onEndEdit.AddListener(delegate
        {
            UserDatas.Instance.SetNameUser(inputField.text);

        });

    }
}
