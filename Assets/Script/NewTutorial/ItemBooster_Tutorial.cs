using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using TMPro;


  namespace TQT
{
    public class ItemBooster_Tutorial : MonoBehaviour
    {
        public const string Key_Bosster = "Booster";
        public string Name;
        [SerializeField] Image imgLight;
        public Vector3 PosNew, PosCurr;
        public Transform targetItemMove;
        [SerializeField] float delay=0.2f;
        bool isTutorial;
        public Sprite imgItem;
        public Sprite sprlock;
        public UnityEvent onClick;
        public int id;
        ItemBooster_GamePlay itemBooster_GamePlay;
        public int siblingIndexCurr;
        public int levelUnlock;
   
        public bool IsTutorial
        {
            get { return isTutorial; }
            set 
            {
                isTutorial = value;
                PlayerPrefs.SetInt(Key_Bosster + name, isTutorial ? 1 : 0);
            }
        }
        public void Init()
        {
            gameObject.SetActive(true);
            isTutorial = PlayerPrefs.GetInt(Key_Bosster + name, 1)!=0;
            siblingIndexCurr = transform.GetSiblingIndex();
            itemBooster_GamePlay = GetComponent<ItemBooster_GamePlay>();
            levelUnlock = itemBooster_GamePlay.CurrentItem.requiredLevel;
            imgLight.gameObject.SetActive(false);
        }
        public void StartTutorial(int index)
        {
            if (!IsTutorial)
                return;
            transform.SetSiblingIndex(index);
            imgLight.gameObject.SetActive(true);
            itemBooster_GamePlay.addIcon.SetActive(false);
            StartBlink();
          

        }
        public void EndTutorial()
        {
            IsTutorial = false; 
            itemBooster_GamePlay.isLock = false;
            SetUiBooster();
            transform.SetSiblingIndex(siblingIndexCurr);
            imgLight.gameObject.SetActive(false);
            itemBooster_GamePlay.addIcon.SetActive(true);
            onClick?.Invoke();

        }

        public void SetUiBooster()
        {
            if (id == 0)
            {
                UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.SmallHammer, 4);
                UIController.Instance.GamePlayUI.DrumButton.UpdateCountBooster();
             
            }
            else if (id == 1)
            {
                UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Refresh, 4);
                UIController.Instance.GamePlayUI.RefreshButton.UpdateCountBooster();
            }
            else if (id == 2)
            {
                UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.MagicWand, 4);
                UIController.Instance.GamePlayUI.MagicWand.UpdateCountBooster();
            }
            else if (id == 3)
            {
                UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Freeze, 4);
                UIController.Instance.GamePlayUI.FreezerButton.UpdateCountBooster();
                GameController.Instance.IsStartLevel = true;
            }
        }

        public void StartBlink()
        {
            InvokeRepeating("ToggleState", delay, 0.5f);

        }
        public void ToggleState()
        {
            imgLight.enabled = !imgLight.enabled;
        }
       
    }
}

