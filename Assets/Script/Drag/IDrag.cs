using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDrag
{
    void MouseDown();
    void MouseUp();
    void MouseDrag();
}
