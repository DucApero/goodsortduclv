using CommonEnum;
using ShopQuyNX;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemCurrency : MonoBehaviour
{
    [SerializeField] CurrencyData _data;
    [SerializeField] Image icon;
    [SerializeField] TMP_Text amountTxt;

    [HideInInspector] public Currency type;
    [HideInInspector] public string amount;

    private void Start()
    {
        icon.sprite = GetDataById().icon;
        amountTxt.text = amount;
    }

    CurrencyData.Item GetDataById()
    {
        CurrencyData.Item itemData = _data.items.Where(item => type == item.type).FirstOrDefault();
        return itemData;
    }
}
