using CommonEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShopQuyNX
{
    [CreateAssetMenu(fileName = "ItemData", menuName = "GameConfiguration/ItemData")]
    public class ItemData : ScriptableObject
    {
        [Serializable]
        public class Item
        {
            public string id;
            public int requiredLevel;
            public ItemGameplay itemId;
            public Sprite icon;
            public int value;
        }

        public Item[] items;
    }

}


