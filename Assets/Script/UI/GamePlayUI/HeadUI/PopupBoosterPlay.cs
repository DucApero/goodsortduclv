using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DVAH;

public class PopupBoosterPlay : PopupUI
{
    private ShopQuyNX.ItemData itemData; 
    [SerializeField] private Button btnPlay;
    [SerializeField] private Button btnClose;
    [SerializeField] private List<ItemBoosterPlayGame> lstItemBooster = new List<ItemBoosterPlayGame>();
    [SerializeField] private TextMeshProUGUI txtLevel;
    bool blockClick;
    bool isPlayWin;
    private UnityAction actionContinue;
    private void Awake()
    {
        itemData = GameController.Instance.ItemDataGamePlay;
        btnPlay.onClick.AddListener(()=>
        {
            Debug.Log("click play");
            FireBaseManager.Instant.LogEventWithParameterAsync("popup_start_play", new Hashtable {
                {
                    "id_screen","home"
                }
            });
            if (blockClick) return;
            blockClick = true;
            SoundController.Instance.PlayClickButton();
            
            mainUI.DOScale(0f, 0.5f).From(1f).SetEase(Ease.InBack).OnComplete(() =>
            {
                int timeInfi = GameController.Instance.TimeInfinityRemaining;
                if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) <= 0 && !isPlayWin && timeInfi <= 0)
                {
                    blockClick = false;
                    ActivePopup(false);
                    UIController.Instance.HomeUI.ShowBuyHeartPopup(null);
                }
                else
                {
                    SoundController.Instance.PlayBackgroundGameplay();
                    actionContinue?.Invoke();
                    if(!isPlayWin && timeInfi <= 0)
                    UIController.Instance.DecreaseHeartOnClickPlay();
                    blockClick = false;
                    ActivePopup(false);
                }
            });
        });
        btnClose.onClick.AddListener(()=> 
        {
            if (blockClick) return;
            blockClick = true;
            SoundController.Instance.PlayClickButton();

            mainUI.DOScale(0f, 0.5f).From(1f).SetEase(Ease.InBack).OnComplete(() =>
            {
                Debug.Log("close button");
                blockClick = false;
                GameController.Instance.ResetCountDownHub();
                UIController.Instance.MoveToHome();
                ActivePopup(false);
                Debug.Log("close button");
            });
        });
        SetInfoItem();
    }
    private void OnEnable()
    {
        base.OnEnable();
        GameController.Instance.LstBoosterInLevel.Clear();
        UpdateTextTitle(LevelManager.Instance.IndexLevel + 1);
        SetInfoItem();
        
    }
    public void Onclick_PlayGameTutorial()
    {
        SoundController.Instance.PlayClickButton();
        FireBaseManager.Instant.LogEventWithParameterAsync("home_btn_level", new Hashtable {
                {
                    "id_screen",1
                }
            });
        SoundController.Instance.PlayBackgroundGameplay();
        actionContinue?.Invoke();
        ActivePopup(false);
    }
    public void Onclick_ContinueTutorial()
    {
        SoundController.Instance.PlayBackgroundGameplay();
        actionContinue?.Invoke();
        UIController.Instance.DecreaseHeartOnClickPlay();
        blockClick = false;
        ActivePopup(false);
    }
    public void UpdateTextTitle(int level)
    {
        txtLevel.text = "Level " + level.ToString();
    }
    public void ActivePopup(bool isActive , UnityAction action = null, bool isHomeUI = false)
    {
        if (isActive)
        {
            TQT.TutorialControler.Instance.SetState_TutorialPopupPlay();
        }
        actionContinue = action;
        this.gameObject.SetActive(isActive);
        this.isPlayWin = isHomeUI;
    }
    
    public void SetInfoItem()
    {
        for(int i = 0; i< lstItemBooster.Count; i++)
        {
            // Spawn Item;
        }
        foreach(ShopQuyNX.ItemData.Item item_data in itemData.items)
        {
            if(item_data.itemId == CommonEnum.ItemGameplay.BigHammer)
            {
                lstItemBooster[0].SetInfo(item_data);
            }
            
            if(item_data.itemId == CommonEnum.ItemGameplay.GreatFreeze)
            {
                lstItemBooster[1].SetInfo(item_data);
            }
            if(item_data.itemId == CommonEnum.ItemGameplay.DoubleStar)
            {
                lstItemBooster[2].SetInfo(item_data);
            }
        }
        
    }
}
