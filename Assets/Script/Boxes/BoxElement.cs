using CommonEnum;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BoxElement : MonoBehaviour
{
    [SerializeField] protected GameObject lockObj;
    public List<int> lstIDItemHolder = new List<int>();
    protected int valueLockedNumber;
    public int ValueLockedNumber
    {
        get => valueLockedNumber;
        set => valueLockedNumber = value;
    }
    [SerializeField] protected TextMeshProUGUI valueLocked;
    public TextMeshProUGUI ValueLocked
    {
        get => valueLocked;
        set => valueLocked = value;
    }
    private bool isLock;
    public bool IsLock
    {
        get => isLock;
        set => isLock = value;
    }
    public DirectionBox direction;
    [SerializeField]
    protected Animator animator;
    protected int maxLayer;
    [SerializeField]
    protected Layer layerFist;
    [SerializeField]
    protected Layer layerSecond;
    [SerializeField]
    protected List<ItemElement> itemContainer = new();

    [SerializeField]
    protected int id;
    public int ID
    {
        get => id;
    }

    [SerializeField] protected BoxType boxType;
    public BoxType BoxType
    {
        get => boxType;
    }

    public List<LayerInfo> layers = new List<LayerInfo>();
    public List<LayerInfo> LayerInfo
    {
        get => layers;
        set => layers = value;
    }
    public List<ItemElement> ItemContainer
    {
        get => itemContainer;
        set => itemContainer = value;
    }

    public LayerInfo GetLayerInfoAt(int index)
    {

        return layers[index];
    }

    public Layer LayerFisrt
    {
        get
        {
            return this.layerFist;
        }
        set
        {
            this.layerFist = value;
        }
    }

    public Layer LayerSecond
    {
        get
        {
            return this.layerSecond;
        }
        set
        {
            this.layerSecond = value;
        }
    }

    public virtual void SetLayerData(bool firstSpawn = false)
    {

    }

    public virtual void ChangeLayerInfo()
    {

    }

    public virtual void AddItem(ItemElement item)
    {

    }

    public virtual void RemoveItem(ItemElement item)
    {

    }

    public virtual void ChangeLayer()
    {

    }

    public virtual void SetColliderLayerSecondOff()
    {

    }

    public virtual void UpdateLayer()
    {

    }
    public virtual void LockBox(bool lockBox)
    {

    }
    public virtual bool IsLayerFirstContainEmpty()
    {
        return false;
    }

    public virtual bool IsLayerFirstEmpty()
    {
        return false;
    }
    public virtual void SetLock(int value)
    {

    }

    public virtual void DecreaseLockNumber()
    {

    }

    public void ActiveAnimation()
    {
        try
        {
            if (direction == DirectionBox.Left) animator.SetTrigger("L");
            else if (direction == DirectionBox.Right) animator.SetTrigger("R");
            else animator.SetTrigger(Random.value > 0.5f ? "L" : "R");
        }
        catch { }
    }

    public virtual void UpdateMeshBox(Material[] material)
    {
        
    }
}


public enum BoxType
{
    triple,
    single
}
