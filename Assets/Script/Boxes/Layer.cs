using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Layer : MonoBehaviour
{
    [SerializeField]
    protected int id;
    
    protected int maxItemLayer;

    [SerializeField]
    protected int numberItem = 3;
    [SerializeField]
    protected List<Transform> listItemPositions = new();
    [SerializeField]
    protected List<ItemElement> listItemContainer;

    [SerializeField] protected BoxElement currentBox;
    protected LayerInfo layerInfo;
    public LayerInfo LayerInfo
    {
        set => layerInfo = value;
    }

    public BoxElement CurrentBox
    {
        get => currentBox;
        set => currentBox = value;
    }

    public List<ItemElement> ListItemContainer
    {
        get
        {
            return listItemContainer;
        }
        set
        {
            listItemContainer = value;
        }
    }

    public List<Transform> ListItemPositions
    {
        get
        {
            return listItemPositions;
        }
        set
        {
            listItemPositions = value;
        }
    }

    private void Awake()
    {
        InitItemContainer();
    }

    private void InitItemContainer()
    {
        listItemContainer = new();
        for (int i = 0; i < numberItem; i++)
        {
            
            listItemContainer.Add(null);
        }
    }

    public virtual void SpawnItem(bool isLayerFirst)
    {

    }

    public virtual void ReplaceItem(List<int> top3Item, int targetReplaceId)
    {

    }

    public virtual void AutoMatchItem(int targetID)
    {

    }  
    public virtual void AutoMatchThreeItem(List<int> targetID)
    {

    }

    public virtual void RemoveItem(ItemElement item)
    {

    }
    public virtual bool IsLayerEmpty()
    {
        return false;
    }

    public virtual void ChangeLayer(List<ItemElement> items)
    {

    }

    public virtual bool CheckEmpty(int index)
    {
        return false;
    }

    public virtual void AddItem(ItemElement item)
    {

    }

    public virtual bool CheckMatch()
    {
        return false;
    }

    public virtual void MatchListItem()
    {
        
    }

    public virtual void CheckStatusGame()
    {

    }

    public virtual void DeleteItem(int index)
    {

    }
    public virtual bool IsLayerContainTwoItemHasSameID()
    {
        return false;
    }
    public virtual bool IsLayerContainEmpty()
    {
        return false;
    }
    public virtual bool IsFirstItemInLayerContainEmpty()
    {
        return false;
    }

    public virtual void TurnOnAllMaterial()
    {

    }
}
public enum LayerType
{
    layerFirst,
    layerSecond
}
