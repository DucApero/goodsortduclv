using DG.Tweening;
using DVAH;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NoAds : PopupUI
{
    [SerializeField] ShopQuyNX.ShopItemData data;
    [SerializeField] Button btnClose;
    [SerializeField] Button btnBuyNoAds;
    [SerializeField] TextMeshProUGUI txtPrice;
    private ShopQuyNX.ShopItemData.NoAdsItem noAds;
    private bool isPlaying;

    private void OnEnable()
    {
        base.OnEnable();
        if (GameController.Instance.IsStartLevel)
        {
            isPlaying = true;
            GameController.Instance.IsStartLevel = false;
        }
    }

    private void OnDisable()
    {
        if (isPlaying) GameController.Instance.IsStartLevel = true;
    }

    private void Awake()
    {
        noAds = data.allItemShops.Where(item => item.type == CommonEnum.ItemShopType.NoAds).FirstOrDefault().noAdsInformation;
        btnClose.onClick.AddListener(()=>
        {
            SoundController.Instance.PlayClickButton();

            mainUI.DOScale(0f, 0.5f).From(1f).SetEase(Ease.InBack).OnComplete(() =>
            {
                ActivePopup(false);
            });
        });
        btnBuyNoAds.onClick.AddListener(()=> 
        {
            IAPManager.Instant.BuyProductID(noAds.idIAP, (isSuccess) =>
            {
                if (isSuccess) BuyNoAds();
            });
        });
        SetInfo();
    }
    public void SetInfo()
    {
        txtPrice.text = noAds.priceIAP.ToString() + "$";
    }

    public void ActivePopup(bool isActive)
    {
        this.gameObject.SetActive(isActive);
        GameController.Instance.IsSelected = isActive;
    }
    public void BuyNoAds()
    {
        List<UserDatas.Currency> currencies = new List<UserDatas.Currency>();
        UserDatas.Currency currency = new(noAds.currency, noAds.amountCoint);
        currencies.Add(currency);
        ActivePopup(false);
        UserDatas.Instance.IsRemoveAds = true;
        GameController.Instance.BuyRemoveAds();
        UIController.Instance.ShowPopupReward(null,currencies);
    }
}
