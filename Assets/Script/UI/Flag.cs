using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flag : MonoBehaviour
{
    public int id;
    public int ID
    {
        get
        {
            return id;
        }
        set
        {
            id = value;
        }
    }
    [SerializeField] private Image icon;
    public Image Icon
    {
        get => icon;
        set => icon = value;
    }

    [SerializeField] private Image selectedImage;

    private Action<Flag> callBackSelectFlag;
    public Action<Flag> CallBackSelectFlag
    {
        get => callBackSelectFlag;
        set => callBackSelectFlag = value;
    }

    // Start is called before the first frame update
    void Start()
    {
        InitButton();
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    private void InitButton()
    {
        this.gameObject.GetComponent<Button>().onClick.AddListener(SelectFlagIcon);
    }

    public void SelectFlagIcon()
    {
        Observer.Instance.Notify(ObserverKey.ChangeAvatar, id);
        callBackSelectFlag?.Invoke(this);
        SelectFlag();
        SoundController.Instance.PlayClickButton();
    }

    public void SelectFlag()
    {
        selectedImage.gameObject.SetActive(true);
    }

    public void UnSelectFlag()
    {
        selectedImage.gameObject.SetActive(false);
    }
}
