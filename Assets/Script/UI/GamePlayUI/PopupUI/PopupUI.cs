using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupUI : MonoBehaviour
{
    [SerializeField] protected Transform mainUI;
    [SerializeField] protected Button closeButton;
    protected void OnEnable()
    {
        mainUI.DOScale(1f, 0.5f).From(0f).SetEase(Ease.OutBack);
    }

    private void Start()
    {
        if(closeButton)
        closeButton.onClick.AddListener(() =>
        {
            this.gameObject.SetActive(false);
            SoundController.Instance.PlayClickButton();
        });
    }
}
