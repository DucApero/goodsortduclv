﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RandomItems : MonoBehaviour
{

    [SerializeField] private ListLevelData listLevelData;
    public ListLevelData levelsData
    {
        get => listLevelData;
    }
    [SerializeField] private bool autoSpawn = false;
    [SerializeField] private int countBox = 12;
    [SerializeField] private int countTypeBox = 1;
    [SerializeField] private int countItem = 84;
    [SerializeField] private int countLayer = 36; // so layermini trong 1 level
    [SerializeField] private float timeDuration = 100;
    [SerializeField] private List<int> BoxOneItem = new List<int>(); // so box 1 trong trong  1 Phase
    [SerializeField] private int maxIdItemInLevel = 3; // so item cung id xuat hien trong level
    [SerializeField] private const int maxItemInLayerBox = 3;  // so item max cua layermini trong box
    [SerializeField] private const int oneEmpty = 1;
    [SerializeField] private const int twoEmpty = 2;
    [SerializeField] private int[] maxTypeItemLayer = new int[3]; // so id item xuat hien trong 1 Phase
    [SerializeField] private int[] maxTypeItemBoxOneInLayer = new int[3]; // so id item box one xuat hien trong 1 Phase
    [SerializeField] private int[] emptyInLayer = new int[3]; // so o trong trong  1 Phase


    [SerializeField] private List<int> lstIdItemRandom = new List<int>();
    [SerializeField] private List<int> lstIdItemSortRandom = new List<int>();
    public List<int> LstIdItemRandom { get => lstIdItemRandom; }

    [SerializeField] private List<int> lstIdItemRandomFake = new List<int>();
    [SerializeField] private List<BoxElement> listBoxElement = new List<BoxElement>();
    [SerializeField] private List<LayerItem> listLayerItem = new List<LayerItem>();
    [SerializeField] private List<ItemElement> listItemElement = new List<ItemElement>();
    [SerializeField] private List<LayerInfo> layerInfos = new List<LayerInfo>();
    [SerializeField] private List<ItemInfo> itemInfos = new List<ItemInfo>();
    [SerializeField] private List<ItemInfo> itemInfosFake = new List<ItemInfo>();

    private List<int> lstItemSpecial = new List<int> { 2, 4 };
    private bool isSort = false;
    private bool isSortWithCondition = true; // condition: k qua 3 id tren mot layer
    private int idItemChange = 0;
    private int countItemSort = 84;
    private List<int> lstIdItemInLayer = new List<int>();
    private List<int> lstIdItemOneBosInLayer = new List<int>();
    [SerializeField] private Dictionary<int, int> dicIdItemInLayer = new Dictionary<int, int>();

    private Dictionary<int, int> dicTest = new Dictionary<int, int>();
    private Dictionary<int, int> dicTest1 = new Dictionary<int, int>();
    private Dictionary<int, List<LayerInfo>> dicLayer = new Dictionary<int, List<LayerInfo>>();
    private List<int> lstIdOnlyTwoItem = new List<int>();
    private List<int> lstIdItemSpecial = new List<int>();
    ItemInfo itemInfoFake;

    private List<Dictionary<int, int>> LstIdItemNotEnough = new List<Dictionary<int, int>>();
    private Dictionary<int, List<int>> dicLayerMatchLayerNotEnough = new Dictionary<int, List<int>> 
    { 
        { 1, new List<int> { 1 } } ,
        { 2, new List<int> { 2 } } ,
        { 3, new List<int> { 3 } } ,
        { 4, new List<int> { 4 } } ,
        { 5, new List<int> { 5 } } ,
        { 6, new List<int> { 6 } } ,
        { 7, new List<int> { 7 } } ,
        { 8, new List<int> { 8 } } 
    };
    private void Awake()
    {
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
        }
    }
    private void Start()
    {
        //Run();
        Observer.Instance.AddObserver(ObserverKey.UpdateListSortItem, MatchItem);
        Observer.Instance.AddObserver(ObserverKey.ReplaceItem, ReplaceListSort);
        Observer.Instance.AddObserver(ObserverKey.UpdateItemSpecial, UpdateListItemSpecial);
    }
    private void OnDestroy()
    {
        Observer.Instance.RemoveObserver(ObserverKey.UpdateListSortItem, MatchItem);
        Observer.Instance.RemoveObserver(ObserverKey.ReplaceItem, ReplaceListSort);
        Observer.Instance.RemoveObserver(ObserverKey.UpdateItemSpecial, UpdateListItemSpecial);
    }
    public void UpdateListItemSpecial(object data)
    {
        if (lstItemSpecial.Count <= 0) return;
        for (int i = lstItemSpecial.Count - 1; i < 0; i--)
        {
            if (lstItemSpecial[i] <= 0)
            {
                lstItemSpecial.RemoveAt(i);
                continue;
            }

            lstItemSpecial[i]--;
        }
    }
    public float GetTimeLevelData()
    {
        return timeDuration;
    }

    public List<ItemElement> lstItem(int idLevel)
    {
        for (int i = 0; i < listLevelData.ListLevel.Count; i++)
        {
            if (listLevelData.ListLevel[i].Id == idLevel)
            {
                return listLevelData.ListLevel[i].LstIdItemRandom;
            }
        }
        return null;
    }
    public void SetRandomItemData(int idLevel)
    {
        LevelData levelData = null;
        for (int i = 0; i < listLevelData.ListLevel.Count; i++)
        {
            if (listLevelData.ListLevel[i].Id == idLevel)
            {
                levelData = listLevelData.ListLevel[i];
                break;
            }
        }
        if (levelData == null)
        {
            Debug.LogError(">>>>>>>>>> No Data level <<<<<<<<<<<");
            return;
        }

        countBox = levelData.CountBox;
        countItem = levelData.CountItem;
        countLayer = levelData.CountLayer;
        countItemSort = countItem;
        timeDuration = levelData.timeDuration;
        int maxTypeItem = levelData.MaxTypeItemLayer.Length;
        maxTypeItemLayer = new int[maxTypeItem];
        for (int i = 0; i < maxTypeItem; i++)
        {
            maxTypeItemLayer[i] = levelData.MaxTypeItemLayer[i];
        }

        int emptyLayer = levelData.EmptyInLayer.Length;
        emptyInLayer = new int[emptyLayer];
        for (int i = 0; i < emptyLayer; i++)
        {
            emptyInLayer[i] = levelData.EmptyInLayer[i];
        }

        lstIdItemRandom.Clear();
        lstIdItemSortRandom.Clear();
        lstItemSpecial.Clear();
        int countItemSpecial = levelData.lstItemSpecial.Count;
        for (int i = 0; i < countItemSpecial; i++)
        {
            lstItemSpecial.Add(levelData.lstItemSpecial[i]);
        }
        int listIdItemRandom = levelData.LstIdItemRandom.Count;
        for (int i = 0; i < listIdItemRandom; i++)
        {
            lstIdItemRandom.Add(levelData.LstIdItemRandom[i].ID);
            lstIdItemSortRandom.Add(levelData.LstIdItemRandom[i].ID);
        }

        int boxOneItemLength = levelData.BoxOneItem.Count;
        BoxOneItem.Clear();
        for (int i = 0; i < boxOneItemLength; i++)
        {
            BoxOneItem.Add(levelData.BoxOneItem[i]);
        }

        int maxTypeInLayer = levelData.maxTypeItemBoxOneInLayer.Length;
        maxTypeItemBoxOneInLayer = new int[maxTypeInLayer];
        for (int i = 0; i < maxTypeInLayer; i++)
        {
            maxTypeItemBoxOneInLayer[i] = levelData.maxTypeItemBoxOneInLayer[i];
        }
    }
    public void MatchItem(object data)
    {
        int id = (int)data;
        RemoveListSort(id);
        if (lstIdItemSpecial.Contains(id))
        {
            lstIdItemSpecial.Remove(id);
        }
    }
    public void ReplaceListSort(object data)
    {
        Dictionary<int, int> dic = (Dictionary<int, int>)data;
        foreach (var id in dic)
        {
            UpdateListSort(id.Key, id.Value);
        }

    }
    private void UpdateListSort(int id, int idTarget)
    {
        string s = "";
        idItemChange = idTarget;
        for (int i = 0; i < lstIdItemSortRandom.Count; i++)
        {
            if (id == lstIdItemSortRandom[i])
            {
                lstIdItemSortRandom[i] = idTarget;
            }
            s += " " + lstIdItemSortRandom[i];
        }
        Debug.Log("lstIdItemSortRandom = " + lstIdItemSortRandom.Count + "\n" + s);
    }
    private void RemoveListSort(int id)
    {
        for (int i = 0; i < lstIdItemSortRandom.Count; i++)
        {
            if (id == lstIdItemSortRandom[i])
            {
                lstIdItemSortRandom.Remove(lstIdItemSortRandom[i]);
                countItemSort -= maxIdItemInLevel;
            }
        }
    }


    private void UpdateListID(List<int> lstIdItem)
    {
        lstIdItemRandomFake = new List<int>();
        for (int i = 0; i < lstIdItem.Count; i++)
        {
            int id = lstIdItem[i];
            lstIdItemRandomFake.Add(id);
        }
    }
    private void ResetList()
    {
        layerInfos = new List<LayerInfo>();
        itemInfos = new List<ItemInfo>();
        itemInfosFake = new List<ItemInfo>();
        dicTest = new Dictionary<int, int>();
        dicTest1 = new Dictionary<int, int>();
        dicLayer = new Dictionary<int, List<LayerInfo>>();
        dicIdItemInLayer = new Dictionary<int, int>();
        itemInfoFake = new ItemInfo();
        itemInfoFake.id = 0;
        LstIdItemNotEnough.Clear();
    }
    public void Run(int idLevel, bool isReplay, bool isSort = false)
    {
        //this.isSort = isSort;

        if (!isSort)
        {
            SetRandomItemData(idLevel);
            Debug.Log("lstIdItemSortRandom = " + lstIdItemRandom.Count);
            UpdateListID(lstIdItemRandom);

        }
        else
        {
            Debug.Log("lstIdItemSortRandom = " + lstIdItemSortRandom.Count);
            UpdateListID(lstIdItemSortRandom);
            Debug.Log("listFake = " + lstIdItemRandomFake.Count);

        }

        ResetList();
        string test = "";
        isSortWithCondition = CheckConditionIdSortItem();
        foreach (var dic in dicItemsort)
        {
            test += "  " + dic.Key.ToString() + " -  " + dic.Value.ToString() + " |";
            /*if (dic.Key != 0 && dic.Value != 3)
            {
                Debug.LogError(" Error : " + dic.Value + " Item id = " + dic.Key + " in Level");

            }*/
        }
        Debug.LogWarning(test);
        if (isSort)
        {
            InitItem(countItemSort);
        }
        else
        {
            InitItem(countItem);
        }
        string s = " ID = ";
        string layer = " Layer = ";
        for (int i = 0; i < itemInfos.Count; i++)
        {
            s += " " + itemInfos[i].id.ToString();
            if (!dicTest1.ContainsKey(itemInfos[i].id))
            {
                dicTest1.Add(itemInfos[i].id, 1);
            }
            else
            {
                dicTest1[itemInfos[i].id]++;
            }
        }
        string s1 = "|";
        foreach (var dic in dicTest1)
        {
            s1 += "  " + dic.Key.ToString() + " -  " + dic.Value.ToString() + " |";
            if (dic.Key != 0 && dic.Value != 3)
            {
                Debug.LogError(" Error : " + dic.Value + " Item id = " + dic.Key + " in Level");

            }
        }

        Debug.Log("Spawn-Item");
        Debug.Log(s1);
        Debug.Log(dicTest1.Count);
        Debug.Log(itemInfos.Count);
        Debug.Log(s);
        if (idLevel == 0)
        {
            InitLayerTutorial();

        }
        else
        {
            Initlayer(isSort);
        }

        Debug.Log(" Current_layer / MaxLayerLevel = " + layerInfos.Count + " / " + countLayer);

        if (layerInfos.Count > countLayer)
        {
            Debug.LogError(">>>> Vuot qua so luong Layer trong 1 level <<<<<");
        }
        string s3 = "";
        int indexLayer = 0;
        int count = 0;
        int countEmpty = 0;
        Dictionary<int, int> dicLayer = new Dictionary<int, int>();

        for (int i = 0; i < layerInfos.Count; i++)
        {
            // test
            count++;
            layer += "\nL" + i.ToString();
            for (int j = 0; j < layerInfos[i].itemSkin.Length; j++)
            {
                /*if(layerInfos[i].itemSkin[j].id == 0)
                {
                    countEmpty++;
                    if(countEmpty > emptyInLayer[indexLayer] )
                    {
                        layerInfos[i].itemSkin[j].id = lstIdOnlyTwoItem[0];
                        lstIdOnlyTwoItem.RemoveAt(0);
                        Debug.LogError(">>>> Update lst Item <<<<");
                    }
                }*/
                layer += " " + layerInfos[i].itemSkin[j].id.ToString();
                if (layerInfos[i].itemSkin[j].isSpecial)
                {
                    layer += " isSpecial | ";
                }
                if (!dicTest.ContainsKey(layerInfos[i].itemSkin[j].id))
                {
                    dicTest.Add(layerInfos[i].itemSkin[j].id, 1);
                }
                else
                {
                    dicTest[layerInfos[i].itemSkin[j].id]++;
                }
                if (!dicLayer.ContainsKey(layerInfos[i].itemSkin[j].id))
                {
                    dicLayer.Add(layerInfos[i].itemSkin[j].id, 1);
                }
                else
                {
                    dicLayer[layerInfos[i].itemSkin[j].id]++;
                }
            }
            if (count % countBox == 0 || count == layerInfos.Count)
            {
                indexLayer++;
                countEmpty = 0;
                s3 += "\nLayer " + indexLayer + " = |";
                foreach (var dic in dicLayer)
                {
                    s3 += "  " + dic.Key.ToString() + " -  " + dic.Value.ToString() + " |";

                }
                dicLayer.Clear();
            }


        }
        Debug.Log("Dic 2");
        string s2 = "|";
        foreach (var dic in dicTest)
        {
            s2 += "  " + dic.Key.ToString() + " -  " + dic.Value.ToString() + " |";
            if (dic.Key != 0 && dic.Value != 3)
            {
                Debug.LogError(" Error : " + dic.Value + " Item id = " + dic.Key + " in Level");
                /* if (dic.Value == 2) // tam thoi`
                 {
                     lstIdOnlyTwoItem.Add(dic.Key);
                 }*/
            }
        }
        Debug.Log(s2);
        Debug.Log(s3);
        Debug.Log(layer);
        ConvertListLayerInfosToDic();
    }

    private void AddItemBoxOne(ref ItemInfo itemInfo)
    {
        int id = -1;
        for (int j = 0; j < lstIdItemOneBosInLayer.Count; j++)
        {
            if (!dicIdItemInLayer.ContainsKey(lstIdItemOneBosInLayer[j])) continue;
            if (isSortWithCondition)
            {
                if (dicIdItemInLayer[lstIdItemOneBosInLayer[j]] < maxIdItemInLevel)
                {
                    dicIdItemInLayer[lstIdItemOneBosInLayer[j]]++;
                    id = lstIdItemOneBosInLayer[j];
                    break;
                }
                else
                {
                    dicIdItemInLayer.Remove(lstIdItemOneBosInLayer[j]);
                }
            }
            else
            {
                if (dicIdItemInLayer[lstIdItemOneBosInLayer[j]] < (dicItemsort[lstIdItemOneBosInLayer[j]] * maxIdItemInLevel))
                {
                    dicIdItemInLayer[lstIdItemOneBosInLayer[j]]++;
                    id = lstIdItemOneBosInLayer[j];
                    if (dicIdItemInLayer[lstIdItemOneBosInLayer[j]] >= (dicItemsort[lstIdItemOneBosInLayer[j]] * maxIdItemInLevel))
                    {
                        dicIdItemInLayer.Remove(lstIdItemOneBosInLayer[j]);
                    }
                    break;
                }
                else
                {
                    dicIdItemInLayer.Remove(lstIdItemOneBosInLayer[j]);
                }

            }

        }
        for (int j = 0; j < itemInfosFake.Count; j++)
        {
            if (autoSpawn)
            {
                if (lstIdItemOneBosInLayer.Contains(itemInfosFake[j].id))
                {
                    itemInfo = itemInfosFake[j];
                    itemInfosFake.Remove(itemInfosFake[j]);
                    lstIdInLayer.Add(itemInfo.id);
                    break;
                }
                else
                {
                    if (itemInfosFake.Count - 1 == j)
                    {
                        Debug.LogError(" >>>>>>>> Dont Add Item <<<<<<<<<");

                        string item1 = "count = " + lstIdItemOneBosInLayer.Count.ToString() + " IdItemInLayer = ";
                        for (int a = 0; a < lstIdItemOneBosInLayer.Count; a++)
                        {
                            item1 += lstIdItemOneBosInLayer[a].ToString() + " _ ";
                        }
                        Debug.LogError(item1);

                    }
                }
            }
            else
            {
                if (id == itemInfosFake[j].id)
                {
                    itemInfo = itemInfosFake[j];
                    itemInfosFake.Remove(itemInfosFake[j]);
                    lstIdInLayer.Add(itemInfo.id);
                    break;
                }
                else
                {
                    if (itemInfosFake.Count - 1 == j)
                    {

                        Debug.LogError(" >>>>>>>> Dont Add Item id = " + id + " <<<<<<<<<");
                        Debug.LogError("indexLayer = " + indexLayer);
                        Debug.LogError("lstIdItemInLayer.Contains(id) = " + lstIdItemOneBosInLayer.Contains(id));
                        string s = "";
                        foreach (var dic in dicIdItemInLayer)
                        {
                            s += " - " + dic.Key + "|" + dic.Value;
                        }
                        Debug.LogError("dic = " + s);
                        string item = "item_fake = ";
                        for (int a = 0; a < itemInfosFake.Count; a++)
                        {
                            item += itemInfosFake[a].id + " _ ";
                        }
                        Debug.LogError(item);
                        string item1 = "count = " + lstIdItemOneBosInLayer.Count.ToString() + " IdItemInLayer = ";
                        for (int a = 0; a < lstIdItemOneBosInLayer.Count; a++)
                        {
                            item1 += lstIdItemOneBosInLayer[a].ToString() + " _ ";
                        }
                        Debug.LogError("lst = " + item1);
                    }
                }
            }

        }
    }

    int indexLayer = -1;
    List<int> lstIdInLayer = new List<int>();
    private void Initlayer(bool isSort)
    {
        if (!isSort)
        {
            lstIdItemSpecial.Clear();
        }
        indexLayer = -1;
        int empty = 0;
        InitListItemInfoFake();
        Debug.Log("countLayer =" + countLayer);
        int indexItem = -1;
        List<int> lstIndexItemSpecial = new List<int>();
        lstIdInLayer.Clear();


        for (int i = 0; i < countLayer; i++)
        {
            if (i % countBox == 0)
            {
                indexLayer++;
                empty = 0;
                Dictionary<int, int> dicId = new Dictionary<int, int>();
                Dictionary<int, int> dicIdItem = new Dictionary<int, int>();
                for (int j = 0; j < lstIdInLayer.Count; j++)
                {
                    if (dicId.ContainsKey(lstIdInLayer[j]))
                    {
                        dicId[lstIdInLayer[j]]++;
                    }
                    else
                    {
                        dicId.Add(lstIdInLayer[j], 1);
                    }


                }
                foreach (var dic in dicId)
                {
                    if (dic.Value < maxItemInLayerBox)
                    {
                        dicIdItem.Add(dic.Key, dic.Value);
                    }
                }
                //if(indexLayer > 0)
                {
                    LstIdItemNotEnough.Add(dicIdItem);
                }

                //if(indexLayer == 0)
               /* {
                    foreach (var dic in dicIdItem)
                    {
                        Debug.LogError("id  = " + dic.Key + "| value = " + dic.Value);
                    }
                }*/
                lstIdInLayer.Clear();
                lstIdItemInLayer.Clear();
                lstIdItemOneBosInLayer.Clear();

                indexItem = -1;
                int countItemInLayer = countBox * maxItemInLayerBox - emptyInLayer[indexLayer];
                lstIndexItemSpecial.Clear();
                int count = 0;
                int maxRandom = countBox * maxItemInLayerBox; // test
                //Debug.LogError("-----------------indexLayer " + indexLayer);
                if (indexLayer < lstItemSpecial.Count && lstItemSpecial[indexLayer] >= 0)
                {
                    while (count <= maxRandom)
                    {

                        int indexItemSpecial = Random.Range(0, countItemInLayer);
                        //Debug.Log("-------countItemInLayer   " + countItemInLayer);
                        if (!lstIndexItemSpecial.Contains(indexItemSpecial))
                        {

                            if (lstIndexItemSpecial.Count >= lstItemSpecial[indexLayer])
                            {
                                count += maxRandom;
                            }
                            else
                            {
                                lstIndexItemSpecial.Add(indexItemSpecial);
                            }

                        }
                        count++;
                    }
                }
                //Debug.Log("------------- lstIndexItemSpecial " + lstIndexItemSpecial.Count);
                if (lstIndexItemSpecial.Count >= 2)
                {
                    //Debug.Log("------------- lstIndexItemSpecial " + lstIndexItemSpecial[0]);
                    //Debug.Log("------------- lstIndexItemSpecial " + lstIndexItemSpecial[1]);
                }

            }



            LayerInfo layerInfo = new LayerInfo();
            layerInfo.itemSkin = new ItemInfo[maxItemInLayerBox];


            int indexBoxOne = i - indexLayer * countBox;
            if (lstIdItemInLayer.Count >= maxTypeItemLayer[indexLayer])
            {
                if (BoxOneItem.Contains(indexBoxOne))
                {
                    if (lstIdItemOneBosInLayer.Count >= maxTypeItemBoxOneInLayer[indexLayer])
                    {
                        Debug.LogError(">>>>>>>>>>>>>>>>>> Stop <<<<<<<<<<<<<<<<<<<");
                    }
                    else
                    {
                        AddItem(ref layerInfo.itemSkin[0]);
                        indexItem++;
                        if (lstIndexItemSpecial.Contains(indexItem))
                        {
                            layerInfo.itemSkin[0].isSpecial = true;
                            if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[0].id))
                            {
                                lstIdItemSpecial.Add(layerInfo.itemSkin[0].id);
                            }
                        }

                    }
                }
                else
                {
                    AddItem(ref layerInfo.itemSkin[0]);
                    indexItem++;
                    if (lstIndexItemSpecial.Contains(indexItem))
                    {
                        layerInfo.itemSkin[0].isSpecial = true;
                        if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[0].id))
                        {
                            lstIdItemSpecial.Add(layerInfo.itemSkin[0].id);
                        }
                    }

                }

            }
            else
            {
                if (itemInfosFake.Count <= 0) break;
                if (BoxOneItem.Contains(indexBoxOne))
                {
                    if (maxTypeItemBoxOneInLayer[indexLayer] == 0)
                    {
                        layerInfo.itemSkin[0] = itemInfoFake;
                    }
                    else
                    {
                        if (lstIdItemOneBosInLayer.Count >= maxTypeItemBoxOneInLayer[indexLayer])
                        {
                            AddItemBoxOne(ref layerInfo.itemSkin[0]);
                            indexItem++;
                            if (lstIndexItemSpecial.Contains(indexItem))
                            {
                                layerInfo.itemSkin[0].isSpecial = true;
                                if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[0].id))
                                {
                                    lstIdItemSpecial.Add(layerInfo.itemSkin[0].id);
                                }
                            }
                        }
                        else
                        {
                            layerInfo.itemSkin[0] = itemInfosFake[0];
                            itemInfosFake.Remove(itemInfosFake[0]);

                            lstIdInLayer.Add(layerInfo.itemSkin[0].id);
                            if (!lstIdItemInLayer.Contains(layerInfo.itemSkin[0].id))
                            {
                                lstIdItemInLayer.Add(layerInfo.itemSkin[0].id);
                            }

                            if (!dicIdItemInLayer.ContainsKey(layerInfo.itemSkin[0].id))
                            {
                                dicIdItemInLayer.Add(layerInfo.itemSkin[0].id, 1);
                            }
                            else
                            {
                                dicIdItemInLayer[layerInfo.itemSkin[0].id]++;
                            }
                            if (!lstIdItemOneBosInLayer.Contains(layerInfo.itemSkin[0].id))
                            {
                                lstIdItemOneBosInLayer.Add(layerInfo.itemSkin[0].id);
                            }
                        }
                    }


                }
                else
                {
                    ItemInfo itemInfo = itemInfoFake;
                    if (dicLayerMatchLayerNotEnough.ContainsKey(indexLayer))
                    {
                        //Debug.LogError("test --indexLayer " + indexLayer);
                        //Debug.LogError("test --layer match " + dicLayerMatchLayerNotEnough[indexLayer].Count);
                        bool dontFindItem = true;
                        int count = 0;
                        for (int j = 0; j < dicLayerMatchLayerNotEnough[indexLayer].Count; j++) // Số layer match với layer current
                        {
                            //Debug.LogError("test --lst index layer =  " + dicLayerMatchLayerNotEnough[indexLayer][j]);
                            if (!dontFindItem) break;
                            count = j;
                            //Debug.LogError("test -- count " + LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]].Count);
                            //if (LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]].Count > 0) // 
                            {

                                foreach (var id in LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]])
                                {
                                   // Debug.LogError("test -- ID " + id.Key);
                                    //Debug.LogError("test -- value " + id.Value);
                                    if (id.Value <= maxItemInLayerBox)
                                    {
                                        //Debug.LogError("test -- match ID " + id.Key);
                                        itemInfo.id = id.Key;
                                        dontFindItem = false;
                                        break;
                                    }
                                }
                            }
                        }
                        if (dontFindItem)
                        {
                            layerInfo.itemSkin[0] = itemInfosFake[0];
                            itemInfosFake.Remove(itemInfosFake[0]);
                        }
                        else
                        {
                            bool isAddItem = false;
                            foreach (var item in itemInfosFake)
                            {
                                if (itemInfo.id == item.id)
                                {
                                    layerInfo.itemSkin[0] = itemInfo;
                                    itemInfosFake.Remove(item);
                                    isAddItem = true;
                                    LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][count]][itemInfo.id]++;
                                    break;
                                }

                            }
                            if (!isAddItem)
                            {

                                layerInfo.itemSkin[0] = itemInfosFake[0];
                                LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][count]].Remove(itemInfo.id);
                                itemInfosFake.Remove(itemInfosFake[0]);
                                //Debug.LogError(">>>>> Dont add  item from LstIdItemNotEnough <<<<<");
                            }
                        }
                    }
                    else
                    {
                        layerInfo.itemSkin[0] = itemInfosFake[0];
                        itemInfosFake.Remove(itemInfosFake[0]);
                    }
                    indexItem++;
                    if (lstIndexItemSpecial.Contains(indexItem))
                    {
                        layerInfo.itemSkin[0].isSpecial = true;
                        if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[0].id))
                        {
                            lstIdItemSpecial.Add(layerInfo.itemSkin[0].id);
                        }
                    }
                    lstIdInLayer.Add(layerInfo.itemSkin[0].id);
                    if (!lstIdItemInLayer.Contains(layerInfo.itemSkin[0].id))
                    {
                        lstIdItemInLayer.Add(layerInfo.itemSkin[0].id);
                    }
                    if (!dicIdItemInLayer.ContainsKey(layerInfo.itemSkin[0].id))
                    {
                        dicIdItemInLayer.Add(layerInfo.itemSkin[0].id, 1);
                    }
                    else
                    {
                        dicIdItemInLayer[layerInfo.itemSkin[0].id]++;
                    }
                }





            }
            if (BoxOneItem.Contains(indexBoxOne))
            {
                layerInfo.itemSkin[1] = itemInfoFake;
                layerInfo.itemSkin[2] = itemInfoFake;
                layerInfos.Add(layerInfo);
                continue;
            }
            int random = 2; // default

            if (empty == emptyInLayer[indexLayer] - oneEmpty)
            {
                random = 1;
            }
            else if (empty == emptyInLayer[indexLayer] - twoEmpty)
            {
                random = 0;
            }
            else if (empty < emptyInLayer[indexLayer])
            {
                random = Random.Range(0, maxItemInLayerBox); // maxItemInLayerBox
            }
            else
            {
                random = 2;
            }

            if (countBox - (i % countBox) <= (emptyInLayer[indexLayer] / twoEmpty) && empty < (emptyInLayer[indexLayer] - twoEmpty)) // auto add 2 item empty
            {
                random = 0;
                Debug.LogError("auto_add_2_item_Empty");
            }


            if (random == 0)
            {
                empty += twoEmpty;
                layerInfo.itemSkin[1] = itemInfoFake;
                layerInfo.itemSkin[2] = itemInfoFake;
            }
            else if (random == 1)
            {
                empty += oneEmpty;
                if (lstIdItemInLayer.Count >= maxTypeItemLayer[indexLayer])
                {

                    AddItem(ref layerInfo.itemSkin[1]);
                    indexItem++;
                    if (lstIndexItemSpecial.Contains(indexItem))
                    {
                        layerInfo.itemSkin[1].isSpecial = true;
                        if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[0].id))
                        {
                            lstIdItemSpecial.Add(layerInfo.itemSkin[0].id);
                        }
                    }
                }
                else
                {
                    if (itemInfosFake.Count <= 0)
                    {
                        layerInfos.Add(layerInfo);
                        break;
                    }
                    ItemInfo itemInfo = itemInfoFake;
                    if (dicLayerMatchLayerNotEnough.ContainsKey(indexLayer))
                    {
                        //Debug.LogError("test --indexLayer " + indexLayer);
                        //Debug.LogError("test --layer match " + dicLayerMatchLayerNotEnough[indexLayer].Count);
                        bool dontFindItem = true;
                        int count = 0;
                        for (int j = 0; j < dicLayerMatchLayerNotEnough[indexLayer].Count; j++) // Số layer match với layer current
                        {
                            //Debug.LogError("test --lst index layer =  " + dicLayerMatchLayerNotEnough[indexLayer][j]);
                            if (!dontFindItem) break;
                            count = j;
                            //Debug.LogError("test -- count " + LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]].Count);
                            //if (LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]].Count > 0) // 
                            {

                                foreach (var id in LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]])
                                {
                                    //Debug.LogError("test -- ID " + id.Key);
                                    //Debug.LogError("test -- value " + id.Value);
                                    if (id.Value <= maxItemInLayerBox)
                                    {
                                        //Debug.LogError("test -- match ID " + id.Key);
                                        itemInfo.id = id.Key;
                                        dontFindItem = false;
                                        break;
                                    }
                                }
                            }
                        }
                        if (dontFindItem)
                        {
                            bool check = false;
                            foreach (var item in itemInfosFake)
                            {
                                if (layerInfo.itemSkin[0].id == item.id)
                                {
                                    layerInfo.itemSkin[1] = item;
                                    itemInfosFake.Remove(item);
                                    check = true;
                                    break;
                                }

                            }
                            if (!check)
                            {
                                layerInfo.itemSkin[1] = itemInfosFake[0];
                                itemInfosFake.Remove(itemInfosFake[0]);
                            }
                        }
                        else
                        {
                            bool isAddItem = false;
                            foreach (var item in itemInfosFake)
                            {
                                if (itemInfo.id == item.id)
                                {
                                    layerInfo.itemSkin[1] = itemInfo;
                                    itemInfosFake.Remove(item);
                                    isAddItem = true;
                                    LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][count]][itemInfo.id]++;
                                    break;
                                }

                            }
                            if (!isAddItem)
                            {

                                layerInfo.itemSkin[1] = itemInfosFake[0];
                                LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][count]].Remove(itemInfo.id);
                                itemInfosFake.Remove(itemInfosFake[0]);
                                //Debug.LogError(">>>>> Dont add  item from LstIdItemNotEnough <<<<<");
                            }
                        }
                    }
                    else
                    {
                        bool check = false;
                        foreach (var item in itemInfosFake)
                        {
                            if (layerInfo.itemSkin[0].id == item.id)
                            {
                                layerInfo.itemSkin[1] = item;
                                itemInfosFake.Remove(item);
                                check = true;
                                break;
                            }

                        }
                        if (!check)
                        {
                            layerInfo.itemSkin[1] = itemInfosFake[0];
                            itemInfosFake.Remove(itemInfosFake[0]);
                        }
                    }
                    indexItem++;
                    if (lstIndexItemSpecial.Contains(indexItem))
                    {
                        layerInfo.itemSkin[1].isSpecial = true;
                        if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[1].id))
                        {
                            lstIdItemSpecial.Add(layerInfo.itemSkin[1].id);
                        }
                    }
                    lstIdInLayer.Add(layerInfo.itemSkin[1].id);
                    if (!lstIdItemInLayer.Contains(layerInfo.itemSkin[1].id))
                    {
                        lstIdItemInLayer.Add(layerInfo.itemSkin[1].id);
                    }
                    if (!dicIdItemInLayer.ContainsKey(layerInfo.itemSkin[1].id))
                    {
                        dicIdItemInLayer.Add(layerInfo.itemSkin[1].id, 1);
                    }
                    else
                    {
                        dicIdItemInLayer[layerInfo.itemSkin[1].id]++;
                    }
                }
                layerInfo.itemSkin[2] = itemInfoFake;
            }
            else
            {
                if (lstIdItemInLayer.Count >= maxTypeItemLayer[indexLayer])
                {

                    AddItem(ref layerInfo.itemSkin[1]);
                    indexItem++;
                    if (lstIndexItemSpecial.Contains(indexItem))
                    {
                        layerInfo.itemSkin[1].isSpecial = true;
                        if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[1].id))
                        {
                            lstIdItemSpecial.Add(layerInfo.itemSkin[1].id);
                        }
                    }
                }
                else
                {
                    if (itemInfosFake.Count <= 0)
                    {
                        layerInfos.Add(layerInfo);
                        break;
                    }

                    ItemInfo itemInfo = itemInfoFake;
                    if (dicLayerMatchLayerNotEnough.ContainsKey(indexLayer))
                    {
                        //Debug.LogError("test --indexLayer " + indexLayer);
                        //Debug.LogError("test --layer match " + dicLayerMatchLayerNotEnough[indexLayer].Count);
                        bool dontFindItem = true;
                        int count = 0;
                        for (int j = 0; j < dicLayerMatchLayerNotEnough[indexLayer].Count; j++) // Số layer match với layer current
                        {
                            //Debug.LogError("test --lst index layer =  " + dicLayerMatchLayerNotEnough[indexLayer][j]);
                            if (!dontFindItem) break;
                            count = j;
                            //Debug.LogError("test -- count " + LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]].Count);
                            //if (LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]].Count > 0) // 
                            {

                                foreach (var id in LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]])
                                {
                                    //Debug.LogError("test -- ID " + id.Key);
                                    //Debug.LogError("test -- value " + id.Value);
                                    if (id.Value <= maxItemInLayerBox)
                                    {
                                        //Debug.LogError("test -- match ID " + id.Key);
                                        itemInfo.id = id.Key;
                                        dontFindItem = false;
                                        break;
                                    }
                                }
                            }
                        }
                        if (dontFindItem)
                        {
                            bool check = false;
                            foreach (var item in itemInfosFake)
                            {
                                if (layerInfo.itemSkin[0].id == item.id)
                                {
                                    layerInfo.itemSkin[1] = item;
                                    itemInfosFake.Remove(item);
                                    check = true;
                                    break;
                                }

                            }
                            if (!check)
                            {
                                layerInfo.itemSkin[1] = itemInfosFake[0];
                                itemInfosFake.Remove(itemInfosFake[0]);
                            }
                        }
                        else
                        {
                            bool isAddItem = false;
                            foreach (var item in itemInfosFake)
                            {
                                if (itemInfo.id == item.id)
                                {
                                    layerInfo.itemSkin[1] = itemInfo;
                                    itemInfosFake.Remove(item);
                                    isAddItem = true;
                                    LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][count]][itemInfo.id]++;
                                    break;
                                }

                            }
                            if (!isAddItem)
                            {

                                layerInfo.itemSkin[1] = itemInfosFake[0];
                                LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][count]].Remove(itemInfo.id);
                                itemInfosFake.Remove(itemInfosFake[0]);
                                //Debug.LogError(">>>>> Dont add  item from LstIdItemNotEnough <<<<<");
                            }
                        }
                    }
                    else
                    {
                        bool check = false;
                        foreach (var item in itemInfosFake)
                        {
                            if (layerInfo.itemSkin[0].id == item.id)
                            {
                                layerInfo.itemSkin[1] = item;
                                itemInfosFake.Remove(item);
                                check = true;
                                break;
                            }

                        }
                        if (!check)
                        {
                            layerInfo.itemSkin[1] = itemInfosFake[0];
                            itemInfosFake.Remove(itemInfosFake[0]);
                        }
                    }
                    indexItem++;
                    if (lstIndexItemSpecial.Contains(indexItem))
                    {
                        layerInfo.itemSkin[1].isSpecial = true;
                        if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[1].id))
                        {
                            lstIdItemSpecial.Add(layerInfo.itemSkin[1].id);
                        }
                    }
                    lstIdInLayer.Add(layerInfo.itemSkin[1].id);
                    if (!lstIdItemInLayer.Contains(layerInfo.itemSkin[1].id))
                    {
                        lstIdItemInLayer.Add(layerInfo.itemSkin[1].id);
                    }
                    if (!dicIdItemInLayer.ContainsKey(layerInfo.itemSkin[1].id))
                    {
                        dicIdItemInLayer.Add(layerInfo.itemSkin[1].id, 1);
                    }
                    else
                    {
                        dicIdItemInLayer[layerInfo.itemSkin[1].id]++;
                    }
                }

                if (layerInfo.itemSkin[1].id == layerInfo.itemSkin[0].id && isSortWithCondition && layerInfo.itemSkin[0].id != -1) // 0 defaut
                {
                    if (lstIdItemInLayer.Count >= maxTypeItemLayer[indexLayer])
                    {

                        AddItem(ref layerInfo.itemSkin[2]);
                        indexItem++;
                        if (lstIndexItemSpecial.Contains(indexItem))
                        {
                            layerInfo.itemSkin[2].isSpecial = true;
                            if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[2].id))
                            {
                                lstIdItemSpecial.Add(layerInfo.itemSkin[2].id);
                            }
                        }
                    }
                    else
                    {
                        string s = "";
                        for (int j = 0; j < itemInfosFake.Count; j++)
                        {
                            s += " - " + itemInfosFake[j].id.ToString();
                            if (layerInfo.itemSkin[1].id != itemInfosFake[j].id)
                            {

                                layerInfo.itemSkin[2] = itemInfosFake[j];
                                indexItem++;
                                lstIdInLayer.Add(layerInfo.itemSkin[2].id);
                                if (lstIndexItemSpecial.Contains(indexItem))
                                {
                                    layerInfo.itemSkin[2].isSpecial = true;
                                    if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[2].id))
                                    {
                                        lstIdItemSpecial.Add(layerInfo.itemSkin[2].id);
                                    }
                                }
                                itemInfosFake.Remove(itemInfosFake[j]);
                                if (!lstIdItemInLayer.Contains(layerInfo.itemSkin[2].id))
                                {
                                    lstIdItemInLayer.Add(layerInfo.itemSkin[2].id);
                                }
                                if (!dicIdItemInLayer.ContainsKey(layerInfo.itemSkin[2].id))
                                {
                                    dicIdItemInLayer.Add(layerInfo.itemSkin[2].id, 1);
                                }
                                else
                                {
                                    dicIdItemInLayer[layerInfo.itemSkin[2].id]++;
                                }
                                break;
                            }
                            else
                            {
                                if (itemInfosFake.Count - 1 == j)
                                {
                                    /*Debug.LogError("Dont Add Item");
                                    Debug.LogError("Dont Add Item id "+ layerInfo.itemSkin[1].id);
                                    Debug.LogError("lst = " + s);
                                    Debug.LogError("itemInfosFake.Count = " + itemInfosFake.Count);
                                    Debug.LogError("layerInfos[layerInfos.Count - 1].itemSkin = " + layerInfos[layerInfos.Count - 1].itemSkin.Length);*/
                                    if (layerInfos.Count > 0)
                                    {
                                        for (int k = 0; k < layerInfos[layerInfos.Count - 1].itemSkin.Length; k++) // thay doi vi tri item cuoi' cung` :((((
                                        {
                                            if (layerInfos[layerInfos.Count - 1].itemSkin[k].id != 0)
                                            {
                                                Debug.LogError(">>>>>>>> Sort Item <<<<<<");/*
                                            Debug.LogError(">>>>>>>> count itemInfosFake = "+ itemInfosFake.Count);
                                            Debug.LogError(">>>>>>>> itemInfosFake.Contains(itemInfosFake[j]) = " + itemInfosFake.Contains(itemInfosFake[j]));*/

                                                layerInfo.itemSkin[2].id = layerInfos[layerInfos.Count - 1].itemSkin[k].id;
                                                indexItem++;
                                                if (lstIndexItemSpecial.Contains(indexItem))
                                                {
                                                    layerInfo.itemSkin[2].isSpecial = true;
                                                    if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[2].id))
                                                    {
                                                        lstIdItemSpecial.Add(layerInfo.itemSkin[2].id);
                                                    }
                                                }
                                                layerInfos[layerInfos.Count - 1].itemSkin[k].id = layerInfo.itemSkin[1].id; // tam thoi co the thay the itemInfosFake[0] vs layerInfo.itemSkin[1].id
                                                itemInfosFake.Remove(itemInfosFake[j]);
                                                break;
                                            }
                                            else
                                            {
                                                if (layerInfos[layerInfos.Count - 1].itemSkin.Length - 1 == k)
                                                    Debug.LogError(">>>>>>>> Dont add Item <<<<<<");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Debug.LogError(">>>>>>>> Dont Sort Item <<<<<<");
                                    }


                                }
                                continue;
                            }
                        }

                    }
                }
                else
                {
                    if (lstIdItemInLayer.Count >= maxTypeItemLayer[indexLayer])
                    {

                        AddItem(ref layerInfo.itemSkin[2]);
                        indexItem++;
                        if (lstIndexItemSpecial.Contains(indexItem))
                        {
                            layerInfo.itemSkin[2].isSpecial = true;
                            if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[2].id))
                            {
                                lstIdItemSpecial.Add(layerInfo.itemSkin[2].id);
                            }
                        }
                    }
                    else
                    {
                        if (itemInfosFake.Count <= 0)
                        {
                            layerInfos.Add(layerInfo);
                            break;
                        }

                        ItemInfo itemInfo = itemInfoFake;
                        if (dicLayerMatchLayerNotEnough.ContainsKey(indexLayer))
                        {
                            //Debug.LogError("test --indexLayer " + indexLayer);
                            //Debug.LogError("test --layer match " + dicLayerMatchLayerNotEnough[indexLayer].Count);
                            bool dontFindItem = true;
                            int count = 0;
                            for (int j = 0; j < dicLayerMatchLayerNotEnough[indexLayer].Count; j++) // Số layer match với layer current
                            {
                                //Debug.LogError("test --lst index layer =  " + dicLayerMatchLayerNotEnough[indexLayer][j]);
                                if (!dontFindItem) break;
                                count = j;
                                //Debug.LogError("test -- count " + LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]].Count);
                                //if (LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]].Count > 0) // 
                                {

                                    foreach (var id in LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][j]])
                                    {
                                        //Debug.LogError("test -- ID " + id.Key);
                                        //Debug.LogError("test -- value " + id.Value);
                                        if (id.Value <= maxItemInLayerBox)
                                        {
                                            //Debug.LogError("test -- match ID " + id.Key);
                                            itemInfo.id = id.Key;
                                            dontFindItem = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (dontFindItem)
                            {
                                bool check = false;
                                foreach (var item in itemInfosFake)
                                {
                                    if (layerInfo.itemSkin[1].id == item.id)
                                    {
                                        layerInfo.itemSkin[2] = item;
                                        itemInfosFake.Remove(item);
                                        check = true;
                                        break;
                                    }

                                }
                                if (!check)
                                {
                                    layerInfo.itemSkin[2] = itemInfosFake[0];
                                    itemInfosFake.Remove(itemInfosFake[0]);
                                }
                            }
                            else
                            {
                                bool isAddItem = false;
                                foreach (var item in itemInfosFake)
                                {
                                    if (itemInfo.id == item.id)
                                    {
                                        layerInfo.itemSkin[2] = itemInfo;
                                        itemInfosFake.Remove(item);
                                        isAddItem = true;
                                        LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][count]][itemInfo.id]++;
                                        break;
                                    }

                                }
                                if (!isAddItem)
                                {

                                    layerInfo.itemSkin[2] = itemInfosFake[0];
                                    LstIdItemNotEnough[dicLayerMatchLayerNotEnough[indexLayer][count]].Remove(itemInfo.id);
                                    itemInfosFake.Remove(itemInfosFake[0]);
                                    //Debug.LogError(">>>>> Dont add  item from LstIdItemNotEnough <<<<<");
                                }
                            }
                        }
                        else
                        {
                            bool check = false;
                            foreach (var item in itemInfosFake)
                            {
                                if (layerInfo.itemSkin[1].id == item.id)
                                {
                                    layerInfo.itemSkin[2] = item;
                                    itemInfosFake.Remove(item);
                                    check = true;
                                    break;
                                }

                            }
                            if (!check)
                            {
                                layerInfo.itemSkin[2] = itemInfosFake[0];
                                itemInfosFake.Remove(itemInfosFake[0]);
                            }
                        }
                        indexItem++;
                        lstIdInLayer.Add(layerInfo.itemSkin[2].id);
                        if (lstIndexItemSpecial.Contains(indexItem))
                        {
                            layerInfo.itemSkin[2].isSpecial = true;
                            if (!lstIdItemSpecial.Contains(layerInfo.itemSkin[2].id))
                            {
                                lstIdItemSpecial.Add(layerInfo.itemSkin[2].id);
                            }
                        }
                        if (!lstIdItemInLayer.Contains(layerInfo.itemSkin[2].id))
                        {
                            lstIdItemInLayer.Add(layerInfo.itemSkin[2].id);
                        }
                        if (!dicIdItemInLayer.ContainsKey(layerInfo.itemSkin[2].id))
                        {
                            dicIdItemInLayer.Add(layerInfo.itemSkin[2].id, 1);
                        }
                        else
                        {
                            dicIdItemInLayer[layerInfo.itemSkin[2].id]++;
                        }
                    }
                }

            }
            layerInfos.Add(layerInfo);

        }
        Debug.Log(" So item chua dc sap xep = " + itemInfosFake.Count);
        if (itemInfosFake.Count > 0)
        {
            Debug.LogError("Spawn chua het item");
        }
        /*if(layerInfos.Count < countLayer)
        {
            for(int i = 0; i < countLayer - layerInfos.Count; i++)
            {
                LayerInfo layerInfo = new LayerInfo();
                layerInfo.itemSkin = new ItemInfo[maxItemInLayerBox];
                for (int j = 0; j < layerInfo.itemSkin.Length; j++)
                {
                    layerInfo.itemSkin[j].id = 0;
                }
                int index = Random.Range(0, layerInfo.itemSkin.Length);
                layerInfos.Insert(index, layerInfo);
            }
        }*/
    }
    private void InitLayerTutorial()
    {
        List<int> lst = new List<int>() { 0, 1, 2, 0, 2, 2, 1, 1, 0 };
        for (int i = 0; i < countLayer; i++)
        {
            LayerInfo layerInfo = new LayerInfo();
            layerInfo.itemSkin = new ItemInfo[maxItemInLayerBox];
            for (int j = 0; j < maxItemInLayerBox; j++)
            {
                ItemInfo itemInfo = new ItemInfo();
                itemInfo.id = lst[0];
                layerInfo.itemSkin[j] = itemInfo;
                lst.Remove(lst[0]);
            }
            layerInfos.Add(layerInfo);
        }


    }

    private void AddItem(ref ItemInfo itemInfo)
    {
        int id = -1;
        for (int j = 0; j < lstIdItemInLayer.Count; j++) // so id item tren layer;
        {
            if (!dicIdItemInLayer.ContainsKey(lstIdItemInLayer[j])) continue; // dicIdItemInLayer : so id va so luong id tren layer
            if (isSortWithCondition)
            {
                if (dicIdItemInLayer[lstIdItemInLayer[j]] < maxIdItemInLevel)
                {
                    dicIdItemInLayer[lstIdItemInLayer[j]]++;
                    id = lstIdItemInLayer[j];
                    break;
                }
                else
                {
                    dicIdItemInLayer.Remove(lstIdItemInLayer[j]);
                }
            }
            else
            {
                if (dicIdItemInLayer[lstIdItemInLayer[j]] < (dicItemsort[lstIdItemInLayer[j]] * maxIdItemInLevel))
                {
                    dicIdItemInLayer[lstIdItemInLayer[j]]++;
                    id = lstIdItemInLayer[j];
                    if (dicIdItemInLayer[lstIdItemInLayer[j]] >= (dicItemsort[lstIdItemInLayer[j]] * maxIdItemInLevel))
                    {
                        dicIdItemInLayer.Remove(lstIdItemInLayer[j]);
                    }
                    break;
                }
                else
                {
                    dicIdItemInLayer.Remove(lstIdItemInLayer[j]);
                }

            }

        }
        for (int j = 0; j < itemInfosFake.Count; j++)
        {
            if (autoSpawn)
            {
                if (lstIdItemInLayer.Contains(itemInfosFake[j].id))
                {
                    itemInfo = itemInfosFake[j];
                    itemInfosFake.Remove(itemInfosFake[j]);
                    lstIdInLayer.Add(itemInfo.id);
                    break;
                }
                else
                {
                    if (itemInfosFake.Count - 1 == j)
                    {
                        Debug.LogError(" >>>>>>>> Dont Add Item <<<<<<<<<");

                        string item1 = "count = " + lstIdItemInLayer.Count.ToString() + " IdItemInLayer = ";
                        for (int a = 0; a < lstIdItemInLayer.Count; a++)
                        {
                            item1 += lstIdItemInLayer[a].ToString() + " _ ";
                        }
                        Debug.LogError(item1);

                    }
                }
            }
            else
            {
                if (id == itemInfosFake[j].id)
                {
                    itemInfo = itemInfosFake[j];
                    itemInfosFake.Remove(itemInfosFake[j]);
                    lstIdInLayer.Add(itemInfo.id);
                    break;
                }
                else
                {
                    if (itemInfosFake.Count - 1 == j)
                    {

                        Debug.LogError(" >>>>>>>> Dont Add Item id = " + id + " <<<<<<<<<");
                        Debug.LogError("indexLayer = " + indexLayer);
                        Debug.LogError("lstIdItemInLayer.Contains(id) = " + lstIdItemInLayer.Contains(id));
                        string s = "";
                        foreach (var dic in dicIdItemInLayer)
                        {
                            s += " - " + dic.Key + "|" + dic.Value;
                        }
                        Debug.LogError("dic = " + s);
                        string item = "item_fake = ";
                        for (int a = 0; a < itemInfosFake.Count; a++)
                        {
                            item += itemInfosFake[a].id + " _ ";
                        }
                        Debug.LogError(item);
                        string item1 = "count = " + lstIdItemInLayer.Count.ToString() + " IdItemInLayer = ";
                        for (int a = 0; a < lstIdItemInLayer.Count; a++)
                        {
                            item1 += lstIdItemInLayer[a].ToString() + " _ ";
                        }
                        Debug.LogError("lst = " + item1);
                    }
                }
            }

        }
    }
    private void InitItem(int countItem)
    {
        Debug.Log("countItem = " + countItem);
        Debug.Log("lstIdItemRandomFake.Count = " + lstIdItemRandomFake.Count);
        for (int i = 0; i < countItem; i++)
        {
            ItemInfo itemInfo = new ItemInfo();
            int rd = Random.Range(0, lstIdItemRandomFake.Count);
            itemInfo.id = lstIdItemRandomFake[rd];

            CheckMaxAndAddItem(itemInfo);
        }
        if (lstIdItemRandomFake.Count > 0)
        {
            Debug.LogError(" >>>>>>>>>>>>>>>>" + lstIdItemRandomFake.Count + " id chua dc add vao layerinfo");
            for (int i = 0; i < lstIdItemRandomFake.Count; i++)
            {
                Debug.LogError(">>>>>>>>>>  id = " + lstIdItemRandomFake[i]);
            }
        }

    }
    private void CheckMaxAndAddItem(ItemInfo itemInfo)
    {
        int countCheck = 0;
        for (int i = 0; i < itemInfos.Count; i++)
        {
            if (itemInfos[i].id == itemInfo.id)
            {
                countCheck++;
            }

        }
        if (countCheck >= dicItemsort[itemInfo.id] * maxIdItemInLevel)
        {
            Debug.Log("STOPPPP " + countCheck + " id " + itemInfo.id);
        }


        if (isSortWithCondition)
        {
            if (countCheck >= maxIdItemInLevel - 1)
            {
                lstIdItemRandomFake.Remove(itemInfo.id);
            }
        }
        else
        {

            int condition = dicItemsort[itemInfo.id] * maxIdItemInLevel - 1;
            if (countCheck >= condition)
            {
                int count = lstIdItemRandomFake.Count;
                for (int j = 0; j < count; j++)
                {
                    if (lstIdItemRandomFake.Contains(itemInfo.id))
                    {
                        lstIdItemRandomFake.Remove(itemInfo.id);
                    }

                }
                if (lstIdItemRandomFake.Contains(itemInfo.id))
                {
                    Debug.LogError(" >>>>>>>>>>> chua xoa het item trong list fake <<<<<<<<<<<<<<<<");
                }

            }
        }

        if (itemInfos.Count > 1 && itemInfos[itemInfos.Count - 1].id == itemInfo.id)
        {
            itemInfos.Insert(0, itemInfo);
        }
        else
        {
            itemInfos.Add(itemInfo);
        }


    }
    private void InitListItemInfoFake()
    {
        Debug.Log("Count Item in level =" + itemInfos.Count);
        foreach (var item in itemInfos)
        {
            ItemInfo itemInfo = item;
            itemInfosFake.Add(itemInfo);
        }
    }
    public List<LayerInfo> GetLayerWithIndexBox(int index)
    {
        if (dicLayer.ContainsKey(index))
            return dicLayer[index];
        else
            return null;
    }
    private void ConvertListLayerInfosToDic()
    {
        int key = -1;
        string s = "";
        for (int i = 0; i < layerInfos.Count; i++)
        {
            key++;
            if (dicLayer.ContainsKey(key))
            {
                dicLayer[key].Add(layerInfos[i]);
            }
            else
            {
                List<LayerInfo> lstlayerInfos = new List<LayerInfo>();
                lstlayerInfos.Add(layerInfos[i]);
                dicLayer.Add(key, lstlayerInfos);
            }

            if (key == countBox - 1)
            {
                key = -1;
            }
        }
        foreach (var layer in dicLayer)
        {
            s += " \nBox " + layer.Key + " = ";
            for (int i = 0; i < layer.Value.Count; i++)
            {
                s += " | ";
                for (int j = 0; j < layer.Value[i].itemSkin.Length; j++)
                {
                    s += " " + layer.Value[i].itemSkin[j].id;
                }

            }
            s += " | ";
        }
        Debug.Log(s);
    }
    private Dictionary<int, int> dicItemsort = new Dictionary<int, int>();
    private bool CheckConditionIdSortItem()
    {
        dicItemsort.Clear();
        bool condition = true;
        for (int i = 0; i < lstIdItemSortRandom.Count; i++)
        {
            if (!dicItemsort.ContainsKey(lstIdItemSortRandom[i]))
            {
                dicItemsort.Add(lstIdItemSortRandom[i], 1);
            }
            else
            {
                dicItemsort[lstIdItemSortRandom[i]]++;
                if (dicItemsort[lstIdItemSortRandom[i]] * maxIdItemInLevel > maxIdItemInLevel)
                {
                    condition = false;
                }
            }
        }
        Debug.LogWarning(condition);
        return condition;
    }
}
