using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class BuyHeartPopup : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI noti;
    [SerializeField] private Button buyHearButton;
    [SerializeField] private int price;
    [SerializeField] private Button closeButton;
    private UnityAction callBack = null;
    public UnityAction CallBack
    {
        get => callBack;
        set => callBack = value;
    }

    private void OnEnable()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        buyHearButton.onClick.AddListener(OnClickBuyHeartButton);
        closeButton.onClick.AddListener(() =>
        {
            callBack?.Invoke();
            callBack = null;
            this.gameObject.SetActive(false);
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void OnClickBuyHeartButton()
    {
        Debug.Log("buy heart");
        //nho sua lai
        if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Gold) - price >= 0)
        {
            Debug.Log("curent heart: " + UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart));
            if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) < 5)
            {
                int currentHeart = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart);
                UserDatas.Instance.SetCurrency(CommonEnum.Currency.Heart, 5 - currentHeart);
                UserDatas.Instance.SetCurrency(CommonEnum.Currency.Gold, -price);
                UserDatas.Instance.SetLastLockHeartDate(GameController.Instance.GetCurrentTimeSecond());
                Observer.Instance.Notify(ObserverKey.CountDownHeart, true);

                List<UserDatas.Currency> currencies = new();
                UserDatas.Currency heart = new(CommonEnum.Currency.Heart, 5);
                currencies.Add(heart);
                UIController.Instance.ShowPopupReward(null, currencies, () =>
                {
                    this.gameObject.SetActive(false);
                });
            }
        }
        else
        {
            UIController.Instance.NotEnoughCoin();
        }
    }
}
