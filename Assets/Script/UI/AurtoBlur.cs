using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AurtoBlur : MonoBehaviour
{
    [SerializeField] byte minValue, Maxvalue;
    [SerializeField]
    byte valueA = 0;
    [SerializeField]
    byte distance = 0;
    bool is_plus;
    bool isStart;
    float time = 0f;
    private void OnEnable()
    {
        valueA = 255;
        this.GetComponent<Image>().color = new Color32(0, 0, 0, 255);
        isStart = true;
    }
    private void OnDisable()
    {
        isStart = false;
        
    }
    void Update()
    {
        /* if(valueA >= Maxvalue)
         {
             is_plus = false;
             time = 0f;
         }*/
        //is_plus = false;
        //time = 0f;
        valueA -= distance;
        if (valueA < minValue)
        {
            valueA = minValue;
        }
        //time += Time.deltaTime;
        
        SetColor(valueA);
    }
    private void SetColor(byte value)
    {
        this.GetComponent<Image>().color = new Color32(0, 0, 0, value);
        if(value <= minValue)
        {
            this.gameObject.SetActive(false);
        }
    }
}
