using CommonEnum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StuckPopup : MonoBehaviour
{
    [SerializeField] private RefreshButton refreshButton;
    [SerializeField] private Button closeButton;

    // Start is called before the first frame update
    void Start()
    {
        refreshButton.buttonAction = OnClickRefreshInStuck;
        closeButton.onClick.AddListener(() =>
        {
            this.gameObject.SetActive(false);
            LevelManager.Instance.IsLoseLevel = false;
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        GameController.Instance.IsSelected = true;
    } 
    private void OnDisable()
    {
        GameController.Instance.IsSelected = false;
        GameController.Instance.IsStartLevel = true;
    }

    public void OnClickRefreshInStuck()
    {
        int numberRefresh = UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.Refresh);
        if (numberRefresh <= 0)
        {
            UIController.Instance.GamePlayUI.BuyBoosterInGame.SetUpPopup(ItemGameplay.Refresh);
            UIController.Instance.GamePlayUI.BuyBoosterInGame.gameObject.SetActive(true);
        }
        else
        {
            GameController.Instance.IsStartLevel = true;
            LevelManager.Instance.SortItem();
            SoundController.Instance.PlayActiveBooster();
            UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Refresh, -1);
            refreshButton.UpdateUIRefresh();
            this.gameObject.SetActive(false);
            
        }
        LevelManager.Instance.IsLoseLevel = false;
    }

    public void UpdateCountBooster()
    {
        refreshButton.UpdateUIRefresh();
    }
}
