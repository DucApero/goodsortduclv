using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TQT
{
    public class Tutorial_Booster : MonoBehaviour
    {
        public ItemBooster_Tutorial skill1, skill2, skill3, skill4;
        public ItemBooster_Tutorial target;
        public GameObject bg_Tutorial;
        public Button closeTutorial;
        public TMP_Text titleBooster;
        public Transform arrow;
        public int indexTarget;
        Tween tween;
        public void Start()
        {
            skill1.Init();
            skill2.Init();
            skill3.Init();
            skill4.Init();
            closeTutorial.onClick.AddListener(HideTutorial);
            bg_Tutorial.SetActive(false);
            closeTutorial.gameObject.SetActive(false);

        }

        public void Show()
        {
 

            if (UserDatas.Instance.CurrentLevel + 1 == skill1.levelUnlock)
                target = skill1;
            if (UserDatas.Instance.CurrentLevel + 1 == skill2.levelUnlock)
                target = skill2;
            if (UserDatas.Instance.CurrentLevel + 1 == skill3.levelUnlock)
                target = skill3;
            if (UserDatas.Instance.CurrentLevel + 1 == skill4.levelUnlock)
                target = skill4;

            if (target != null && target.IsTutorial)
            {
                if (!target.IsTutorial)
                {
                    target = null;
                    return;
                }
                if (!GameController.Instance.IsSelected)
                    GameController.Instance.IsSelected = true;
                UIController.Instance.GamePlayUI.UnLockItem.GameStart(target.imgItem, target.targetItemMove);
                UIController.Instance.GamePlayUI.UnLockItem.callback = delegate { GameStart(); };
            }
        }
        public void GameStart()
        {
            bg_Tutorial.SetActive(true);
            closeTutorial.gameObject.SetActive(true);
            target.StartTutorial(indexTarget);
            titleBooster.text = target.Name;
            AnimArrow();
        }

        public void HideTutorial()
        {
            if (target != null && target.IsTutorial)
            {
                target.EndTutorial();
                bg_Tutorial.SetActive(false);
                arrow.SetParent(bg_Tutorial.transform);
                closeTutorial.gameObject.SetActive(false);
                tween.Kill();
                target = null;
                if (GameController.Instance.IsSelected)
                    GameController.Instance.IsSelected = false;
            } 
                
        }
        public void AnimArrow()
        {
            if (target == null)
                return;
            arrow.SetParent(target.transform);
            arrow.SetSiblingIndex(target.transform.childCount);
            arrow.localPosition = new Vector3(target.PosCurr.x, target.PosCurr.y, target.PosCurr.z);
            tween = arrow.DOLocalMove(target.PosNew, 0.5f)
                  .SetLoops(-1, LoopType.Yoyo);
        }
       
    }
}

