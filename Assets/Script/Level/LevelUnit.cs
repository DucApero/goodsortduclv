﻿using DG.Tweening;
using DVAH;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelUnit : MonoBehaviour
{
    [SerializeField] private int id = 0;
    public int Id => id;

    [SerializeField] float _width, _height, _center;
    public float Width => _width;
    public float Height => _height;
    public float CenterY => _center;
    [SerializeField] private SkinContainer skinContainer;
    [SerializeField] private List<BoxElement> listBoxElement = new List<BoxElement>();
    public List<BoxElement> ListBoxElement => listBoxElement;
    private List<LayerInfo> layers = new List<LayerInfo>();
    public List<LayerInfo> LayerInfo
    {
        set => layers = value;
    }
    private List<int> boxLockedID = new List<int>();
    private Dictionary<int, int> lstItemLayerFirst = new Dictionary<int, int>();
    public Dictionary<int, int> ListItemLayerFirst
    {
        get => lstItemLayerFirst;
    }
    public float timeDuration;
    public float TimeDuration
    {
        get => timeDuration;
        set => timeDuration = value;
    }
    #region firebase
    private int tapCubeNumber;
    public int TapCubeNumber
    {
        get => tapCubeNumber;
        set => tapCubeNumber = value;
    }
    private int useHint;
    public int UseHint
    {
        get => useHint;
        set => useHint = value;
    }   
    
    private int useMagicWand;
    public int UseMagicWand
    {
        get => useMagicWand;
        set => useMagicWand = value;
    }  
    private int useFreezer;
    public int UseFreezer
    {
        get => useFreezer;
        set => useFreezer = value;
    }  
    private int useRefresh;
    public int UseRefresh
    {
        get => useRefresh;
        set => useRefresh = value;
    }

    private int timeLoseLevel;
    public int TimeLoseLevel {
        get => timeLoseLevel;
        set => timeLoseLevel = value;
    }

    private int timeStartLevel;
    private int timeWinLevel;
    public int TimeWinLevel {
        get => timeWinLevel;
        set => timeWinLevel = value;
    }

    #endregion

    private void Start()
    {
        UpdateBoxMaterial();
        timeLoseLevel = (int)timeDuration;
        timeStartLevel = GameController.Instance.GetCurrentTimeSecond();
    }

    private void OnEnable()
    {
        SoundController.Instance.PlayBackgroundGameplay();
    }

    public void Awake()
    {
        //Observer.Instance.AddObserver(ObserverKey.CheckLoseLevel, CheckLoseLevel);
       //Observer.Instance.AddObserver(ObserverKey.CheckWinLevel, CheckWinLevel);
        Observer.Instance.AddObserver(ObserverKey.AddToListItemFisrt, AddItemToListLayerFirst);
        Observer.Instance.AddObserver(ObserverKey.DecreaseItemInListLayerFirst, DecreaseItemInListLayerFirst);
        Observer.Instance.AddObserver(ObserverKey.DecreaseNumberLock, DecreaseNumberLock);
        calculateSize();
    }

 
    private void OnDestroy()
    {
        //Observer.Instance.RemoveObserver(ObserverKey.CheckLoseLevel, CheckLoseLevel);
        //Observer.Instance.RemoveObserver(ObserverKey.CheckWinLevel, CheckWinLevel);
        Observer.Instance.RemoveObserver(ObserverKey.AddToListItemFisrt, AddItemToListLayerFirst);
        Observer.Instance.RemoveObserver(ObserverKey.DecreaseItemInListLayerFirst, DecreaseItemInListLayerFirst);
        Observer.Instance.RemoveObserver(ObserverKey.DecreaseNumberLock, DecreaseNumberLock);
    }
    void calculateSize()
    {
        float minX = listBoxElement[0].transform.position.x,
            maxX = listBoxElement[0].transform.position.x,
            minY = listBoxElement[0].transform.position.y,
            maxY = listBoxElement[0].transform.position.y;
        foreach (BoxElement e in listBoxElement)
        {
            if (e.transform.position.x < minX)
            {
                minX = e.transform.position.x;
            }

            if (e.transform.position.x > maxX)
            {
                maxX = e.transform.position.x;
            }

            if (e.transform.position.y < minY)
            {
                minY = e.transform.position.y;
            }

            if (e.transform.position.y > maxY)
            {
                maxY = e.transform.position.y;
            }
        }

        _width = maxX - minX;
        _height = maxY - minY;
        _center = (maxY - minY) / 2 + minY;
    }

    public void UpdateBoxMaterial()
    {
       foreach(BoxElement box in listBoxElement)
        {
            box.UpdateMeshBox(skinContainer.listBackground[UserDatas.Instance.GetItemIsSelected()].boxMaterial);
        }
    }

    public void ActionItemBegin()
    {

        if (GameController.Instance.LstBoosterInLevel.Count > 0)
        {
            foreach (CommonEnum.ItemGameplay typeItem in GameController.Instance.LstBoosterInLevel)
            {
                if (typeItem == CommonEnum.ItemGameplay.BigHammer)
                {
                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.BigHammer, -1);
                    AutoMatchThreeItem();
                    FireBaseManager.Instant.LogEventWithParameterAsync("popup_start_booster", new Hashtable {
                    {
                        "popup_start_booster",CommonEnum.ItemGameplay.BigHammer.ToString()
                    } });

                    //GameController.Instance.lstBoosterInLevel.Remove(typeItem);
                }

                if (typeItem == CommonEnum.ItemGameplay.GreatFreeze)
                {
                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.GreatFreeze, -1);
                    GameController.Instance.SetTimeFree60();
                    FireBaseManager.Instant.LogEventWithParameterAsync("popup_start_booster", new Hashtable {
                    {
                        "popup_start_booster",CommonEnum.ItemGameplay.GreatFreeze.ToString()
                    } });
                    //GameController.Instance.lstBoosterInLevel.Remove(typeItem);
                }
                if(typeItem == CommonEnum.ItemGameplay.DoubleStar)
                {
                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.DoubleStar, -1);
                    FireBaseManager.Instant.LogEventWithParameterAsync("popup_start_booster", new Hashtable {
                    {
                        "popup_start_booster",CommonEnum.ItemGameplay.DoubleStar.ToString()
                    } });
                }
            }
        }
    }

    public void LoadTimeDurationData(float timeData)
    {
        timeDuration = timeData;
    }

    public void GetInfoBoxElement()
    {
        for (int i = 0; i < listBoxElement.Count; i++)
        {

            listBoxElement[i].LayerInfo = LevelManager.Instance.GetLayer(listBoxElement[i].ID);
            //listBoxElement[i].lstIDItemHolder.Add(listBoxElement[i].LayerInfo[])
            if (listBoxElement[i].LayerInfo == null) continue;
            foreach(var layerinfo in listBoxElement[i].LayerInfo)
            {
                if(layerinfo.itemSkin[0].id != 0)
                listBoxElement[i].lstIDItemHolder.Add(layerinfo.itemSkin[0].id); // add id vao lst single box(duclv sua sau)
            }
            if (listBoxElement[i].LayerInfo == null)
            {
                Debug.Log("layer info " + i + "null");
            }

            listBoxElement[i].SetLayerData(true);
        }
    }

    public void CheckWinLevel()
    {
        if (CheckWin() && !LevelManager.Instance.IsWinLevel)
        {
            Observer.Instance.Notify(ObserverKey.WinLevel);
            LevelManager.Instance.IsWinLevel = true;
            TQT.TutorialControler.Instance.Tutorial_Level.SetActiveHand(false);
            StartCoroutine(WinEffect());
            GameController.Instance.IsStartLevel = false;
            //UIController.Instance.WinLevel();
            SoundController.Instance.PlayWinSound();
            timeWinLevel = GameController.Instance.GetCurrentTimeSecond() - timeStartLevel;

            FireBaseManager.Instant.LogEventWithParameterAsync("gameplay_fail", new Hashtable {
                {
                    "id_level", id
                },
                {
                    "id_screen", "gameplay"
                },
                {
                    "level_time", TimeWinLevel
                },
                {
                    "level_action", tapCubeNumber
                },
                {
                    "level_booster1", useHint
                },
                {
                    "level_booster2", useMagicWand
                },
                {
                    "level_booster3", UseFreezer
                },
                {
                    "level_booster4", useRefresh
                }
        });
        }
    }

    IEnumerator WinEffect()
    {
        yield return new WaitForSeconds(0.5f);
        UIController.Instance.ActiveEffectWin();
        foreach (BoxElement box in listBoxElement)
        {
            box.ActiveAnimation();
            yield return new WaitForSeconds(0.15f);
            SoundController.Instance.PlayThrowItem();
        }

        yield return new WaitForSeconds(1f);
        GameController.Instance.WinShowInter(() =>
        {
            GameController.Instance.CheckAndShowRate((check) => 
            {
                UIController.Instance.WinLevel();
            });
            
        });
        
    }

    public void CheckLoseLevel()
    {
        if (CheckLose() && !LevelManager.Instance.IsLoseLevel)
        {
            Observer.Instance.Notify(ObserverKey.LoseLevel);
            if (GameController.Instance.ItemDataGamePlay.items.Where(item => item.itemId == CommonEnum.ItemGameplay.Refresh).FirstOrDefault().requiredLevel > UserDatas.Instance.CurrentLevel + 1)
            {
                LevelManager.Instance.SortItem();
                GameController.Instance.IsSelected = false;
            }
            else 
            {
                LevelManager.Instance.IsLoseLevel = true;
                GameController.Instance.IsSelected = true;
                StartCoroutine(DelayTurnOnStuckPopup(0.3f));
                GameController.Instance.IsStartLevel = false;
            }
            FireBaseManager.Instant.LogEventWithParameterAsync("gameplay_fail", new Hashtable {
                {
                    "id_level", id
                },
                {
                    "id_screen", "gameplay"
                },
                {
                    "level_time", TimeLoseLevel
                },
                {
                    "level_action", tapCubeNumber
                },
                {
                    "level_booster1", useHint
                },
                {
                    "level_booster2", useMagicWand
                },
                {
                    "level_booster3", UseFreezer
                },
                {
                    "level_booster4", useRefresh
                }
        });
        }
    }

    IEnumerator DelayTurnOnStuckPopup(float time)
    {
        yield return new WaitForSeconds(1);
        UIController.Instance.TurnOnStuckPopup();
    }
    
    public bool CheckLose()
    {
        //Debug.Log("check lose");
        foreach(BoxElement boxElement in listBoxElement)
        {
            if (boxElement.IsLock) continue; 
            if (boxElement.IsLayerFirstContainEmpty())
            {
                //Debug.Log("not lose");
                return false;
            }
        }
        //Debug.Log("lose");
        return true;
    }

    public bool CheckWin()
    {
        //Debug.Log("check win");
        foreach (BoxElement boxElement in listBoxElement)
        {
            if (!boxElement.IsLayerFirstEmpty())
            {
                //Debug.Log("not win");
                return false;
            }
        }
        //Debug.Log("win");
        return true;
    }

    public void LockBox()
    {
        int count = 0;
        List<int> tripleBoxID = new();
        List<int> lockedTripleBoxID = new();
        int numberLocked = LevelManager.Instance.GetDataLockValue().Count;
        List<int> valueLocked = LevelManager.Instance.GetDataLockValue();
        foreach (BoxElement boxElement in listBoxElement)
        {
            if(boxElement.BoxType == BoxType.triple && !boxElement.LayerFisrt.IsLayerContainEmpty() && boxElement.LayerFisrt.IsLayerContainTwoItemHasSameID())
            {

                tripleBoxID.Add(boxElement.ID);
            }
        }

        for (int i = 0; i < numberLocked; i++)
        {
            int index = Random.Range(0,tripleBoxID.Count);
            lockedTripleBoxID.Add(tripleBoxID[index]);
            
            boxLockedID.Add(tripleBoxID[index]);
            tripleBoxID.RemoveAt(index);
        }

        foreach (BoxElement boxElement in listBoxElement)
        {
            for (int i = 0; i < numberLocked; i++)
            {
                if (boxElement.ID == lockedTripleBoxID[i])
                {
                    boxElement.LockBox(true);
                    boxElement.IsLock = true;
                    boxElement.SetLock(valueLocked[i]);
                    if (!LevelManager.Instance.IsSort || !LevelManager.Instance.IsReplay) 
                    {
                        LevelManager.Instance.CurrentValueLock.Add(valueLocked[i]);
                    }
                    
                }
            }
        }
    }

    public void DecreaseNumberLock(object data)
    {
        if (boxLockedID.Count == 0) return;
        if (listBoxElement.Where(box => box.ID == boxLockedID[0]).FirstOrDefault().ValueLockedNumber <= 0)
        {
            boxLockedID.RemoveAt(0);
            LevelManager.Instance.CurrentValueLock.RemoveAt(0);
        }
        if (boxLockedID.Count == 0) return;
        listBoxElement.Where(box => box.ID == boxLockedID[0]).FirstOrDefault().DecreaseLockNumber();
        LevelManager.Instance.CurrentValueLock[0]--;
        return;
    }

    public void AddItemToListLayerFirst(object data)
    {
        int id = (int)data;
        if (!lstItemLayerFirst.ContainsKey(id)) lstItemLayerFirst[id] = 0;
       
        lstItemLayerFirst[id] += 1;
        DisplayFirstLayer();
    }

    public void DecreaseItemInListLayerFirst(object data)
    {
        int id = (int)data;
        if (!lstItemLayerFirst.ContainsKey(id)) {
            lstItemLayerFirst[id] = 0;
        }
        lstItemLayerFirst[id] -= 1;
    }

    public void DisplayFirstLayer()
    {
        string a="";
        foreach (var kvp in lstItemLayerFirst)
        {
            int id = kvp.Key;
            int val = kvp.Value;
            a += " |" +id.ToString() + " " + val.ToString();
            
        }
    }

    /*lay ra 3id có số lượng item lớn nhất theo chiều giảm*/
    public Dictionary<int,int> GetTopThreeItem()
    {
        Dictionary<int, int> value = new Dictionary<int, int>();
        // Sắp xếp Dictionary dựa trên giá trị giảm dần
        var sortedListItem = lstItemLayerFirst.OrderByDescending(x => x.Value);
        foreach (var kvp in sortedListItem)
        {
            int id = kvp.Key; 
            int val = kvp.Value; 

        }
        // Lấy ra 3 key có giá trị lớn nhất
        var top3IDs = sortedListItem.Take(3).Select(x => x.Key).ToList();

        int randomID = Random.Range(1, LevelManager.Instance.RandomItem.LstIdItemRandom.Count);
        //Get3SameType();
        string a = "";
        foreach (int id in top3IDs)
        {
            a += " |" + id;
            value.Add(id, randomID);
        }


        /*foreach(int id in value.Keys)
        {
            Debug.Log(id + " "+ value[id]);
        }*/
        return value ;
    }


    public int GetItemIDCanMatch()
    {
        Dictionary<int, int> value = new Dictionary<int, int>();
        // Sắp xếp Dictionary dựa trên giá trị giảm dần
        var sortedListItem = lstItemLayerFirst.OrderByDescending(x => x.Value);
        foreach (var kvp in sortedListItem)
        {
            int id = kvp.Key;
            int val = kvp.Value;
            return id;
            
        }
        List<int> valuesList = lstItemLayerFirst.Keys.ToList();
        // Lấy một giá trị ngẫu nhiên từ danh sách
        int randomValue = Random.Range(1,valuesList.Count);

        return randomValue;
    }

    public List<int> GetThreeItemIDCanMatch()
    {
        List<int> threeItem = new();
        // Sắp xếp Dictionary dựa trên giá trị giảm dần
        var sortedListItem = lstItemLayerFirst.OrderByDescending(x => x.Value);
        foreach (var kvp in sortedListItem)
        {
            int id = kvp.Key;
            int val = kvp.Value;
            threeItem.Add(id);
            if(threeItem.Count == 3)
            {
                return threeItem;
            }
        }
        List<int> valuesList = lstItemLayerFirst.Keys.ToList();
        // Lấy một giá trị ngẫu nhiên từ danh sách
        int randomValue = Random.Range(1, valuesList.Count);

        return threeItem;
    }


    public void ReplaceItem()
    {
        Observer.Instance.Notify(ObserverKey.ReplaceItem, GetTopThreeItem());
    }

    public void AutoMatchItem()
    {
        Observer.Instance.Notify(ObserverKey.AutoMatchItem, GetItemIDCanMatch());
        //Observer.Instance.Notify(ObserverKey.DecreaseNumberLock);
        CheckLoseLevel();
    } 
    
    public void AutoMatchThreeItem()
    {
        Observer.Instance.Notify(ObserverKey.AutoMatchThreeItem, GetThreeItemIDCanMatch());
        //Observer.Instance.Notify(ObserverKey.DecreaseNumberLock);
        CheckLoseLevel();
    }
}
