using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using CommonEnum;

namespace ShopQuyNX
{
    [CreateAssetMenu(fileName = "ShopItemData", menuName = "GameConfiguration/ShopItemData")]
    public class ShopItemData : ScriptableObject
    {
        [Serializable]
        public class CommonItemShop
        {
            public bool isIAP;

            [ConditionalField(nameof(isIAP), false)]
            public string idIAP;
            [ConditionalField(nameof(isIAP), false)]
            public float priceIAP;

            [ConditionalField(nameof(isIAP), true)]
            public string idRewardAds;

            [ConditionalField(nameof(isIAP), true)]
            public float totalMinuteToUnlock;

        }

        [Serializable]
        public class Pack : CommonItemShop
        {
            public string title;
            public int amountGold;
            public ItemShop[] itemShop;
        }

        [Serializable]
        public class SmallItem : CommonItemShop
        {
            public bool isGold;

            [ConditionalField(nameof (isGold), true)]
            public ItemShop item;

            [ConditionalField(nameof(isGold), false)]
            public int amountCoin;
        }

        [Serializable]
        public class ItemShop
        {
            public ItemGameplay id;
            public int amount;
            public string amountText;
        }

        [Serializable]
        public class NoAdsItem
        {
            public string idIAP;
            public Sprite icon;
            public float priceIAP;
            public int amountCoint;
            public Currency currency;
        }

        [Serializable]
        public class ItemShopToShow
        {
            public ItemShopType type;

            [ConditionalField(nameof(type), false, ItemShopType.NoAds)]
            public NoAdsItem noAdsInformation;

            [ConditionalField(nameof(type), false, ItemShopType.SeasonPack)]
            public Pack seasonPass;

            [ConditionalField(nameof(type), false, ItemShopType.LimitedPack)]
            public Pack limitedPack;

            [ConditionalField(nameof(type), false, ItemShopType.NormalPack)]
            public Pack normalPack;

            [ConditionalField(nameof(type), false, ItemShopType.SmallItem)]
            public SmallItem smallItem;
        }

        public ItemShopToShow[] allItemShops;
    }

    
}

