using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionPref
{
    static ConfigData configData;
    public static InGameData inGameData;

    public static void SetConfigData(ConfigData currentData)
    {
        configData = currentData;
        SaveConfigData();
    }

    public static void SetInGameData(InGameData inGameData)
    {
        SessionPref.inGameData = inGameData;
        SaveInGameData();
    }

    public static void AddMoney(int value)
    { 
        inGameData.currentMoney += value;
        SaveInGameData();
    }


    public static void AddItemOwned(int idItem)
    {
        SessionPref.inGameData.ShopItemData.OwnedItemID.Add(idItem);
        SaveInGameData();
    }

    public static void AddWatchAdsCount(int idItem)
    {
        if(SessionPref.inGameData.ShopItemData.WatchAdsCount.Count == 0)
        {
            SetDefaultWatchAdsData();
        }
        Debug.Log("count: " + SessionPref.inGameData.ShopItemData.WatchAdsCount.Count);
        SessionPref.inGameData.ShopItemData.WatchAdsCount[idItem].WatchCount += 1;
        SaveInGameData();
    }

    private static void SetDefaultWatchAdsData()
    {
        Debug.Log("itemskin count: "+ ShopSkinUI.Instance.RecordSkin.itemSkin);
        for(int i = 0; i < ShopSkinUI.Instance.RecordSkin.itemSkin.Length; i++)
        {
            inGameData.ShopItemData.WatchAdsCount.Add(new());
            inGameData.ShopItemData.WatchAdsCount[i].WatchCount = 0;
            inGameData.ShopItemData.WatchAdsCount[i].ID = i + 1;
        }
    }

    public static int GetWatchAdsCount(int idItem)
    {
        if (inGameData.ShopItemData.WatchAdsCount.Count == 0) return 0;
        return inGameData.ShopItemData.WatchAdsCount[idItem].WatchCount;
    }

    public static int GetItemIsSelected()
    {
        return inGameData.ShopItemData.ItemIsSelected;
    }

    public static void SetItemIsSelected(int idItem)
    {
        inGameData.ShopItemData.ItemIsSelected = idItem;
        SaveInGameData();
    }

    public static List<int> GetOwnedItem()
    {
        if (inGameData.ShopItemData != null)
        return inGameData.ShopItemData.OwnedItemID;
        else
        {
            Debug.Log(inGameData.ShopItemData);
            inGameData.ShopItemData.OwnedItemID = new List<int>();
            return inGameData.ShopItemData.OwnedItemID;
        }
    }

    public static int GetMoney()
    {
        return inGameData.currentMoney;
    }
    
    public static void SetValueMusic(bool value)
    {
        configData.OnMusic = value;
        SaveConfigData();
    }
    public static bool GetValueMusic()
    {
        return configData.OnMusic;
    }
    public static void SetValueSound(bool value)
    {
        configData.OnSound = value;
        SaveConfigData();
    }
    public static bool GetValueSound()
    {
        return configData.OnSound;
    }
    public static void SetValueVibrate(bool value)
    {
        configData.OnVibrate = value;
        SaveConfigData();
    }
    public static bool GetValueVibrate()
    {
        return configData.OnVibrate;
    }



    public static void SaveConfigData()
    {
        string data = JsonUtility.ToJson(configData);
        Debug.Log("set " + data);
        PlayerPrefs.SetString("CONFIGDATA", data);
    }

    public static void SaveInGameData()
    {
        string data = JsonUtility.ToJson(inGameData);
        PlayerPrefs.SetString("INGAME_DATA", data);
    }
}
