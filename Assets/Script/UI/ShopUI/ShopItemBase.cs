using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopItemBase : MonoBehaviour
{
    public int id;
    [SerializeField] protected Image Outline;
    [SerializeField] protected Image hubItem;
    [SerializeField] protected Image typeCostIcon;
    [SerializeField] protected TextMeshProUGUI cost;
    [SerializeField] protected Sprite SelectedImage;
    [SerializeField] protected Sprite UnSelectedImage;
    [SerializeField] protected Sprite hubAds;
    [SerializeField] protected Sprite hubGold;

    [SerializeField] protected Sprite iconTypeAds;
    [SerializeField] protected Sprite iconTypeGold;
    protected Button itemButton;
    [SerializeField] protected Button hubButton;
    public int ID
    {
        get
        {
            return id;
        }
        set
        {
            id = value;
        }
    }

    public Image HubItem
    {
        get
        {
            return hubItem;
        }
        set
        {
            hubItem = value;
        }
    }

    public TextMeshProUGUI Cost
    {
        get
        {
            return cost;
        }
        set
        {
            cost = value;
        }
    }

    private void OnEnable()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void OnClickItem()
    {

    }

    public void TurnOnOutline()
    {
        Outline.sprite = SelectedImage;
    }

    public void TurnOffOutline()
    {
        Outline.sprite = UnSelectedImage;
    }
}



