using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpawnItemData", menuName = "GameConfiguration/SpawnItemData", order = 1)]
public class SpawnItemData : ScriptableObject
{
    public List<ItemElementData> ListItemData;
}

[System.Serializable]
public class ItemElementData
{
    public ItemElement prefabObj;
}
