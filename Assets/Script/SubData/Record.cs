using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Record : MonoBehaviour
{

}
[System.Serializable]
public struct LayerInfo
{
    public ItemInfo[] itemSkin;


}

[System.Serializable]
public struct ItemInfo
{
    public int id;
    public ItemElement ItemElement;
    public bool isSpecial;
}

public struct RecordItemSkin
{
    public ItemInfoSkin[] itemSkin;
    public struct ItemInfoSkin
    {
        public int id;
        public string name;
        public int cost;
        public string name_skin;
        public int type;
    }
    
}
public struct RecordRate
{
    public ArrayRate[] rate;
    public struct ArrayRate
    {
        public int index;
    }
}
