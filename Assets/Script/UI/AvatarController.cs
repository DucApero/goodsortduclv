using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AvatarController : MonoBehaviour
{
    [SerializeField] private FlagDataContainer flagContainer;
    private Image flagIcon;

    public string name;
    public string Name
    {
        get
        {
            return name;
        }
        set
        {
            name = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Observer.Instance.AddObserver(ObserverKey.ChangeAvatar, ChangeAvatar);
        InitAvatar();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeAvatar(object data)
    {
        int id = (int)data;
        flagIcon.sprite = flagContainer.FlagContainer[id].FlagIcon;
        UserDatas.Instance.SetAvartarID(id);
        ConfigGameController.Instance.SetAvatar(id);
    }

    public void UpdateName(string text)
    {
        name = text;
    }

    public void InitAvatar()
    {
        flagIcon = this.gameObject.GetComponent<Image>();
        flagIcon.sprite = flagContainer.FlagContainer[UserDatas.Instance.GetUserData().IDAvatar].FlagIcon;
    }


}

