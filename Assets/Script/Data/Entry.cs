using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Entry : Singleton<Entry>
{
    private void Awake()
    {
        Application.targetFrameRate = 120;
        DontDestroyOnLoad(this);
        // Disable screen dimming
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    private void OnEnable()
    {
        SetDefaultDataAppOpen();
    }

    private void Start()
    {
        
    }

    void SetDefaultDataAppOpen()
    {
        SessionPref.SetConfigData(GetCurrentData());
        SessionPref.SetInGameData(GetInGameData());
        //SessionPref.SetUserData(GetUserData());
    }

    ConfigData GetCurrentData()
    {
        ConfigData configData;

        if (PlayerPrefs.HasKey("CONFIGDATA"))
        {
            string data = PlayerPrefs.GetString("CONFIGDATA");
            Debug.Log("get " + data);
            configData = JsonUtility.FromJson<ConfigData>(data);
        }
        else
        {
            configData = new ConfigData();

        }

        return configData;
    }

    InGameData GetInGameData()
    {
        InGameData inGameData;
        if (PlayerPrefs.HasKey("INGAME_DATA"))
        {
            string data = PlayerPrefs.GetString("INGAME_DATA");
            inGameData = JsonUtility.FromJson<InGameData>(data);
        }
        else
        {
            inGameData = new();
            inGameData.currentMoney = 0;
        }

        return inGameData;
    }



/*    UserInfo GetUserData()
    {
        UserInfo userData;

        if (PlayerPrefs.HasKey("USERDATA"))
        {
            string data = PlayerPrefs.GetString("USERDATA");
            Debug.Log("get " + data);
            userData = JsonUtility.FromJson<UserInfo>(data);
        }
        else
        {
            userData = new();
            userData.IDAvatar = 1;
            userData.Name = "=ANM";
        }

        return userData;
    }*/

}
