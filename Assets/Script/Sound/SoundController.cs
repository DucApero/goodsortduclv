using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    [SerializeField] AudioSource audioSourceMusic;
    [SerializeField] AudioSource audioSourceSFX;
    [SerializeField] AudioSource audioSourceMusic2;

    [SerializeField]
    AudioClip backgroundGameplay, backgroundHome,
        clickButton, activeBooster, throwItem, tapItem, win, lose, openRewardBox;
    [SerializeField] AudioClip[] lstMatchSound;
    public AudioClip BackgroundGameplay { get => backgroundGameplay; }
    public AudioClip BackgroundHome { get => backgroundHome; }
    public AudioClip ClickButton { get => clickButton; }
    public AudioClip ActiveBooster { get => activeBooster; }
    //public AudioClip MatchSound { get => matchSound; }
    public AudioClip ThrowItem { get => throwItem; }
    public AudioClip TapItem { get => tapItem; }

    public static SoundController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    private void Start()
    {
        bool isMusicOn = UserDatas.Instance.IsMusicActive();
        ChangeVolumnMusic(isMusicOn);

        bool isSFXOn = UserDatas.Instance.IsSFXActive();
        ChangeVolumnSFX(isSFXOn);
    }

    public void ChangeMusicSetting()
    {
        UserDatas.Instance.ChangeMusicSetting();
        bool isOn = UserDatas.Instance.IsMusicActive();
        ChangeVolumnMusic(isOn);
    }

    public void ChangeSFXSetting()
    {
        UserDatas.Instance.ChangeSFXSetting();
        bool isOn = UserDatas.Instance.IsSFXActive();
        ChangeVolumnSFX(isOn);
    }

    void ChangeVolumnMusic(bool isOn)
    {
        audioSourceMusic.volume = isOn ? 1 : 0;
        audioSourceMusic2.volume = isOn ? 1 : 0;
    }

    void ChangeVolumnSFX(bool isOn)
    {
        audioSourceSFX.volume = isOn ? 1 : 0;
    }

    public void ChangeVolumnMusic(float volume)
    {
        audioSourceMusic.volume = volume;
    }

    public void PlayOneShot(AudioClip clip)
    {
        audioSourceSFX.PlayOneShot(clip);
    }

    public void PlayBackgroundGameplay()
    {
        if (audioSourceMusic.isPlaying) audioSourceMusic.Stop();
        audioSourceMusic.clip = backgroundGameplay;
        audioSourceMusic.volume = .15f;
        audioSourceMusic.Play();
        audioSourceMusic2.volume = 0;
    }

    public void PlayBackgroundHome()
    {
        if (audioSourceMusic.isPlaying) audioSourceMusic.Stop();
        if (audioSourceMusic2.isPlaying) audioSourceMusic2.Stop();
        audioSourceMusic.clip = backgroundHome;
        audioSourceMusic.volume = 1;
        audioSourceMusic2.volume = 1;
        audioSourceMusic.Play();
        audioSourceMusic2.Play();
    }

    public void PlayClickButton() { PlayOneShot(ClickButton); }
    public void PlayActiveBooster() { PlayOneShot(ActiveBooster); }
    public void PlayThrowItem() { PlayOneShot(ThrowItem); }
    public void PlayTapItem() { PlayOneShot(TapItem); }
    public void PlayWinSound()
    {
        audioSourceMusic.volume = 0f;
        PlayOneShot(win);
    }
    public void PlayLoseSound()
    {
        audioSourceMusic.volume = 0f;
        PlayOneShot(lose);
    }
    public void PlayOpenRewardBox()
    {
        PlayOneShot(openRewardBox);
    }
    public void PlayMatchSound(float delay, int countCombo)
    {
        StartCoroutine(ActiveMatchSount(delay, countCombo));
    }

    IEnumerator ActiveMatchSount(float delay, int countCombo)
    {
        yield return new WaitForSeconds(delay);
        switch (countCombo)
        {
            case 1:
                PlayOneShot(lstMatchSound[0]);
                break;
            case 2:
                PlayOneShot(lstMatchSound[1]);
                break;
            case 3:
                PlayOneShot(lstMatchSound[2]);
                break;
            case 4:
                PlayOneShot(lstMatchSound[3]);
                break;
            case 5:
                PlayOneShot(lstMatchSound[4]);
                break;
            case 6:
                PlayOneShot(lstMatchSound[5]);
                break;
            case 7:
                PlayOneShot(lstMatchSound[6]);
                break;
            default:
                PlayOneShot(lstMatchSound[7]);
                break;
        }
    }
}
