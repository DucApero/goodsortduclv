using CustomAd;
using DVAH;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IShopItem : MonoBehaviour
{
    [HideInInspector] public string idRewardAds;
    [HideInInspector] public float totalMinuteToUnlock;
    [HideInInspector] public bool isIAP;
    [HideInInspector] public string idIAP;
    [HideInInspector] public float priceIAP;

    protected bool isReadyToBuy;

    public void OnBuy()
    {

        if (isIAP)
        {
            IAPManager.Instant.BuyProductID(idIAP, (isSuccess) =>
            {
                if (isSuccess) { 
                    OnClickBuy();
                    FireBaseManager.Instant.LogEventWithParameterAsync("setting_start", new Hashtable {
                {
                    idIAP,"home"
                }
        });
                } 
            });
        }
        else
        {
            CustomAdManager.Intance.ShowRewardAds(null, OnClickBuy, null);
            FireBaseManager.Instant.LogEventWithParameterAsync("setting_start", new Hashtable {
                {
                    "id_screen","home"
                }
        });
        }
    }
    public abstract void OnClickBuy();


}
