using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonAnim 
{
    public const string die = "die";
    public const string die2 = "die2";
    public const string run = "run";
    public const string run2 = "run2";
    public const string destroy1 = "destroy1";
    public const string destroy2 = "destroy2";
    public const string getHit = "get_hit";
    public const string transform = "transform";
    public const string idle = "idle";
    public const string attack = "attack";
    public const string jumb = "jumb";
    public const string animation = "animation";
    public const string stun = "stun";
    public const string start = "start";
}
