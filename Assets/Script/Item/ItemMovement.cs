﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMovement : MonoBehaviour, IDrag
{
    [SerializeField] private Transform startRaycastPoint;
    private Vector3 mousePosition;
    public float pickupZPosition = -2;
    private Camera mainCamera;
    private Vector3 prePos;
    private bool isSelected;
    public Layer current_layer;
    public Layer CurrentLayer
    {
        get => current_layer;
        set => current_layer = value;
    }
    private Action<BoxElement> callBackAddItem;
    public Action<BoxElement> CallBackAddItem
    {
        set => callBackAddItem = value;
    }

    void Start()
    {
        mainCamera = Camera.main;
    }


    private Vector3 GetMousePos()
    {
        return Camera.main.WorldToScreenPoint(transform.position);
    }

    public void OnMouseDown()
    {
        if (GameController.Instance.IsSelected) return;
        SoundController.Instance.PlayTapItem();
        MouseDown();
    }

    public void OnMouseDrag()
    {

        if (Physics.Raycast(startRaycastPoint.position, transform.TransformDirection(Vector3.forward), out RaycastHit hitinfo, 200f, 1 << 6))
        {
            Debug.DrawRay(startRaycastPoint.position, transform.TransformDirection(Vector3.forward) * hitinfo.distance, Color.red);
        }
        else
        {
            Debug.DrawRay(startRaycastPoint.position, transform.TransformDirection(Vector3.forward) * hitinfo.distance, Color.blue);
        }

        if (!GameController.Instance.IsSelected)
        {
            GameController.Instance.IsSelected = true;
            isSelected = true;
            MouseDrag();
            if (!GameController.Instance.IsStartLevel) GameController.Instance.IsStartLevel = true;
        }
        else
        {
            if (isSelected) MouseDrag();
        }
    }

    public void OnMouseUp()
    {
        if (isSelected)
        {
            SoundController.Instance.PlayThrowItem();
            MouseUp();
        }
        LevelManager.Instance.CurrentLevel.TapCubeNumber++;
    }

    /*public void SetPositionItem()
    {
        if (isSelectedBox)
        {
            Transform nearestTransform = null;
            float nearestDistance = Mathf.Infinity;

            Transform boxNearestTransform = null;
            float boxNearestDistance = Mathf.Infinity;

            List<Transform> pos = new List<Transform>();
            List<BoxController> boxs;

            foreach (Transform tf in PositionItemList.Instance.listPos)
            {
                float distance = Vector2.Distance(this.transform.position, tf.position);
                if (distance < boxNearestDistance)
                {
                    boxNearestDistance = distance;
                    boxNearestTransform = tf;
                }
            }

            Debug.Log(boxNearestTransform.gameObject.name + "near");

            foreach (Transform pos_item in boxNearestTransform.GetComponent<BoxController>().pos)
            {
                float distance = Vector2.Distance(this.transform.position, pos_item.position);
                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearestTransform = pos_item;
                }
            }

            this.transform.position = nearestTransform.position;
        }
        else
        {
            this.transform.position = prePos;
        }
    }*/

    protected void CheckSelectedBox()
    {
        if (Physics.Raycast(startRaycastPoint.position, transform.TransformDirection(Vector3.forward), out RaycastHit hitinfo, 200f, 1 << 6))
        {
            Debug.DrawRay(startRaycastPoint.position, transform.TransformDirection(Vector3.forward) * hitinfo.distance, Color.red);
            if (hitinfo.collider.gameObject.tag == "Box")
            {
                callBackAddItem?.Invoke(hitinfo.collider.gameObject.GetComponent<BoxElement>());
            }
            else
            {
                MoveToPrePos();
            }
        }
        else
        {
            Debug.DrawRay(startRaycastPoint.position, transform.TransformDirection(Vector3.forward) * 1000, Color.red);

            MoveToPrePos();
        }
    }

    public void MoveToTarget(Transform target)
    {
        //this.transform.position = target.position;
        
        transform.DOMove(target.position, 0.1f).OnComplete(() =>
        {
            if(!LevelManager.Instance.IsLoseLevel)
            GameController.Instance.IsSelected = false;
            //transform.localPosition = Vector3.zero;
        });
        if (!current_layer.CheckMatch())
        {
            TQT.TutorialControler.Instance.Tutorial_Level.SetUpHand();

        }
    }

    public void MoveToPrePos()
    {
        //this.transform.position = prePos;
        transform.DOLocalMove(Vector3.zero, 0.1f).OnComplete(() =>
        {
            GameController.Instance.IsSelected = false;
            //transform.localPosition = Vector3.zero;
            if (current_layer.CheckMatch())
            {
                Observer.Instance.Notify(ObserverKey.DecreaseNumberLock);
                current_layer.MatchListItem();
                current_layer.CurrentBox.ChangeLayer();
                
            }
            else
            {

            }
            current_layer.CheckStatusGame();
            LevelManager.Instance.CurrentLevel.CheckLoseLevel();
        });
        if(SessionPref.GetValueVibrate()) Vibration.Vibrate(500);
    }

    public void MouseDown()
    {
        prePos = this.transform.position;
        mousePosition = Input.mousePosition - GetMousePos();
        
    }

    public void MouseUp()
    {
        CheckSelectedBox();
        isSelected = false;
        
    }

    public void MouseDrag()
    {
        Vector3 target = Camera.main.ScreenToWorldPoint(Input.mousePosition - mousePosition);
        transform.position = new Vector3(target.x, target.y, -2); //target;
    }
}
