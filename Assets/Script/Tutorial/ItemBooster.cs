using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using TMPro;

[System.Serializable]
public enum typeItem {
    Booster1,
    Booster2
}
  namespace TQT
{
    public class ItemBooster : MonoBehaviour
    {
        public const string Key_Bosster = "Booster";
        [SerializeField] string Name;
        [SerializeField] Image imgLight;
        [SerializeField] RectTransform arrow;
        [SerializeField] TMP_Text text;
        [SerializeField] float PosYNew;
        public Transform target;
        [SerializeField] float delay=0.2f;
        public float interval = 0.5f;
        bool isTutorial;
        public Sprite imgItem;
        public Sprite sprlock;
        public typeItem typeItem;
        public UnityEvent onClick;
        public int id;
/*        public void OnEnable()
        {
            if (!GameController.Instance.IsSelected)
            {
                GameController.Instance.IsSelected = true;
            }
        }*/


        public bool IsTutorial
        {
            get { return isTutorial; }
            set 
            {
                isTutorial = value;
                PlayerPrefs.SetInt(Key_Bosster + name, isTutorial ? 1 : 0);
            }
        }
        public void Init()
        {
            gameObject.SetActive(true);
            isTutorial = PlayerPrefs.GetInt(Key_Bosster + name, 1)!=0;

        }
        public void GameStart()
        {
            Observer.Instance.Notify(ObserverKey.ShowTutorial, true);
            
            text.text = Name;
            gameObject.SetActive(true);
            if (imgLight!=null)
                StartBlink();

            AnimArrow();

        }
        public void GameEnd()
        {
            IsTutorial= false;
            gameObject.SetActive(false);
            onClick?.Invoke();
            Observer.Instance.Notify(ObserverKey.ShowTutorial, false);
            if (typeItem == typeItem.Booster1)
            {
                if (id ==0)
                {
                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.SmallHammer, 3);
                    UIController.Instance.GamePlayUI.DrumButton.UpdateCountBooster();
                }
                else if (id==1)
                {
                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Refresh, 3);
                    UIController.Instance.GamePlayUI.RefreshButton.UpdateCountBooster();

                }
                else if (id ==2)
                {
                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.MagicWand, 3);
                    UIController.Instance.GamePlayUI.MagicWand.UpdateCountBooster();

                }
                else if (id ==3)
                {

                    UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Freeze, 3);
                    UIController.Instance.GamePlayUI.FreezerButton.UpdateCountBooster();

                }
                GameController.Instance.IsStartLevel = false;
            }
        }

        //Effect Level

        public void StartBlink()
        {
            InvokeRepeating("ToggleState", delay, interval);

        }
        public void ToggleState()
        {
            imgLight.enabled = !imgLight.enabled;
        }
        public void AnimArrow()
        {
           arrow.DOAnchorPosY(PosYNew, delay)
                .SetLoops(-1, LoopType.Yoyo);
        }
      
    }
}

