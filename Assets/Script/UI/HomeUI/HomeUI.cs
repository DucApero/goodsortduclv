using DG.Tweening;
using DVAH;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HomeUI : MonoBehaviour
{
    [SerializeField] private ConfigGameController Setting;
    [SerializeField] private StarChest starChest;
    [SerializeField] private PopUpShop popUpShop;
    [SerializeField] private ShopSkinUI shopSkinUI;
    [SerializeField] private ConfigGameController PopupSetting;
    [SerializeField] private BuyHeartPopup buyHeartPopup;
    [SerializeField] private HeartBar heartBar;
    [SerializeField] private Button btnRemoveAds;
    [SerializeField] private Button playButton;
    [SerializeField] private Button btnStarCheat;
    [SerializeField] private Button btnSkin;
    [SerializeField] private Button btnShop;
    [SerializeField] private Button btnHome;
    [SerializeField] private Button btnSetting;
    [SerializeField] private Button buyHeartButton;

    [SerializeField] private GameObject UiLobby;
    [SerializeField] private Sprite[] sprButton;
    [SerializeField] private GameObject textHome, textShop;
    [SerializeField] private List<AnimationComponent> lstAnim;
    [SerializeField] private TextMeshProUGUI txtLevel;
    [SerializeField] private TextMeshProUGUI txtProgress;
    [SerializeField] private Slider sliderStar;
    int indexAnim = 0;
    int timeDelayAnim = 3;
    private bool dontClick = false;
    private float timeDelay = 1.5f;
    private void Awake()
    {
        Observer.Instance.AddObserver(ObserverKey.UpdateStar, UpdateProgress);
    }
    private void OnDestroy()
    {
        Observer.Instance.RemoveObserver(ObserverKey.UpdateStar, UpdateProgress);
    }
    // Start is called before the first frame update
    private void OnEnable()
    {
        UpdateProgress(null);
        UpdateTextLevel();
        dontClick = false;
    }
   
    void Start()
    {
        playButton.onClick.AddListener(() =>
        {
            if (dontClick) return;
            FireBaseManager.Instant.LogEventWithParameterAsync("home_btn_level", new Hashtable {
                {
                    "id_screen","home"
                }
            });
            if (!dontClick)
            {
                dontClick = true;
                StartCoroutine(GameController.Instance.DelayAction(()=> 
                {
                    dontClick = false;
                }, timeDelay));
            }
            SoundController.Instance.PlayClickButton();
            playButton.transform.DOScale(0.75f, 0.25f / 2f).OnComplete(() =>
            {
                playButton.transform.DOScale(1f, 0.25f / 2f).SetEase(Ease.OutBack).OnComplete(() =>
                {
                    //UIController.Instance.ShowpoupRewardPlay(UIController.Instance.MoveToGamePlay);
                    if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) <= 0)
                    {
                        buyHeartPopup.gameObject.SetActive(true);
                    }
                    else
                    {
                        if(LevelManager.Instance.IndexLevel == 0)
                        {
                            UIController.Instance.MoveToGamePlay(); // tutorial
                        }
                        else
                        {
                            UIController.Instance.ShowpoupRewardPlay(UIController.Instance.MoveToGamePlay, false);
                        }
                        
                    }
                });
            });
           

        });

        btnRemoveAds.onClick.AddListener(()=> {
            ShowpoupAds();
            FireBaseManager.Instant.LogEventWithParameterAsync("home_btn_noads", new Hashtable {
                {
                    "id_screen","home"
                }
            });
        });

        btnStarCheat.onClick.AddListener(()=> {
            ShowpoupStarCheat();
            FireBaseManager.Instant.LogEventWithParameterAsync("home_btn_starchest", new Hashtable {
                {
                    "id_screen","home"
                }
            });
        });

        btnSkin.onClick.AddListener(()=> {
            ShowpoupSkin();
            FireBaseManager.Instant.LogEventWithParameterAsync("home_btn_skin", new Hashtable {
                {
                    "id_screen","home"
                }
            });
        });

        btnSetting.onClick.AddListener(()=> {
            TurnOnSetting();
            FireBaseManager.Instant.LogEventWithParameterAsync("home_btn_setting", new Hashtable {
                {
                    "id_screen","home"
                }
            });
        });

        btnShop.onClick.AddListener(() =>
        {
            SoundController.Instance.PlayClickButton();
            ClickBtnShop();
            FireBaseManager.Instant.LogEventWithParameterAsync("home_btn_shop", new Hashtable {
                {
                    "id_screen","home"
                }
            });
        });

        btnHome.onClick.AddListener(() =>
        {
            SoundController.Instance.PlayClickButton();
            ClickBtnHome();
        });

        buyHeartButton.onClick.AddListener(() =>
        {
            FireBaseManager.Instant.LogEventWithParameterAsync("home_btn_heart", new Hashtable {
                {
                    "id_screen","home"
                }
            });
            if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) < 5)
            {
                buyHeartPopup.gameObject.SetActive(true);
            }
            else
            {
                UIController.Instance.EnoughHeart();
            }
        });
        
    }
    public void BuyRemoveAds()
    {
        btnRemoveAds.gameObject.SetActive(false);
    }

    public void ShowBuyHeartPopup(UnityAction action)
    {
        buyHeartPopup.gameObject.SetActive(true);
        buyHeartPopup.CallBack = action;
    }
    public void DecreaseHeart()
    {
        UserDatas.Instance.SetCurrency(CommonEnum.Currency.Heart, -1);
        heartBar.IsSetDefault = false;
        UserDatas.Instance.SetLastUseHeart(GameController.Instance.GetCurrentTimeSecond());
        if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) <= 5)
        {
            if (heartBar.CountDown == false)
            {
                UserDatas.Instance.SetLastLockHeartDate(GameController.Instance.GetCurrentTimeSecond());
                Observer.Instance.Notify(ObserverKey.CountDownHeart, true);
            }
        }
    }
    public void ClickBtnHome()
    {
        btnShop.GetComponent<Image>().sprite = sprButton[1];
        btnHome.GetComponent<Image>().sprite = sprButton[0];
        textHome.SetActive(true);
        textShop.SetActive(false);
        ActivePoupShop(false);
        FireBaseManager.Instant.LogEventWithParameterAsync("btn_home", new Hashtable {
                {
                    "id_screen","home"
                }
        });
    }
    public void ClickBtnShop()
    {
        btnShop.GetComponent<Image>().sprite = sprButton[0];
        btnHome.GetComponent<Image>().sprite = sprButton[1];
        textHome.SetActive(false);
        textShop.SetActive(true);
        ActivePoupShop(true);
        FireBaseManager.Instant.LogEventWithParameterAsync("shop_start", new Hashtable {
                {
                    "id_screen","home"
                }
        });
    }
    public void ActivePopup(bool isActive)
    {
        this.gameObject.SetActive(isActive);
        ActiveUiLobby(isActive);
        if (isActive)
        {
            StartCoroutine(PlayAnim(0));
        }
    }

    public void ActivePoupShop(bool isActive)
    {
        popUpShop.ActivePopup(isActive);
    }
    public void ShowpoupStarCheat()
    {
        SoundController.Instance.PlayClickButton();
        starChest.ActivePopup(true);
    }
    public void ShowpoupSkin()
    {
        SoundController.Instance.PlayClickButton();
        shopSkinUI.ActivePopup(true);
    }
    public void ShowpoupAds()
    {
        SoundController.Instance.PlayClickButton();
        UIController.Instance.ActivePopupAds(true);
    }
    public void OnClickPlayButton()
    {
        SoundController.Instance.PlayClickButton();
        UIController.Instance.MoveToGamePlay();
    }
    public void ActiveUiLobby(bool isActive)
    {
        UiLobby.SetActive(isActive);
    }
    public void TurnOnSetting()
    {
        SoundController.Instance.PlayClickButton();
        Setting.gameObject.SetActive(true);
        FireBaseManager.Instant.LogEventWithParameterAsync("setting_start", new Hashtable {
                {
                    "id_screen","home"
                }
        });
    }
    public void TurnOffSetting()
    {
        SoundController.Instance.PlayClickButton();
        Setting.gameObject.SetActive(false);
    }

    private IEnumerator PlayAnim(float time)
    {
        yield return new WaitForSeconds(time);
        lstAnim[indexAnim].SetStateSkeUI(0, CommonAnim.start, false);
        indexAnim++;
        if (indexAnim >= lstAnim.Count) indexAnim = 0;
        StartCoroutine(PlayAnim(timeDelayAnim));

    }
    public void UpdateTextLevel()
    {
        txtLevel.text = "Level " + (UserDatas.Instance.CurrentLevel +1).ToString();
    }
    public void TurnOnStarChest()
    {
        SoundController.Instance.PlayClickButton();
        starChest.gameObject.SetActive(true);
    }
    public void TurnOffStarChest()
    {
        SoundController.Instance.PlayClickButton();
        starChest.gameObject.SetActive(false);
    }
    private void UpdateProgress(object data)
    {
        int current = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Star);
        int max = GameController.Instance.GetConditionStarRewar();
        if(current > max )
        {
            txtProgress.text = "Full";
        }
        else
        {
            txtProgress.text = current.ToString() + " / " + max.ToString();
        }
        sliderStar.value = (float)current / (float)max;
    }
}
