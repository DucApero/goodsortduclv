using DVAH;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomAd
{
    public class CustomAdManager : MonoBehaviour
    {
        [HideInInspector] public bool turnOnInterAds = true;
        [HideInInspector] public bool turnOnRewardAds = true;

        Coroutine callbackFailCoroutine;
        Coroutine callbackNoneCoroutine;

        bool isShowAd;

        public static CustomAdManager Intance;

        private void Awake()
        {
            
            if (Intance == null)
            {
                Intance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void ShowInterAds(Action preCallback, Action callback)
        {
            if (isShowAd) return;
            bool ready = AdManager.Instant.InterstitialIsLoaded();
            preCallback?.Invoke();

            if (ready && turnOnInterAds)
            {
                Time.timeScale = 0f;
                isShowAd = true;

                AdManager.Instant.ShowInterstitial((status) =>
                {
                    if (status == InterVideoState.None || status == InterVideoState.Closed)
                    {
                        isShowAd = false;
                        Time.timeScale = 1f;
                        callback?.Invoke();
                    }
                }, true);
            }
            else
            {
                callback?.Invoke();
                Debug.Log("Reward Ads not ready");
            }
        }
        
        public void ShowRewardAds(Action preCallback, Action callbackWatched, Action callbackClosed)
        {
            if (isShowAd)
            {
                preCallback?.Invoke();
                return;
            }
            bool ready = AdManager.Instant.VideoRewardIsLoaded();
            if (ready && turnOnRewardAds)
            {
                preCallback?.Invoke();
                Time.timeScale = 0f;
                isShowAd = true;

                AdManager.Instant.ShowRewardVideo((status) =>
                {
                    if (status == RewardVideoState.Watched)
                    {
                        Time.timeScale = 1f;

                        isShowAd = false;
                        callbackWatched?.Invoke();
                        StartCoroutine(CheckFailAds());
                    }
                    else if (status == RewardVideoState.Closed)
                    {
                        Time.timeScale = 1f;

                        isShowAd = false;
                        callbackFailCoroutine = StartCoroutine(DelayCallbackFail(callbackClosed));
                        StartCoroutine(CheckNoneAds());
                    }
                    else if (status == RewardVideoState.None)
                    {
                        Time.timeScale = 1f;

                        isShowAd = false;
                        callbackNoneCoroutine = StartCoroutine(DelayCallbackNone(callbackClosed));
                    }
                }, true);
            }
            else
            {
                preCallback?.Invoke();

                if (turnOnRewardAds)
                {
                    callbackClosed?.Invoke();
                    Debug.Log("Reward Ads not ready");
                    //PopupManager.Instance.ShowNoAdsPopup();
                }
                else
                {
                    callbackWatched?.Invoke();
                }
            }
        }

        IEnumerator DelayCallbackFail(Action callback)
        {
            yield return null;
            yield return null;

            callback?.Invoke();
            callbackFailCoroutine = null;
        }

        IEnumerator DelayCallbackNone(Action callback)
        {
            yield return null;
            yield return null;
            yield return null;
            yield return null;

            callback?.Invoke();
            callbackNoneCoroutine = null;
        }

        IEnumerator CheckFailAds()
    {
        while (true)
        {
            if (callbackFailCoroutine != null)
            {
                StopCoroutine(callbackFailCoroutine);
                callbackFailCoroutine = null;
                break;
            }

            yield return null;
        }
    }

    IEnumerator CheckNoneAds()
    {
        while (true)
        {
            if (callbackNoneCoroutine != null)
            {
                StopCoroutine(callbackNoneCoroutine);
                callbackNoneCoroutine = null;
                break;
            }

            yield return null;
        }
    }
    }
}