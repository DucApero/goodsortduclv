using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SORewardStar", menuName = "GameConfiguration/SORewardStar")]
public class SORewardStar : ScriptableObject
{
    [System.Serializable]
    public class RewradStar
    {
        public List<ShopQuyNX.ItemData.Item> lstRewardBooster;
        public List<UserDatas.Currency> lstRewardCurrency;
        public int conditionStarUnlock;
    }
    public RewradStar[] Reward;
}
