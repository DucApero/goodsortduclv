﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;

public class LayerItem : Layer
{
    public void Start()
    {
        // Observer.Instance.AddObserver(ObserverKey.CheckMatch, CheckMatchInLayer);
    }

    public override void SpawnItem(bool isLayerFirst)
    {
        for (int i = 0; i < layerInfo.itemSkin.Length; i++)
        {
            if (layerInfo.itemSkin[i].id != 0)
            {
                ItemElement item = LevelManager.Instance.SpawnItem(layerInfo.itemSkin[i].id, listItemPositions[i]);
                listItemContainer[i] = item;
                item.CurrentLayer = this;
                currentBox.ItemContainer.Add(item);
                if (item)
                    item.CurrentBoxElement = currentBox;

                if (isLayerFirst)
                {
                    item.IsLayerFirst = true;
                    if (layerInfo.itemSkin[i].isSpecial)
                    {
                        item.SetUpHiddenItem();
                        item.IsSpecial = true;
                    }
                    Observer.Instance.Notify(ObserverKey.AddToListItemFisrt, layerInfo.itemSkin[i].id);
                }
                else
                {
                    isLayerFirst = false;
                    if (layerInfo.itemSkin[i].isSpecial)
                    {
                        item.IsSpecial = true;
                        item.SetUpHiddenItem();
                    }
                }
            }
            else listItemContainer[i] = null;
        }
    }

    public override void ReplaceItem(List<int> top3Item, int targetReplaceId)
    {
        for (int i = 0; i < listItemContainer.Count; i++)
        {
            foreach (int id in top3Item)
            {
                if (listItemContainer[i] && listItemContainer[i].ID == id)
                {
                    GamePlayUIEffect.Instance.SpawnMagicWandEffect(listItemContainer[i].transform.TransformPoint(new(0f, 0.5f, -1f)), listItemContainer[i].transform.rotation);
                    if (this.id == 0)
                        Observer.Instance.Notify(ObserverKey.DecreaseItemInListLayerFirst, listItemContainer[i].ID);
                    ItemElement item = LevelManager.Instance.SpawnItem(targetReplaceId, listItemPositions[i]);
                    if (this.id == 0)
                        Observer.Instance.Notify(ObserverKey.AddToListItemFisrt, item.ID);
                    Destroy(listItemContainer[i].gameObject);
                    listItemContainer[i] = item;
                    item.UpdateCurrentLayer(this);
                    item.CurrentLayer = this;
                    item.CurrentBoxElement = currentBox;
                    break;
                }
            }
        }
        SetItemElement();

        //StartCoroutine(ReplaceItemSystem(top3Item, targetReplaceId));
    }

    IEnumerator ReplaceItemSystem(List<int> top3Item, int targetReplaceId)
    {
        for (int i = 0; i < listItemContainer.Count; i++)
        {
            foreach (int id in top3Item)
            {
                if (listItemContainer[i] && listItemContainer[i].ID == id)
                {
                   GamePlayUIEffect.Instance.SpawnMagicWandEffect(listItemContainer[i].transform.TransformPoint(new(0f, 0.5f, -1f)), listItemContainer[i].transform.rotation);
                    yield return new WaitForSeconds(0.5f);

                    if (this.id == 0)
                        Observer.Instance.Notify(ObserverKey.DecreaseItemInListLayerFirst, listItemContainer[i].ID);
                    ItemElement item = LevelManager.Instance.SpawnItem(targetReplaceId, listItemPositions[i]);
                    if (this.id == 0)
                        Observer.Instance.Notify(ObserverKey.AddToListItemFisrt, item.ID);
                    Destroy(listItemContainer[i].gameObject);
                    listItemContainer[i] = item;
                    item.UpdateCurrentLayer(this);
                    item.CurrentLayer = this;
                    item.CurrentBoxElement = currentBox;
                    break;
                }
            }

            
        }
        SetItemElement();
        yield return null;
    }

    public override void AutoMatchItem(int targetID)
    {
        string ex = "";
        for (int i = 0; i < listItemContainer.Count; i++)
        {
            if (listItemContainer[i] && listItemContainer[i].ID == targetID)
            {
                if (this.id == 0)
                    Observer.Instance.Notify(ObserverKey.DecreaseItemInListLayerFirst, listItemContainer[i].ID);
                Observer.Instance.Notify(ObserverKey.UpdateListSortItem, listItemContainer[i].ID);
                listItemContainer[i].MatchTween(true);
                listItemContainer[i] = null;
            }
        }
        CheckStatusGame();
    }

    public override void AutoMatchThreeItem(List<int> targetID)
    {
        GamePlayUIEffect.Instance.SpawnBigHammerEffect(0.75f);
        string ex = "";
        for (int i = 0; i < listItemContainer.Count; i++)
        {
            foreach (int target_id in targetID)
            {
                if (listItemContainer[i] && listItemContainer[i].ID == target_id)
                {
                    if (this.id == 0)
                        Observer.Instance.Notify(ObserverKey.DecreaseItemInListLayerFirst, listItemContainer[i].ID);
                    Observer.Instance.Notify(ObserverKey.UpdateListSortItem, listItemContainer[i].ID);
                    listItemContainer[i].MatchTween();
                    listItemContainer[i] = null;
                }
            }

            //yield return new WaitForSeconds(0.25f);
        }
        CheckStatusGame();
        //StartCoroutine(AutoMatchThreeItemSystem(targetID));
    }

    IEnumerator AutoMatchThreeItemSystem(List<int> targetID)
    {
        GamePlayUIEffect.Instance.SpawnBigHammerEffect(0.75f);
        string ex = "";
        for (int i = 0; i < listItemContainer.Count; i++)
        {
            foreach (int target_id in targetID)
            {
                if (listItemContainer[i] && listItemContainer[i].ID == target_id)
                {
                    if (this.id == 0)
                        Observer.Instance.Notify(ObserverKey.DecreaseItemInListLayerFirst, listItemContainer[i].ID);
                    Observer.Instance.Notify(ObserverKey.UpdateListSortItem, listItemContainer[i].ID);
                    listItemContainer[i].MatchTween();
                    listItemContainer[i] = null;
                }
            }

            yield return new WaitForSeconds(0.25f);
        }
        CheckStatusGame();
    }

    protected void TurnOnLayer()
    {
        foreach (ItemElement item in listItemContainer)
        {
            item.SetMaterialOn();
        }
    }

    public void SetItemElement()
    {
        foreach (ItemElement item in listItemContainer)
        {
            if (id == 1 && item)
            {
                item.SetColliderOff();
                item.SetMaterialOff();
            }
        }
    }

    public override void TurnOnAllMaterial()
    {
        foreach (ItemElement item in listItemContainer)
        {
            item.SetMaterialOn();
        }
    }

    protected void TurnOffLayer()
    {
        foreach (ItemElement item in listItemContainer)
        {
            item.SetMaterialOff();
        }
    }

    public override bool CheckMatch()
    {
        int id = -1;
        foreach (ItemElement item in listItemContainer)
        {
            if (item != null)
            {
                if (id != -1)
                {
                    if (id != item.ID) return false;
                }
                else
                {
                    id = item.ID;
                }
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    public override bool CheckEmpty(int index)
    {
        if (listItemContainer[index] == null) return true;
        else return true;
    }

    public override void AddItem(ItemElement item)
    {
        int index = -1;
        float nearDistane = Mathf.Infinity;
        for (int i = 0; i < listItemPositions.Count; i++)
        {
            if (listItemContainer[i] == null)
            {
                float distance = Vector2.Distance(item.transform.position, listItemPositions[i].position);
                if (distance < nearDistane)
                {
                    nearDistane = distance;
                    index = i;
                }
            }
        }
        if (index >= 0 && index < listItemPositions.Count)
        {
            item.MoveToTarget(listItemPositions[index]);
            listItemContainer[index] = item;
            item.transform.parent = listItemPositions[index];
            item.CurrentLayer = this;
        }

        CheckMatchInLayer(null);
        LevelManager.Instance.CurrentLevel.CheckLoseLevel();
    }

    public void CheckMatchInLayer(object data)
    {
        if (CheckMatch())
        {
            MatchListItem();
            //Observer.Instance.Notify(ObserverKey.DecreaseNumberLock);
            currentBox.ChangeLayer();
        }
        else
        {
            if(listItemContainer.Count == 3)
            {
                
            }
        }
        CheckStatusGame();
    }

    public override void CheckStatusGame()
    {
        LevelManager.Instance.CurrentLevel.CheckWinLevel();
       
    }

    public override void DeleteItem(int index)
    {
        if (listItemContainer[index] != null)
        {
            listItemContainer[index] = null;
        }
    }

    public override bool IsLayerContainTwoItemHasSameID()
    {
        for (int i = 0; i < listItemContainer.Count; i++)
        {
            for (int j = i + 1; j < listItemContainer.Count; j++)
            {
                if (listItemContainer[i].ID == listItemContainer[j].ID)
                {
                    return true; // Có ít nhất hai phần tử có giá trị bằng nhau
                }
            }
        }
        return false; // Không có hai phần tử nào có giá trị bằng nhau
    }

    public override bool IsLayerContainEmpty()
    {
        foreach (ItemElement item in listItemContainer)
        {
            if (item == null) return true;
        }

        return false;
    }
    public override bool IsFirstItemInLayerContainEmpty()
    {
        if (listItemContainer[0]) return false;
        return true;
    }

    public override bool IsLayerEmpty()
    {
        foreach (ItemElement item in listItemContainer)
        {
            if (item != null) return false;
        }

        return true;
    }

    public override void RemoveItem(ItemElement item)
    {
        for (int i = 0; i < listItemContainer.Count; i++)
        {
            if (listItemContainer[i] != null && item.GetInstanceID() == listItemContainer[i].GetInstanceID())
            {
                listItemContainer[i] = null;
            }
        }


    }

    public override void MatchListItem()
    {
        //SoundController.Instance.PlayMatchSound(0.5f);
        for (int i = 0; i < listItemContainer.Count; i++)
        {
            Observer.Instance.Notify(ObserverKey.DecreaseItemInListLayerFirst, listItemContainer[i].ID);
            Observer.Instance.Notify(ObserverKey.UpdateListSortItem, listItemContainer[i].ID);
            listItemContainer[i].MatchTween();
            listItemContainer[i] = null;
        }
    }

    public override void ChangeLayer(List<ItemElement> items)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i])
            {
                items[i].transform.parent = listItemPositions[i].transform;
                items[i].transform.DOMove(listItemPositions[i].transform.position, .7f);
                items[i].transform.DOScale(items[i].transform.localScale, .7f).From(items[i].transform.localScale * 0.75f).SetEase(Ease.InOutBack);
                listItemContainer[i] = items[i];
                items[i] = null;
                listItemContainer[i].SetColliderOn();
                listItemContainer[i].gameObject.SetActive(true);
                listItemContainer[i].IsLayerFirst = true;
                listItemContainer[i].SetMaterialOn();
                listItemContainer[i].UpdateCurrentLayer(this);
                listItemContainer[i].CurrentLayer = this;

                Observer.Instance.Notify(ObserverKey.AddToListItemFisrt, listItemContainer[i].ID);
            }

        }
    }
}
