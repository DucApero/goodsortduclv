using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System;

public class UnLockItem : MonoBehaviour
{
    [SerializeField] Transform lightCricle;
    [SerializeField] Image item;
    [SerializeField] Button Claim;
    [SerializeField] GameObject conten;
    [SerializeField] float delay;
    Tween tween;
    [SerializeField] Transform point1;
    [SerializeField] Transform target;
    [SerializeField] List<RectTransform> _lstItem = new List<RectTransform>();
    public Action callback;
    private void Start()
    {
        RotateCricle_Light();
        Claim.onClick.AddListener(() =>
        {
            conten.SetActive(false);
            StartCoroutine(MoveItem());
        });
    }
    public void GameStart(Sprite itemNew,Transform target)
    {
        gameObject.SetActive(true);
        item.gameObject.SetActive(true);
        conten.SetActive(true);
        item.sprite = itemNew;
        this.target = target;
        AddListItem();

    }
    public void RotateCricle_Light()
    {
       tween =  lightCricle.DOLocalRotate(new Vector3(0, 0, 350), delay)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Incremental);
    }

    public void AddListItem()
    {
        for (int i = 0; i < 3; i++)
        {
            RectTransform items = Instantiate(item.rectTransform,transform);
            _lstItem.Add(items);
        }
    }
    public IEnumerator MoveItem()
    {
        int index = 0;
        while (index<_lstItem.Count)
        {
            _lstItem[index].DOScale(0.5f, 0.3f);
            _lstItem[index].DOMove(point1.position, 0.18f)
               .SetEase(Ease.InOutSine)
               .OnComplete(() =>
               {
                   _lstItem[index].DOMove(target.position, 0.18f);

               });
            yield return new WaitForSeconds(0.2f);
            index++;
            if (index >=_lstItem.Count)
            {
                item.gameObject.SetActive(false);
                yield return new WaitForSeconds(0.3f);
                for (int i = 0; i < _lstItem.Count; i++)
                    Destroy(_lstItem[i].gameObject);
                _lstItem.Clear();
                callback?.Invoke();
                gameObject.SetActive(false);
                yield break;
            }
        }
       
      
      
       
        
    }


}

