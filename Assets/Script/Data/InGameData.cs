using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameData
{
    public int currentMoney;
    public ShopItemData ShopItemData = new();
}

[System.Serializable]
public class ShopItemData
{
    public List<int> OwnedItemID;
    public List<WatchAds> WatchAdsCount; 
    public int ItemIsSelected;

    public ShopItemData()
    {
        OwnedItemID = new List<int>();
        WatchAdsCount = new List<WatchAds>();
       
        ItemIsSelected = -1;
    }
}
public class WatchAds
{
    public int ID;
    public int WatchCount;
}

