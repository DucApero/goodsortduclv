using CommonEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UserDatas : Singleton<UserDatas>
{
    private const string DataKey = "UserData";
    private bool isLoadDone = false;

    public bool IsLoadDone
    {
        get => isLoadDone;
    }

    [Serializable]
    private class UserData
    {
        public RewardDailyData rewardDailyData = new();
        public int timeOut = 0;
        public int timeRemaining = 0;
        public int lastLockHeart = 0;
        public int targetUnlockHeart = 0;
        public int CurrentLevel = 0;
        public int lastUseHeart = 0;
        public bool IsRemoveAds = false;
        public List<CurrentItemData> CurrentItemDatas = new List<CurrentItemData>();
        public List<RewardAdCountDown> RewardAdCountDowns = new List<RewardAdCountDown>();
        public List<Currency> Currency = new List<Currency>();
        public int Coin = 0;
        public int IndexGetRewardStar = 0;
        public bool NoAds = false;
        public bool HideLimitedPack = false;
        public bool ActiveMusic = true;
        public bool ActiveSFX = true;
        public ShopData ShopItemData = new();

        public UserInfo userInfo = new();
    }
    [System.Serializable]
    public class Currency
    {
        public CommonEnum.Currency Id;
        public int Value;
        public Currency(CommonEnum.Currency id, int value)
        {
            this.Id = id;
            this.Value = value;
        }
    }
    [System.Serializable]
    public class UserInfo
    {
        public int IDAvatar;
        public string Name;

        public UserInfo()
        {
            IDAvatar = 0;
            Name = "Player";
        }
    }

    [System.Serializable]
    public class ShopData
    {
        public List<int> OwnedItemID;
        public List<WatchAds> WatchAdsCount;
        public int ItemIsSelected;

        public ShopData()
        {
            OwnedItemID = new List<int>();
            OwnedItemID.Add(0);
            WatchAdsCount = new List<WatchAds>();

            ItemIsSelected = 0;
        }
    }

    [System.Serializable]
    public class WatchAds
    {
        public int ID;
        public int WatchCount;

        public WatchAds(int id, int watchCount)
        {
            ID = id;
            WatchCount = watchCount;
        }
    }


    [SerializeField] private UserData dataLocal;
    private void Awake()
    {
        Initialize();
    }

    public List<int> GetListRewardSelected()
    {
        return dataLocal.rewardDailyData.selectedReward;
    }

    public void AddSelectedRewardDay(int index)
    {
        dataLocal.rewardDailyData.selectedReward.Add(index);
        SaveLocal();
    }

    public int GetLastDayPlayGame()
    {
        return dataLocal.rewardDailyData.lastDayPlay;
    }

    public void SetLastDayPlayGame()
    {
        dataLocal.rewardDailyData.lastDayPlay = GameController.Instance.GetCurrentDay();
        SaveLocal();
    }

    public int GetFirstDayPlayGame()
    {
        return dataLocal.rewardDailyData.firstDayPlay;
    }

    public void SetFirstDayPlayGame(int day)
    {
        dataLocal.rewardDailyData.firstDayPlay = day;
        SaveLocal();
    }

    public int GetLastLockHeartDate()
    {
        return dataLocal.lastLockHeart;
    }

    public int GetTimeOut()
    {
        return dataLocal.timeOut;
    }

    public void SetTimeOut(int value)
    {
        dataLocal.timeOut = value;
        SaveLocal();
    }

    public int GetTimeRemaining()
    {
        return dataLocal.timeRemaining;
    }

    public void SetInfinityTimeRemaining(int value)
    {
        dataLocal.timeRemaining = value;
        SaveLocal();
    }

    public int GetLastUseHeart()
    {
        if(dataLocal.lastUseHeart == 0)
        {
            dataLocal.lastUseHeart = GameController.Instance.GetCurrentTimeSecond();
        }
        return dataLocal.lastUseHeart;
    }
    public void SetLastUseHeart(int value)
    {
        dataLocal.lastUseHeart = value;
        SaveLocal();
    }
    public void SetLastLockHeartDate(int value)
    {
        dataLocal.lastLockHeart = value;
        SaveLocal();
    }

    public void Initialize()
    {
        try
        {
            LoadLocal();
        }
        catch (Exception e)
        {
            Debug.LogError("Init userdata error: " + e.ToString());
        }

    }
    public int CurrentLevel
    {
        get
        {
            if (dataLocal == null)
            {
                return 0;
            }
            return dataLocal.CurrentLevel;
        }
        set
        {
            if (dataLocal == null)
                return;
            dataLocal.CurrentLevel = value;
            SaveLocal();
        }
    }
    public int IndexGetRewardStar
    {
        get
        {
            if (dataLocal == null)
            {
                return 0;
            }
            return dataLocal.IndexGetRewardStar;
        }
        set
        {
            if (dataLocal == null)
                return;
            dataLocal.IndexGetRewardStar = value;
            SaveLocal();
        }
    }
    public bool IsRemoveAds
    {
        get
        {
            if (dataLocal == null)
            {
                return false;
            }
            return dataLocal.IsRemoveAds;
        }
        set
        {
            if (dataLocal == null)
                dataLocal = new UserData();
            dataLocal.IsRemoveAds = value;
            SaveLocal();
        }
    }

    public bool IsHideLimitedBox
    {
        get
        {
            if (dataLocal == null)
            {
                return false;
            }
            return dataLocal.HideLimitedPack;
        }
        set
        {
            if (dataLocal == null)
                dataLocal = new UserData();
            dataLocal.HideLimitedPack = value;
            SaveLocal();
        }
    }

    #region Records

    public UserInfo GetUserData() => dataLocal.userInfo;

    public void SetAvartarID(int id)
    {
        dataLocal.userInfo.IDAvatar = id;
        SaveLocal();
    }

    public int GetAvatarID()
    {
        return dataLocal.userInfo.IDAvatar;
    }

    public void SetNameUser(string name)
    {
        dataLocal.userInfo.Name = name;
        SaveLocal();
    }

    public void AddItem(ItemGameplay id, int amount)
    {
        int index = GetIndexItemInList(id);
        
        if (index != -1)
        {
            CurrentItemData currentItemData = dataLocal.CurrentItemDatas[index];
            currentItemData.amount += amount;

            dataLocal.CurrentItemDatas[index] = currentItemData;
        }
        else
        {
            CurrentItemData currentItemData = new()
            {
                id = id,
                amount = amount
            };
            
            dataLocal.CurrentItemDatas.Add(currentItemData);
            //AddItem(id, amount);
        }
        Observer.Instance.Notify(ObserverKey.UpdateCountBooster, id);
        SaveLocal();
    }

    int GetIndexItemInList(ItemGameplay id)
    {
        for (int i = 0; i < dataLocal.CurrentItemDatas.Count; i++)
        {
            if (dataLocal.CurrentItemDatas[i].id == id) return i;
        }

        return -1;
    }

    public int GetCurrentItemData(ItemGameplay id)
    {
        CurrentItemData currentItemData = dataLocal.CurrentItemDatas.Where(item => item.id == id).FirstOrDefault();
        if (currentItemData == null)
        {
            AddItem(id, 0);
            return GetCurrentItemData(id);
        }

        return dataLocal.CurrentItemDatas.Where(item => item.id == id).FirstOrDefault().amount;
    }
    public int GetCurrency(CommonEnum.Currency currency)
    {
        if (dataLocal == null)
        {
            Debug.Log(">>>>>>>>>> Error : Null data local <<<<<<<<<<<");
            return 0;
        }
        for (int i = 0; i < dataLocal.Currency.Count; i++)
        {
            if (dataLocal.Currency[i].Id == currency)
            {
                return dataLocal.Currency[i].Value;
            }
        }
        Currency newCurrency = new Currency(currency, 0);
        dataLocal.Currency.Add(newCurrency);
        return 0;
    }
    public void SetCurrency(CommonEnum.Currency currency, int value)
    {
        if (dataLocal == null)
        {
            Debug.Log(">>>>>>>>>> Error : Null data local <<<<<<<<<<<");
            return;
        }
        string key = "Update" + currency.ToString();
        for (int i = 0; i < dataLocal.Currency.Count; i++)
        {
            if (dataLocal.Currency[i].Id == currency)
            {
                dataLocal.Currency[i].Value += value;
                Observer.Instance.Notify(key, dataLocal.Currency[i].Value);
                SaveLocal();
                return;
            }
        }
        Currency newCurrency = new Currency(currency, value);
        dataLocal.Currency.Add(newCurrency);
        Observer.Instance.Notify(key,value);
        SaveLocal();
    }

    public void AddCoin(int amount)
    {

        SetCurrency(CommonEnum.Currency.Gold, amount);
        SaveLocal();
    }

    public int GetCoin()
    {
        return dataLocal.Coin;
    }

    public bool IsNoAds()
    {
        return dataLocal.NoAds;
    }

    public void ActiveNoAds()
    {
        dataLocal.NoAds = true;
        SaveLocal();
    }

    public void ChangeMusicSetting()
    {
        dataLocal.ActiveMusic = !dataLocal.ActiveMusic;
        SaveLocal();
    }

    public bool IsMusicActive()
    {
        return dataLocal.ActiveMusic;
    }

    public void ChangeSFXSetting()
    {
        dataLocal.ActiveSFX = !dataLocal.ActiveSFX;
        SaveLocal();
    }

    public bool IsSFXActive()
    {
        return dataLocal.ActiveSFX;
    }
    public void AddItemOwned(int idItem)
    {
        dataLocal.ShopItemData.OwnedItemID.Add(idItem);
        SaveLocal();
    }

    public void AddWatchAdsCount(int idItem)
    {

        foreach (WatchAds watchAds in dataLocal.ShopItemData.WatchAdsCount)
        {
            if (watchAds.ID == idItem)
            {
                watchAds.WatchCount++;
                SaveLocal();
                return;
            }
        }
        dataLocal.ShopItemData.WatchAdsCount.Add(new WatchAds(idItem, 1));
        Debug.Log("count " + dataLocal.ShopItemData.WatchAdsCount.Count);
        SaveLocal();
    }

    private void SetDefaultWatchAdsData()
    {
        /*Debug.Log("itemskin count: " + ShopUI.Instance.RecordSkin.itemSkin);
        for (int i = 0; i < ShopUI.Instance.RecordSkin.itemSkin.Length; i++)
        {
            dataLocal.ShopItemData.WatchAdsCount.Add(new());
            dataLocal.ShopItemData.WatchAdsCount[i].WatchCount = 0;
            dataLocal.ShopItemData.WatchAdsCount[i].ID = i + 1;
        }*/
    }

    public int GetWatchAdsCount(int idItem)
    {
        if (dataLocal.ShopItemData.WatchAdsCount.Count == 0) return 0;
        int value = 0;
        foreach (WatchAds watchAds in dataLocal.ShopItemData.WatchAdsCount)
        {
            if (watchAds.ID == idItem)
            {
                value = watchAds.WatchCount;
            }
        }
        return value;
    }

    public int GetItemIsSelected()
    {
        return dataLocal.ShopItemData.ItemIsSelected;
    }

    public void SetItemIsSelected(int idItem)
    {
        dataLocal.ShopItemData.ItemIsSelected = idItem;
        SaveLocal();
    }

    public List<int> GetOwnedItem()
    {
        if (dataLocal.ShopItemData != null)
            return dataLocal.ShopItemData.OwnedItemID;
        else
        {
            Debug.Log(dataLocal.ShopItemData);
            dataLocal.ShopItemData.OwnedItemID = new List<int>();
            dataLocal.ShopItemData.OwnedItemID.Add(0);
            Debug.Log("Owned"+dataLocal.ShopItemData.OwnedItemID.Count);
            return dataLocal.ShopItemData.OwnedItemID;
        }
    }

    public string GetRewardAdsCountDownById(string id)
    {
        RewardAdCountDown rewardAdCountDown = dataLocal.RewardAdCountDowns.Where(item => item.id == id).FirstOrDefault();
        if (rewardAdCountDown != null)
        {
            string unlockTime = dataLocal.RewardAdCountDowns.Where(item => item.id == id).FirstOrDefault().unlockTime;
            return unlockTime;
        }

        return null;
    }

    public void AddRewardAdsCountDown(string id, float minute)
    {
        DateTime currentTime = DateTime.Now;
        DateTime nextTime = currentTime.AddMinutes(minute);

        for (int i = 0; i < dataLocal.RewardAdCountDowns.Count; i++)
        {
            if (id == dataLocal.RewardAdCountDowns[i].id)
            {
                dataLocal.RewardAdCountDowns[i].unlockTime = nextTime.ToString();
                SaveLocal();
                return;
            }
        }

        RewardAdCountDown rewardAdCountDown = new()
        {
            id = id,
            unlockTime = nextTime.ToString()
        };
        dataLocal.RewardAdCountDowns.Add(rewardAdCountDown);

        SaveLocal();
    }

    #endregion

    #region Process File

    private void LoadLocal()
    {
        try
        {
            if (PlayerPrefs.HasKey(DataKey))
            {
                var jsonString = PlayerPrefs.GetString(DataKey);
                dataLocal = JsonUtility.FromJson<UserData>(jsonString);
              
                Debug.Log("Data Load : " + jsonString);
            }
            else
            {
                dataLocal = new UserData();
                Currency heart = new(CommonEnum.Currency.Heart, 5);
                dataLocal.Currency.Add(heart);
                Debug.Log("Data Load None : " + dataLocal);
                RequestSave();
            }
        }
        catch (Exception e)
        {
            Debug.LogError("=================> Loadlocal error: " + e.ToString());
        }
        isLoadDone = true;
    }

    private void RequestSave()
    {
        Debug.Log("RequestSave");
        SaveLocal();
    }

    private void SaveLocal()
    {
        try
        {
            var jsonString = JsonUtility.ToJson(dataLocal);
            PlayerPrefs.SetString(DataKey, jsonString);
            Debug.Log("data save : " + jsonString);
            PlayerPrefs.Save();

        }
        catch (Exception e)
        {
            Debug.LogError("======> Save file fail, error: " + e.ToString());
        }
    }

    #endregion
}
