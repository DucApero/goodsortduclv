using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class ItemElement : Item
{
    [SerializeField]
    private MeshRenderer[] listMesh;
    [SerializeField]
    private Material[] materialOn;
    [SerializeField]
    private Material[] materialOff;
    private bool isSpecial;
    public bool IsSpecial
    {
        get => isSpecial;
        set => isSpecial = value;
    }
    [SerializeField] GameObject hiddenSprite;
    [SerializeField] Material hiddenMaterialOff;
    [SerializeField] Material hiddenMaterialOn;
    [SerializeField]
    private ItemMovement itemMovement;
    [SerializeField] private GameObject matchVfx;
    [SerializeField] private Transform matchVfxPos;
    [SerializeField] private GameObject model;
    [SerializeField] private Layer currentLayer;
    public Layer CurrentLayer
    {
        get => currentLayer;
        set => currentLayer = value;
    }

    public BoxElement currentBoxElement;
    public BoxElement CurrentBoxElement
    {
        get
        {
            return currentBoxElement;
        }
        set
        {
            currentBoxElement = value;
        }

    }

    private bool isLayerFirst;
    public bool IsLayerFirst
    {
        get => isLayerFirst;
        set => isLayerFirst = value;
    }

    private void OnValidate()
    {
        GetMaterial();
    }

    private void GetMaterial()
    {
        listMesh = GetComponentsInChildren<MeshRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        itemMovement.CurrentLayer = currentLayer;
        itemMovement.CallBackAddItem = (box) =>
        {
            box.AddItem(this);
        };
    }

    // Update is called once per frame
    public void UpdateCurrentLayer(Layer layer)
    {
        currentLayer = layer;
        itemMovement.CurrentLayer = layer;
    }

    public void SetUpHiddenItem()
    {
        if (isLayerFirst)
        {
            for (int i = 0; i < listMesh.Length; i++)
            {
                listMesh[i].material = hiddenMaterialOn;
            }
        }
        else
        {
            for (int i = 0; i < listMesh.Length; i++)
            {
                listMesh[i].material = hiddenMaterialOff;
            }
        }
        hiddenSprite.SetActive(true);
    }
        
    public void SetMaterialOff()
    {
        for (int i = 0; i < listMesh.Length; i++)
        {
            listMesh[i].material = materialOff[i];
        }
        //this.GetComponent<MeshRenderer>().material.color = new Color32(35, 35, 35, 255);
    }
    public void SetMaterialOn()
    {
        for (int i = 0; i < listMesh.Length; i++)
        {
            listMesh[i].material = materialOn[i];
        }
        if(isSpecial)
        SetUpHiddenItem();
        //this.GetComponent<MeshRenderer>().material.color = new Color32(255, 255, 255, 255);
    }

    public void MoveToTarget(Transform target)
    {
        itemMovement.MoveToTarget(target);

    }

    public void MoveToPrePos()
    {
        itemMovement.MoveToPrePos();
    }

    public void UpdateCurrentBox(BoxElement box)
    {
        currentBoxElement.RemoveItem(this);
        if (currentBoxElement != box)
            currentBoxElement.UpdateLayer();
        currentBoxElement = box;
    }


    public void MatchTween(bool toCenter = false)
    {
        Observer.Instance.Notify(ObserverKey.UpdateItemSpecial,ID);
        if (toCenter)
        {
            SetMaterialOn();
            model.transform.DOMove(GamePlayUIEffect.Instance.Center.position, 0.5f).OnComplete(() =>
            {
                model.transform.DOScale(0f, .5f).SetEase(Ease.InBack).OnComplete(() =>
                {
                    Instantiate(matchVfx, GamePlayUIEffect.Instance.Center.position, Quaternion.identity, transform.parent);
                    Destroy(this.gameObject);
                    LevelManager.Instance.CheckMatchItem(ID, ()=> 
                    {
                        for (int i = 0; i < GameController.Instance.GetStarEarn(); i++)
                        {
                            GamePlayUIEffect.Instance.SpawnStarEffect(GamePlayUIEffect.Instance.Center, 200f);
                        }
                        

                    });
                    
                });
            });
        }
        else
        {
            model.transform.DOScale(0f, .5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                Instantiate(matchVfx, matchVfxPos.position, Quaternion.identity, transform.parent);
                Destroy(this.gameObject);
                LevelManager.Instance.CheckMatchItem(ID, () =>
                {
                    for (int i =0; i<GameController.Instance.GetStarEarn();i++)
                    {
                        GamePlayUIEffect.Instance.SpawnStarEffect(currentBoxElement.transform, 0.5f);
                    }
                    

                });
                
                TQT.TutorialControler.Instance.Tutorial_Level.NextStep();

            });

        }
    }

    public void SetColliderOff()
    {
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
    }
    public void SetColliderOn()
    {
        this.gameObject.GetComponent<BoxCollider>().enabled = true;
    }
}
