using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;


public class ItemBooster_GamePlay : MonoBehaviour
{
    [SerializeField] Sprite spriteLock, spriteBooster;
    [SerializeField] Image icon;
    public GameObject addIcon,countItem;
    private ShopQuyNX.ItemData.Item currentItem;
    public ShopQuyNX.ItemData.Item CurrentItem => currentItem;
    public bool isLock;

    public void SetInfo(ShopQuyNX.ItemData.Item item)
    {
        currentItem = item;
        icon.sprite = currentItem.icon;
        SetItemStateUI();
    }
    public void SetItemStateUI()
    {
        if (currentItem == null) return;
        if (UserDatas.Instance.CurrentLevel + 1 >= currentItem.requiredLevel)
        {
            icon.sprite = spriteBooster;
            isLock = false;
        }
        else
        {
            icon.sprite = spriteLock;
            isLock = true;
        }
    }
}
