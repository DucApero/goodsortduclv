using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : Singleton<BackgroundController>
{
    [SerializeField] private SkinContainer backgroundContainer;

    [SerializeField] private MeshRenderer backgroundUp, backgroundDown;

    // Start is called before the first frame update
    void OnEnable()
    {
        UpdateBackground();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateBackground()
    {
        int index = UserDatas.Instance.GetItemIsSelected();
        backgroundUp.material = backgroundContainer.listBackground[index].backgroundMaterial;
        backgroundDown.material = backgroundContainer.listBackground[index].backgroundMaterial;
    }
}
