using CommonEnum;
using MyBox;
using ShopQuyNX;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static ShopQuyNX.ShopItemData;

public class SmallItemController : IShopItem
{
    [SerializeField] ItemData _data;
    [SerializeField] Image itemIcon, watchAdIcon;
    [SerializeField] TMP_Text price, amount, countDown;
    [SerializeField] Button buyButton;

    [HideInInspector] public bool isGold;
    [HideInInspector] public ItemShop itemInformation;
    [HideInInspector] public int amountCoin;

    private void Start()
    {
        if (isGold) amount.text = "x" + amountCoin;
        else
        {
            ItemData.Item itemDataShow = GetDataById();
            amount.text = itemInformation.amountText;

            itemIcon.sprite = itemDataShow.icon;
        }

        if (isIAP)
        {
            price.gameObject.SetActive(true);
            watchAdIcon.gameObject.SetActive(false);

            price.text = priceIAP + " $";
        }
        else
        {
            if (IsReadyToBuy()) return;
            buyButton.interactable = false;
        }
    }

    ItemData.Item GetDataById()
    {
        ItemGameplay id = itemInformation.id;
        ItemData.Item itemData = _data.items.Where(item => id == item.itemId).FirstOrDefault();
        return itemData;
    }

    public override void OnClickBuy()
    {
        if (!isIAP && !isReadyToBuy) return;
        SoundController.Instance.PlayClickButton();
        if (isGold) UserDatas.Instance.AddCoin(amountCoin);
        else UserDatas.Instance.AddItem(itemInformation.id, itemInformation.amount);
        ShowPopupReward();

        if (isIAP) return;
        UserDatas.Instance.AddRewardAdsCountDown(idRewardAds, totalMinuteToUnlock);
        isReadyToBuy = false;

    }

    void ShowPopupReward()
    {
        if (isGold)
        {
            ShopGenerateUIController.Instance.ShowRewardAnItemPopup(null, amountCoin);
        }
        else
        {
            ItemData.Item item = ShopGenerateUIController.Instance.ItemData.items.Where(itemData => itemData.itemId == itemInformation.id).First();
            ShopGenerateUIController.Instance.ShowRewardAnItemPopup(item.icon, itemInformation.amount, false);
        }
        
    }

    bool IsReadyToBuy()
    {
        DateTime currentTime = DateTime.Now;

        string nextTimeText = UserDatas.Instance.GetRewardAdsCountDownById(idRewardAds);
        if (nextTimeText != null)
        {
            DateTime nextTime = DateTime.Parse(nextTimeText);
            TimeSpan timeDifference = nextTime - currentTime;
            double totalSeconds = timeDifference.TotalSeconds;

            return totalSeconds <= 0;
        }

        return true;
    }

    private void Update()
    {
        if (isIAP || isReadyToBuy) return;


        if (IsReadyToBuy())
        {
            buyButton.interactable = true;
            price.gameObject.SetActive(false);
            countDown.gameObject.SetActive(false);
            watchAdIcon.gameObject.SetActive(true);
            isReadyToBuy = true;
        }
        else
        {
            buyButton.interactable = false;
            price.gameObject.SetActive(false);
            watchAdIcon.gameObject.SetActive(false);
            countDown.gameObject.SetActive(true);

            DateTime currentTime = DateTime.Now;
            string nextTimeText = UserDatas.Instance.GetRewardAdsCountDownById(idRewardAds);
            if (DateTime.TryParse(nextTimeText, out var nextTime))
            {
                TimeSpan timeDifference = nextTime - currentTime;
                float totalSeconds = (float)timeDifference.TotalSeconds;

                int second = Mathf.FloorToInt(totalSeconds % 60f);
                int minute = Mathf.FloorToInt(totalSeconds / 60f);
                int hour = Mathf.FloorToInt(minute / 60f);

                string hourText = hour > 0f ? hour + "h" : "";
                string minuteText = minute > 0f ? minute + "m" : "";
                string secondText = second + "s";

                countDown.text = hourText + " " + minuteText + " " + secondText;
            }
        }
    }
}
