using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TQT
{
    public class Tutorial_BosterHome : MonoBehaviour
    {
        public ItemBooster skillA, skillB, skillC;
        public void Start()
        {
            GameStart();
        }
        public void GameStart()
        {
            skillA.Init();
            skillB.Init();
            skillC.Init();
            HideSkill();
        }
        public void GameEnd()
        {
            HideSkill();
            skillA.GameEnd();
            skillB.GameEnd();
            skillC.GameEnd();
        }
        public void Tutorial_skillA()
        {
            if (!skillA.IsTutorial)
                return;
            HideSkill();
            skillA.GameStart();
        }


        public void Tutorial_skillB()
        {
            if (!skillB.IsTutorial)
                return;
            HideSkill();
            skillB.GameStart();

        }
        public void Tutorial_skillC()
        {
            if (!skillC.IsTutorial)
                return;
            HideSkill();
            skillC.GameStart();

        }
   
        public void HideSkill()
        {
            skillA.gameObject.SetActive(false);
            skillB.gameObject.SetActive(false);
            skillC.gameObject.SetActive(false);
        }
    }
}

