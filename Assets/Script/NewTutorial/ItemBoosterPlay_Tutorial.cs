using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace TQT
{
    public class ItemBoosterPlay_Tutorial : MonoBehaviour
    {
        public const string Key_Bosster = "Booster";
        public int siblingIndexCurr;
        public string Name;
        public int levelUnlock;
        public Vector3 PosNew, PosCurr;
        public UnityEvent onClick;
        Tutorial_PopupPlay tutorial_Popupp;
        bool isTutorial;
        public bool IsTutorial
        {
            get { return isTutorial; }
            set
            {
                isTutorial = value;
                PlayerPrefs.SetInt(Key_Bosster + name, isTutorial ? 1 : 0);
            }
        }
        [SerializeField] ItemBoosterPlayGame itemBooster;
        public void Init(Tutorial_PopupPlay tutorial_PopupPlay)
        {
            isTutorial = PlayerPrefs.GetInt(Key_Bosster + name, 1) != 0;
            this.tutorial_Popupp = tutorial_PopupPlay;
            siblingIndexCurr = transform.GetSiblingIndex();
            itemBooster = GetComponent<ItemBoosterPlayGame>();
            levelUnlock = itemBooster.CurrentItem.requiredLevel;

        }
        public void StartTutorial(int index)
        {

            if (!IsTutorial)
                return;
            UserDatas.Instance.AddItem(itemBooster.CurrentItem.itemId, 3);
            itemBooster.ClickTutorial();
            transform.SetSiblingIndex(index);
        }
        public void EndTutorial()
        {
            IsTutorial = false;
            transform.SetSiblingIndex(siblingIndexCurr);
            GameController.Instance.LstBoosterInLevel.Add(itemBooster.CurrentItem.itemId);
            //onClick?.Invoke();
        }

    }
}

