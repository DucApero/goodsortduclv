using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HeartBar : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI heartText;
    [SerializeField] private TextMeshProUGUI countDownHeart;
    [SerializeField] private int lockTime;
    [SerializeField] private Button addIcon;
    [SerializeField] private Image heartIcon;
    [SerializeField] private Sprite normalHeart;
    [SerializeField] private Sprite infinityHeart;
    private bool countDown;
    public bool CountDown
    {
        get => countDown;
        set => countDown = value;
    }

    private bool infinityHeartCountDown;
    public bool InfinityHeartCountDown
    {
        get => infinityHeartCountDown;
        set => infinityHeartCountDown = value;
    }
    private bool isSetDefault;
    public bool IsSetDefault
    {
        get 
        {
            Debug.Log("isSetDefault "+ isSetDefault);
            return isSetDefault;
        }
        set
        {
            isSetDefault = value;
            Debug.Log("isSetDefault " + isSetDefault);
        }
    }
    // Start is called before the first frame update

    private void OnEnable() 
    {
        if(UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) <= 0)
        {
            countDown = true;
        }
        else
        {
            heartText.text = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart).ToString();
        }
    }

    private void Awake()
    {
        //lockTime = GameController.Instance.TimeHeart;
    }

    public void Start()
    {
        if((UserDatas.Instance.GetLastLockHeartDate() + lockTime) - GameController.Instance.GetCurrentTimeSecond() < 0)
        {
            GetHeart();
        }
        else
        {
            countDown = true;
        }
        Observer.Instance.AddObserver(ObserverKey.UpdateHeart, UpdateHeartBar);
        Observer.Instance.AddObserver(ObserverKey.CountDownHeart, SetCountDown);
    }

    public void OnDestroy()
    {
        Observer.Instance.RemoveObserver(ObserverKey.CountDownHeart, SetCountDown);
        Observer.Instance.RemoveObserver(ObserverKey.CountDownHeart, SetCountDown);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.Instance.TimeInfinityRemaining > 0)
        {
            UpdateInfinityCountDown();
        }
        else
        {
            int interval = (UserDatas.Instance.GetLastUseHeart() + lockTime) - GameController.Instance.GetCurrentTimeSecond();
            if (countDown)
            {
                UpdateCountDown();
            }
            else
            {
                heartIcon.sprite = normalHeart;
                heartText.gameObject.SetActive(true);
                heartText.text = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart).ToString();
                if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) >= 5) countDownHeart.text = "Full";
                
            }
            heartText.text = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart).ToString();
        }

    }

    public void SetCountDown(object data)
    {
        if (countDown == (bool)data) return;
        countDown = (bool)data;
    }

    private void UpdateCountDown()
    {
        heartIcon.sprite = normalHeart;
        heartText.gameObject.SetActive(true);
        heartText.text = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart).ToString();
        int interval = (UserDatas.Instance.GetLastLockHeartDate() + lockTime) - GameController.Instance.GetCurrentTimeSecond() ;
        int remainingHours = (int)(interval / 3600);
        int remainingMinutes = (int)((interval % 3600) / 60);
        int remainingSeconds = (int)(interval % 60);
        if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) < 5 && interval < lockTime && interval > 0)
        {
            countDownHeart.text = string.Format("{0:D2}:{1:D2}:{2:D2}", remainingHours, remainingMinutes, remainingSeconds);

        }
        else if(UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) >= 5)
        {
            countDownHeart.text = "Full";
        }

        if (interval <= 0 && (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) <= 0 || UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart) != 5))
        {
            GetHeart();
            countDownHeart.text = "Full";
        }
    }

    private void UpdateInfinityCountDown()
    {
        heartIcon.sprite = infinityHeart;
        heartText.gameObject.SetActive(false);
        int timeRemaining = GameController.Instance.targetTime - GameController.Instance.GetCurrentTimeSecond();
        int remainingHours = (int)(timeRemaining / 3600);
        int remainingMinutes = (int)((timeRemaining % 3600) / 60);
        int remainingSeconds = (int)(timeRemaining % 60);
        countDownHeart.text = string.Format("{0:D2}:{1:D2}:{2:D2}", remainingHours, remainingMinutes, remainingSeconds);
        GameController.Instance.TimeInfinityRemaining = (int)timeRemaining;
        if(timeRemaining <= 0)
        {
            UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Heart, -UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.Heart));
        }
    }

    public void GetHeart()
    {
        UserDatas.Instance.SetCurrency(CommonEnum.Currency.Heart, +5 - UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart));
        heartText.text = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart).ToString();
        countDown = false;
        IsSetDefault = true;
        countDownHeart.text = "Full";
    }

    public void UpdateHeartBar(object data)
    {
        heartText.text = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Heart).ToString();
    }
}
