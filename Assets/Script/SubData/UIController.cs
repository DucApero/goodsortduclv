﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIController : Singleton<UIController>
{
    [SerializeField] private HomeUI HomeUICanvas;
    [SerializeField] private GamePlayUI GamePlayUICanvas;
    [SerializeField] private NoAds NoAds;
    [SerializeField] private PopupBoosterPlay popupBoosterPlay;
    [SerializeField] private PopupReward popupReward;
    [SerializeField] private BackgroundControl backgroundControl;
    [SerializeField] private BackgroundController background;
    [SerializeField] private GameObject notEnoughCoin;
    [SerializeField] private GameObject enoughHeart;
    public GamePlayUI GamePlayUI => GamePlayUICanvas;
    public HomeUI HomeUI => HomeUICanvas;


    void Start()
    {
        MoveToHome();
        
    }
    public void ActionBuyNoAds()
    {
        HomeUICanvas.BuyRemoveAds();
        GamePlayUICanvas.BuyNoAds();
    }
    public void NotEnoughCoin()
    {
        notEnoughCoin.SetActive(true);
    }
    public void EnoughHeart()
    {
        enoughHeart.SetActive(true);
    }
    public void UpdateHubCombo()
    {
        GamePlayUICanvas.UpdateValueProgressBar();
    }
    public void DecreaseHeartOnClickPlay()
    {
        HomeUICanvas.DecreaseHeart();
    }

    public void MoveToGamePlay()
    {
        ShowGamePlayUI();
        HideHomeUI();
        GameController.Instance.MoveToGamePlay();
        background.gameObject.SetActive(true);
        backgroundControl.MoveToGamePlay();
        
    }
    public void MoveToHome()
    {
        ShowHomeUI();
        HideGamePlayUI();
        GameController.Instance.MoveToHome();
        GamePlayUICanvas.ActivePopupSettings(false);
        if (SoundController.Instance != null)
        {
            SoundController.Instance.PlayBackgroundHome();
        }
        backgroundControl.MoveToLobby();
    }
    public void ShowHomeUI()
    {
        HomeUICanvas.ActivePopup(true);
    }
    public void ClickBtnShop()
    {
        HomeUICanvas.ClickBtnShop();
    }
    public void ClickBtnHome()
    {
        HomeUICanvas.ClickBtnHome();
    }
    public void UpdateStarGamePlay(int value)
    {
        GamePlayUICanvas.UpdateStar(value);
    }
    public void ResetStarHub()
    {
        GamePlayUICanvas.ResetStarHub();
    }
    public void ShowPopupReward(List<ShopQuyNX.ItemData.Item> items = null, List<UserDatas.Currency> currencies = null , Action action = null)
    {
        popupReward.ShowAndSetInfo(items, currencies,action);
        //popupBoosterPlay.tutorial_PopupPlay.Show();
    }
    public void WinLevel()
    {
        GamePlayUICanvas.ActivePopupWin(true);
        GamePlayUIEffect.Instance.HideAllEffect();
        GameController.Instance.ActiveBaner(false);
    }
    public void LoseLevel()
    {
        GameController.Instance.ResetCombo();
        GameController.Instance.LoseShowInter(() => 
        {
            GamePlayUICanvas.ActivePopuplose(true);
            GamePlayUIEffect.Instance.HideAllEffect();
            GameController.Instance.ActiveBaner(false);
        });
    }

    public void ActiveEffectWin()
    {
        GamePlayUICanvas.ActiveEffecWin();
    }
    public void HideHomeUI()
    {
        HomeUICanvas.ActivePopup(false);
    }

    public void ShowGamePlayUI()
    {
        GamePlayUICanvas.gameObject.SetActive(true);
    }
    public void HideGamePlayUI()
    {
        GamePlayUICanvas.gameObject.SetActive(false);
    }
    public void UpdateLevelText()
    {
        GamePlayUICanvas.UpdateLevelText();
    }

    public void UpdateTextLevel()
    {
        HomeUICanvas.UpdateTextLevel();
    }
    public void ShowpoupRewardPlay(UnityAction action, bool isPlayWin)
    {
        popupBoosterPlay.ActivePopup(true,action,isPlayWin);
    }
    public void TurnOnSettingHome()
    { 
        HomeUICanvas.TurnOnSetting();
    }
    public void TurnOffSettingHome()
    { 
        HomeUICanvas.TurnOffSetting();
    }
    public void ActivePopupAds(bool isActive)
    {
        NoAds.ActivePopup(isActive);
    }
    public void TurnOnStarChest() { 
        HomeUICanvas.TurnOnStarChest();
    }
    public void TurnOffStarChest() { 
        HomeUICanvas.TurnOffStarChest();
    }

    public void TurnOnSettingInGame() { 
        GamePlayUICanvas.TurnOnSettingInGame();
    }
    public void TurnOffSettingInGame() { 
        GamePlayUICanvas.TurnOffSettingInGame();
    }



    public void TurnOnStuckPopup() { 
        GamePlayUICanvas.TurnOnStuckPopup();
    }
    public void TurnOffStuckPopup() { 
        GamePlayUICanvas.TurnOffStuckPopup();
    } 
}
