using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TQT;
using UnityEngine;
using Random = UnityEngine.Random;

public class GamePlayUIEffect : Singleton<GamePlayUIEffect>
{
    [SerializeField] Transform header, footer, mainCamera, starUI;
    [SerializeField] GameObject starEffect, canvasEffect, magicWandEffect, bigHammerEffect, freezeEffect, confetti;

    readonly int maxStar = 10;
    int currentStar;

    [SerializeField] Transform star, center;
    GameObject freezeEffectObj;

    public Transform Star { get => star; set => star = value; }
    public Transform Center { get => center; set => center = value; }
    public GameObject MagicWandEffect { get => magicWandEffect; set => magicWandEffect = value; }
    public GameObject BigHammerEffect { get => bigHammerEffect; set => bigHammerEffect = value; }
    public GameObject FreezeEffectObj { get => freezeEffectObj; set => freezeEffectObj = value; }

    private void OnEnable()
    {
        header.DOMoveY(header.position.y, 0.5f).From(header.position.y + 1f);
        footer.DOMoveY(footer.position.y, 0.5f).From(footer.position.y - 1f);
    }

    private void Start()
    {
      /*  GameObject canvasEf = Instantiate(canvasEffect,this.transform.parent);
        canvasEf.GetComponent<Canvas>().worldCamera = mainCamera.GetComponent<Camera>();
        star = canvasEf.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0);
        Center = canvasEf.transform.GetChild(1);*/
    }

    public void SpawnMagicWandEffect(Vector3 startPoint, Quaternion rotate)
    {
        Instantiate(magicWandEffect, startPoint, rotate);
    }

    public void SpawnStarEffect(Transform startPoint, float offsetStar)
    {
        if (TutorialControler.Instance.Tutorial_Level.IsCanTut) return;
        if(currentStar < maxStar)
        {
            ++currentStar;
            Vector3 startPos = startPoint.TransformPoint(new(0f, 0f, -1f));

            Vector3 offset = startPoint.TransformPoint(new(Random.Range(-offsetStar, offsetStar), Random.Range(-offsetStar, offsetStar), -1f));
            GameObject starVfx = Instantiate(starEffect, startPos, Quaternion.identity);

            float delayMoveOffset = Random.Range(0.25f, 0.5f);
            float delayMoveTarget = Random.Range(0.5f, 1f);

            starVfx.transform.DOMove(offset, delayMoveOffset).SetEase(Ease.OutSine).OnComplete(() =>
            {
                starVfx.transform.DOMove(star.position, delayMoveTarget).SetEase(Ease.InSine).OnComplete(() =>
                {
                    UIController.Instance.UpdateStarGamePlay(GameController.Instance.StarEarn);
                    if (currentStar > 0)
                        currentStar--;
                    Destroy(starVfx);
                });

            });

            //StartCoroutine(DestroyEffect(delayMoveTarget + delayMoveOffset, starVfx, () => currentStar--));
        }
        else
        {
            UIController.Instance.UpdateStarGamePlay(GameController.Instance.StarEarn);
        }
    }

    public void SpawnStarEffectUI(Transform startPoint, float offsetStar)
    {
        if (TutorialControler.Instance.Tutorial_Level.IsCanTut) return;

        if (currentStar < maxStar)
        {
            ++currentStar;
            Vector3 startPos = startPoint.TransformPoint(new(0f, 0f, -1f));
            Vector3 endPos = starUI.position;

            Vector3 offset = startPoint.TransformPoint(new(Random.Range(-offsetStar, offsetStar), Random.Range(-offsetStar, offsetStar), -1f));
            endPos.z = offset.z;

            GameObject starVfx = Instantiate(starEffect, startPos, Quaternion.identity);
            //starVfx.transform.localScale /= 2f;

            float delayMoveOffset = Random.Range(0.25f, 0.5f);
            float delayMoveTarget = Random.Range(0.5f, 1f);

            starVfx.transform.DOMove(offset, delayMoveOffset).SetEase(Ease.OutSine).OnComplete(() =>
            {
                UIController.Instance.UpdateStarGamePlay(GameController.Instance.StarEarn);
                if (currentStar > 0)
                    currentStar--;
                starVfx.transform.DOMove(endPos, delayMoveTarget).SetEase(Ease.InSine);
            });

            StartCoroutine(DestroyEffect(delayMoveTarget + delayMoveOffset, starVfx, () => currentStar--));
        }
    }

    IEnumerator DestroyEffect(float delay, GameObject obj, Action callback = null)
    {
        yield return new WaitForSeconds(delay);
        callback?.Invoke();
        Destroy(obj);
    }

    public void SpawnStarEffectUI(Transform startPoint, float offsetStar, Transform target)
    {
        if (TutorialControler.Instance.Tutorial_Level.IsCanTut) return;

        if (currentStar < maxStar)
        {
            ++currentStar;
            Vector3 startPos = startPoint.TransformPoint(new(0f, 0f, -1f));
            Vector3 endPos = target.position;

            Vector3 offset = startPoint.TransformPoint(new(Random.Range(-offsetStar, offsetStar), Random.Range(-offsetStar, offsetStar), -1f));
            endPos.z = offset.z;

            GameObject starVfx = Instantiate(starEffect, startPos, Quaternion.identity);
            starVfx.transform.localScale /= 1.5f;

            float delayMoveOffset = Random.Range(0.25f, 0.5f);
            float delayMoveTarget = Random.Range(0.5f, 1f);

            starVfx.transform.DOMove(offset, delayMoveOffset).SetEase(Ease.OutSine).OnComplete(() =>
            {
                UIController.Instance.UpdateStarGamePlay(GameController.Instance.StarEarn);
                if(currentStar > 0)
                    currentStar--;
                starVfx.transform.DOMove(endPos, delayMoveTarget).SetEase(Ease.InSine);
            });

            StartCoroutine(DestroyEffect(delayMoveTarget + delayMoveOffset, starVfx, () => currentStar--));
        }
    }

    public void SpawnBigHammerEffect(float duration)
    {
        StartCoroutine(SpawnBigHammerEffectSystem(duration));
    }

    IEnumerator SpawnBigHammerEffectSystem(float duration)
    {
        Vector3 pos = mainCamera.TransformPoint(Vector3.forward);
        GameObject effect = Instantiate(bigHammerEffect, pos, mainCamera.rotation);
        yield return new WaitForSeconds(0.75f);
        Destroy(effect);
    }

    public void SpawnFreezeEffect(float duration)
    {
        Vector3 pos = mainCamera.TransformPoint(Vector3.forward);
        freezeEffectObj = Instantiate(freezeEffect, pos, mainCamera.rotation);
        StartCoroutine(FadeInEffect(freezeEffectObj, 2f));
        //StartCoroutine(FadeOutEffect(freezeEffectObj, 2f, duration - 2f));

        //Invoke(nameof(HideFreezeEffect), duration);
    }

    IEnumerator FadeInEffect(GameObject effect, float duration)
    {
        SpriteRenderer spriteRenderer = effect.GetComponentInChildren<SpriteRenderer>();
        
        Color color = spriteRenderer.color;
        float maxAlpha = color.a;

        float currentAlpha = 0f;

        while(currentAlpha < maxAlpha)
        {
            currentAlpha += Time.deltaTime / duration * maxAlpha;
            color.a = currentAlpha;
            spriteRenderer.color = color;
            yield return null;
        }
    }

    IEnumerator FadeOutEffect(GameObject effect, float duration, float delay)
    {
        yield return new WaitForSeconds(delay);
        SpriteRenderer spriteRenderer = effect.GetComponentInChildren<SpriteRenderer>();

        Color color = spriteRenderer.color;
        float maxAlpha = color.a;

        float currentAlpha = maxAlpha;

        while (currentAlpha > 0f)
        {
            if (spriteRenderer == null) break;
            currentAlpha -= Time.deltaTime / duration * maxAlpha;
            color.a = currentAlpha;
            spriteRenderer.color = color;
            yield return null;
        }
        Destroy(effect);
    }

    public void HideFreezeEffect()
    {
        if(freezeEffectObj == null) return;
        StartCoroutine(FadeOutEffect(freezeEffectObj, 2f, 0f));
    }
    public void DesTroyFreezeEffect()
    {
        if (freezeEffectObj == null) return;
        Destroy(freezeEffectObj);
    }
    public void HideAllEffect()
    {
        HideFreezeEffect();
        //
    }

    public void SpawnConfetti(Quaternion rotation ,Transform transform)
    {
        Instantiate(confetti, Vector3.zero, rotation, transform);
    }
}
