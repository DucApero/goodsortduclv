using CustomAd;
using DVAH;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupWin : PopupUI
{
    [SerializeField] Huynn.ScrollController _scrollRank;
    [SerializeField] Button completedButton;
    [SerializeField] Button claimMore;
    [SerializeField] Transform leftSite, rightSite;
    [SerializeField] int baseRewardStar => LevelManager.Instance.CurrentStarEarn;
    [SerializeField] TMP_Text textClaimButton, valueClaimButton, valueClaimButtonNormal;
    [SerializeField] GameObject effectLeft, effectRight;
    [SerializeField] TextMeshProUGUI txtProgress;
    [SerializeField] Image sliderBar;
    float timeClose = 1.4f;

    bool blockClick;
    Animator animator;

    private int multipleReward = 2;
    public int MultipleReward
    {
        get => multipleReward;
        set => multipleReward = value;
    }
    public Transform LeftSite { get => leftSite; set => leftSite = value; }
    public Transform RightSite { get => rightSite; set => rightSite = value; }

    private int starEarn = 1;
    private void Awake()
    {
        Observer.Instance.AddObserver(ObserverKey.UpdateStar, UpdateProgress);
        completedButton.onClick.AddListener(() =>
        {
            SoundController.Instance.PlayClickButton();

            if (blockClick) return;
            blockClick = true;

            int totalStar = baseRewardStar;
            
            for (int i = 0; i < totalStar; i++)
            {
                GamePlayUIEffect.Instance.SpawnStarEffectUI(completedButton.transform, 200f, _scrollRank.playerItem.Star);
            }
            Invoke(nameof(Complete), timeClose);
            FireBaseManager.Instant.LogEventWithParameterAsync("win_btn_collect", new Hashtable {
                    {
                    "id_screen","gameplay"
                    }
            });
        });
        claimMore.onClick.AddListener(() =>
        {
            SoundController.Instance.PlayClickButton();
            if (blockClick) return;
            animator.enabled = false;

            CustomAdManager.Intance.ShowRewardAds(
                () => blockClick = true,
                () =>
                {
                    int totalStar = baseRewardStar * multipleReward;
                    for (int i = 0; i < totalStar; i++)
                    {
                        GamePlayUIEffect.Instance.SpawnStarEffectUI(claimMore.transform, 200f, _scrollRank.playerItem.Star);
                    }
                    if (LevelManager.Instance.IndexLevel < 1)
                    {
                        Invoke(nameof(CompleteWithMultiStar), 0);
                    }
                    else
                    {
                        Invoke(nameof(CompleteWithMultiStar), timeClose);
                    }
                    FireBaseManager.Instant.LogEventWithParameterAsync("win_btn_collectx5", new Hashtable {
                    {
                    "id_screen","gameplay"
                    }
            });
                }
                , () => blockClick = false);
        });
    }

    private void OnEnable()
    {
        base.OnEnable();
        FireBaseManager.Instant.LogEventWithParameterAsync("win_start", new Hashtable {
                    {
                    "id_screen","gameplay"
                    }
            });
        blockClick = false;
        if (animator != null) animator.enabled = true;

        //effectLeft.SetActive(true);
        //effectRight.SetActive(true);
        starEarn = LevelManager.Instance.CurrentStarEarn;
        valueClaimButtonNormal.text = starEarn.ToString();

        completedButton.gameObject.SetActive(false);
        claimMore.gameObject.SetActive(false);
     


        StartCoroutine(_scrollRank.waitCanvasUpdate(() =>
        {
            completedButton.gameObject.SetActive(true);
            claimMore.gameObject.SetActive(true);
        }));
        UpdateProgress(null);
        GameController.Instance.CurrentStar = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Star);
    }
    private void OnDestroy()
    {
        Observer.Instance.RemoveObserver(ObserverKey.UpdateStar, UpdateProgress);
    }
    private void UpdateProgress(object data)
    {
        int current = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Star);
        int max = GameController.Instance.GetConditionStarRewar();
        if (current > max)
        {
            txtProgress.text = "Full";
        }
        else
        {
            txtProgress.text = current.ToString() + " / " + max.ToString();
        }
        sliderBar.fillAmount = (float)current / (float)max;
    }
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void ActivePopup(bool isActive)
    {
        this.gameObject.SetActive(isActive);
        TQT.TutorialControler.Instance.GameEnd();

    }

    void ChangeMultiple(int multi)
    {
        MultipleReward = multi;
        textClaimButton.text = "Claim x" + multi;
        valueClaimButton.text = (starEarn * multi).ToString();
    }

    void Complete()
    {
        UserDatas.Instance.SetCurrency(CommonEnum.Currency.Star, starEarn);
        UserDatas.Instance.CurrentLevel++;
        GameController.Instance.CheckAndShowRewardStar(() =>
        {
            
            UIController.Instance.ShowpoupRewardPlay(()=> {  GameController.Instance.NextLevel(); }, true);
            
        });
        ActivePopup(false);
    }

    void CompleteWithMultiStar()
    {
        int star = starEarn * MultipleReward;
        UserDatas.Instance.SetCurrency(CommonEnum.Currency.Star, star);
        UserDatas.Instance.CurrentLevel++;
        GameController.Instance.CheckAndShowRewardStar(() =>
        {
            UIController.Instance.ShowpoupRewardPlay(() => { GameController.Instance.NextLevel(); }, true);
        });
        ActivePopup(false);
    }
}
