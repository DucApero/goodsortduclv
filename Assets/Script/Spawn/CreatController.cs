using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatController : Singleton<CreatController>
{
    public T CreateObjectGetComponent<T>(GameObject prefab, Vector3 pos, Transform parent = null, string tag = "") where T : MonoBehaviour
    {
        GameObject ob = Instantiate(prefab, pos, prefab.transform.rotation);
        if (parent != null)
            ob.transform.SetParent(parent, false);
        ob.name = prefab.name;
        T t = ob.gameObject.GetComponent<T>();
        return t;
    }
}

