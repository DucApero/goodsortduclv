using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StarChest : PopupUI
{
    [SerializeField] Image imgProgress;
    [SerializeField] TextMeshProUGUI txtProgress;
    [SerializeField] Button btnClose;
    private bool dontClick = false;
    private float timeDelay = 1.5f;
    private void Awake()
    {
        btnClose.onClick.AddListener(() =>
        {
            if (dontClick) return;
            if (!dontClick)
            {
                dontClick = true;
                StartCoroutine(GameController.Instance.DelayAction(() =>
                {
                    dontClick = false;
                }, timeDelay));
            }
            SoundController.Instance.PlayClickButton();
            mainUI.DOScale(0f, 0.5f).From(1f).SetEase(Ease.InBack).OnComplete(() =>
            {
                ActivePopup(false);
            });
        });
    }
    private void OnEnable()
    {
        base.OnEnable();
        UpdateProgress();
        dontClick = false;
    }
    public void ActivePopup(bool isActive)
    {
        this.gameObject.SetActive(isActive);
    }
    private void UpdateProgress()
    {
        int current = UserDatas.Instance.GetCurrency(CommonEnum.Currency.Star);
        int max = GameController.Instance.GetConditionStarRewar();
        if (current > max)
        {
            txtProgress.text = "Full";
        }
        else
        {
            txtProgress.text = current.ToString() + " / " + max.ToString();
        }
       
        imgProgress.fillAmount = (float)current / (float)max;
    }
}
