using CustomAd;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : ShopItemBase
{
    public ItemStatus itemStatus { get; set; }

    private Action<ShopItem> callbackSelectItem;
    public Action<ShopItem> CallbackSelectItem
    {
        get => callbackSelectItem;
        set => callbackSelectItem = value;
    }

    public int ItemType { get; set; }
    public int Cost { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        SetStatusItemUI();
        SetItemButton();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SetItemButton()
    {
        itemButton = GetComponent<Button>();
        itemButton.onClick.AddListener(OnClickItem);
        hubButton.onClick.AddListener(OnClickItem);
    }

    public override void OnClickItem()
    {
        SoundController.Instance.PlayClickButton();

        if (itemStatus == ItemStatus.unlocked)
        {
            OnClickUnLockedItem();
        }
        else if (itemStatus == ItemStatus.selected)
        {
            return;
        }
        else if (itemStatus == ItemStatus.lockByAds)
        {
            OnClickItemAds();
        }
        else if (itemStatus == ItemStatus.lockByGold)
        {
            OnClickItemGold();
        }
    }

    public void OnClickUnLockedItem()
    {
        callbackSelectItem?.Invoke(this);
        itemStatus = ItemStatus.selected;
        TurnOnOutline();
        UserDatas.Instance.SetItemIsSelected(id);
        BackgroundController.Instance.UpdateBackground();
    }

    public void UnSelectItem()
    {
        itemStatus = ItemStatus.unlocked;
        TurnOffOutline();
    }

    public void OnClickItemAds()
    {
        CustomAdManager.Intance.ShowRewardAds(null,
            () =>
            {
                if (UserDatas.Instance.GetWatchAdsCount(id) <= Cost)
                {
                    UserDatas.Instance.AddWatchAdsCount(id);
                    cost.text = UserDatas.Instance.GetWatchAdsCount(id).ToString() + "/" + Cost.ToString();
                }

                if (UserDatas.Instance.GetWatchAdsCount(id) >= Cost)
                {
                    itemStatus = ItemStatus.unlocked;
                    UserDatas.Instance.AddItemOwned(id);
                    OnClickUnLockedItem();
                    UnLockItem();
                    UserDatas.Instance.SetItemIsSelected(id);
                }
            }, null);
    }

    public void UnLockItem()
    {
        hubItem.enabled = false;
        typeCostIcon.enabled = false;
        //outLineItem.enabled = false;
        cost.enabled = false;
    }

    public void OnClickItemGold()
    {
        /*if(SessionPref.GetMoney() - Cost >= 0)
        {
            SessionPref.AddMoney(-(Cost));
            itemStatus = ItemStatus.unlocked;
            TurnOnOutline();
            OnClickUnLockedItem();
        }
        else
        {
            return;
        }*/
        if (UserDatas.Instance.GetCurrency(CommonEnum.Currency.Gold) - Cost >= 0)
        {
            UserDatas.Instance.SetCurrency(CommonEnum.Currency.Gold, -Cost);
            itemStatus = ItemStatus.unlocked;
            UserDatas.Instance.AddItemOwned(id);
            OnClickUnLockedItem();
            UnLockItem();
            UserDatas.Instance.SetItemIsSelected(id);
        }
        else
        {
            UIController.Instance.NotEnoughCoin();
        }

    }

    public void SetHubUI()
    {
        if (ItemType == 0)
        {

        }
        if (ItemType == 1)
        {
            hubItem.sprite = hubAds;
            typeCostIcon.sprite = iconTypeAds;
        }
        else if (ItemType == 2)
        {
            hubItem.sprite = hubGold;
            typeCostIcon.sprite = iconTypeGold;
        }
    }

    public void SetStatusItemUI()
    {

        if (UserDatas.Instance.GetOwnedItem().Contains(id))
        {
            itemStatus = ItemStatus.unlocked;
            Debug.Log("contain: " + id);
            Debug.Log("chua: " + UserDatas.Instance.GetOwnedItem().Count);
            if (id == UserDatas.Instance.GetItemIsSelected())
            {
                itemStatus = ItemStatus.selected;
                Debug.Log("select: " + id);
            }
        }
        else
        {

            switch (ItemType)
            {
                case 1:
                    itemStatus = ItemStatus.lockByAds;
                    break;
                case 2:
                    itemStatus = ItemStatus.lockByGold;
                    break;
            }
        }

        switch (itemStatus)
        {
            case ItemStatus.unlocked:
                hubItem.gameObject.SetActive(false);
                TurnOffOutline();
                break;
            case ItemStatus.selected:
                hubItem.gameObject.SetActive(false);
                TurnOnOutline();
                break;
            case ItemStatus.lockByAds:
                cost.text = UserDatas.Instance.GetWatchAdsCount(id).ToString() + "/" + Cost.ToString();
                TurnOffOutline();
                break;
            case ItemStatus.lockByGold:
                cost.text = Cost.ToString();
                TurnOffOutline();
                break;
        }
        SetHubUI();
    }

    public void SetSkinItem(SkinContainer backGround)
    {
        this.gameObject.GetComponent<Image>().sprite = backGround.listBackground[id].iconBg;
    }
}

public enum ItemStatus
{
    unlocked,
    selected,
    lockByAds,
    lockByGold
}
