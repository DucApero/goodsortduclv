using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ConditionCombo", menuName = "GameConfiguration/ConditionCombo", order = 1)]
public class ConditionCombo : ScriptableObject
{
    public List<Combo> ListCombo;
}

[System.Serializable]
public class Combo
{
    public int id;
    public int star;
    public float time;
}
