using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ProgressBar : MonoBehaviour
{
    public float countdownDuration = 60.0f;
    public Action ActionDone
    {
        get => actionDone;
        set => actionDone = value;
    }
    private Action actionDone;
    private float currentTime;
    [SerializeField] private TextMeshProUGUI txtCombo;
    private Vector3 vtScale = new Vector3(1.35f,1.35f,1.35f);
    private float timeScale = 0.2f;
    public float CurrentTime 
    {
        get => currentTime;
        set
        {
           
            countdownDuration = value;
            currentTime = value;
            if(countdownDuration > 0)
            {
                slider.fillAmount = currentTime / countdownDuration;
                txtCombo.text = "Combo x" + (GameController.Instance.CountCombo).ToString();
                txtCombo.gameObject.transform.DOScale(vtScale, timeScale).OnComplete(()=> 
                {
                    txtCombo.gameObject.transform.DOScale(Vector3.one, timeScale);
                });
            }
            else
            {
                ResetSlider();
            }
            
        }
    }
    [SerializeField] private Image slider;
/*    private bool isStopCount;
    public bool IsStopCount
    {
        get => isStopCount;
        set => isStopCount = value;
    }*/

    public void Awake()
    {
        Observer.Instance.AddObserver(ObserverKey.WinLevel, WinLevel);
        Observer.Instance.AddObserver(ObserverKey.LoseLevel, LoseLevel);
    }

    public void OnDestroy()
    {
        Observer.Instance.RemoveObserver(ObserverKey.WinLevel, WinLevel);
        Observer.Instance.RemoveObserver(ObserverKey.LoseLevel, LoseLevel);
    }

    void Start()
    {
        currentTime = countdownDuration;
    }

    public void WinLevel(object data)
    {
        ResetSlider();
        //isStopCount = true;
    }

    public void LoseLevel(object data)
    {
        //ResetSlider();
        //isStopCount = true;
    }

    void Update()
    {
        if (!GameController.Instance.IsStartLevel) return;
        
        if (currentTime > 0)
        {
            currentTime -= Time.deltaTime;
            slider.fillAmount = currentTime / countdownDuration;
        }
        else
        {
            actionDone?.Invoke();
            actionDone = null;
        }
    }
    public void ResetSlider()
    {
        slider.fillAmount = 0;
        if(txtCombo)
        txtCombo.text = "";
        //isStopCount = false;
    }
}
