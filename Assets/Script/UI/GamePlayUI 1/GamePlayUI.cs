using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEnum;
using UnityEngine.UI;
using TMPro;
using DVAH;

public class GamePlayUI : MonoBehaviour
{
    [SerializeField] private LevelDisplay levelDisplay;
    [SerializeField] private ConfigGameController settingInGamePlay;
    [SerializeField] private RefreshPopup refreshPopup;
    [SerializeField] private StuckPopup stuckPopup;
    [SerializeField] private PopUpLose PopUpLose;
    [SerializeField] private PopupWin PopupWin;
    [SerializeField] UnLockItem unLockItem;
    [SerializeField] Button btnNoAds;
    [SerializeField] Button btnSettings;
    [SerializeField] StarHub starHub;
    [SerializeField] private BuyBoosterInGame BuyBoosterPopup;
    [SerializeField] private DrumButton hintButton;
    [SerializeField] private MagicWandButton changeButton;
    [SerializeField] private FreezerButton freezerButton;
    [SerializeField] private RefreshButton refreshButton; 
    [SerializeField] private Button addHintButton;
    [SerializeField] private Button addChangeButton;
    [SerializeField] private Button addFreezerButton;
    [SerializeField] private Button addRefreshButton;
    [SerializeField] private ProgressBar progressBar;
    [SerializeField] GameObject ui_Bottom, ui_Head;

    [SerializeField] GameObject effectLeft, effectRight;
    public DrumButton DrumButton => hintButton;
    public MagicWandButton MagicWand => changeButton;
    public FreezerButton FreezerButton => freezerButton;
    public RefreshButton RefreshButton => refreshButton;

    public StuckPopup StuckPopup => stuckPopup;

    public BuyBoosterInGame BuyBoosterInGame => BuyBoosterPopup;
    public UnLockItem UnLockItem => unLockItem;
    private ShopQuyNX.ItemData itemData;
    [SerializeField] private List<ItemBooster_GamePlay> lstItemBooster = new List<ItemBooster_GamePlay>();
    private void Awake()
    {
        itemData = GameController.Instance.ItemDataGamePlay;
        btnNoAds.onClick.AddListener(()=> 
        {
            if (LevelManager.Instance.IsWinLevel) return;
            if (LevelManager.Instance.IsLoseLevel) return;
            SoundController.Instance.PlayClickButton();
            TurnOnNoAds();
        });
        btnSettings.onClick.AddListener(()=> 
        {
            if (LevelManager.Instance.IsWinLevel) return;
            if (LevelManager.Instance.IsLoseLevel) return;
            SoundController.Instance.PlayClickButton();
            ActivePopupSettings(true);
            FireBaseManager.Instant.LogEventWithParameterAsync("setting_start", new Hashtable {
                {
                    "id_screen","gameplay"
                }
        });
        });
        hintButton.DrumButtonn.onClick.AddListener(OnClickHintButton);
        addHintButton.onClick.AddListener(OnClickHintButton);
        changeButton.MagicwandButton.onClick.AddListener(OnClickChangeButton);
        addChangeButton.onClick.AddListener(OnClickChangeButton);
        freezerButton.FreezerButtonn.onClick.AddListener(OnClickFreezerButton);
        addFreezerButton.onClick.AddListener(OnClickFreezerButton);
        refreshButton.RefreshButtonn.onClick.AddListener(OnClickRefreshButton);
        addRefreshButton.onClick.AddListener(OnClickRefreshButton);
    }

    private void OnEnable()
    {
        SoundController.Instance.PlayBackgroundGameplay();
        SetInfoItem();
        FireBaseManager.Instant.LogEventWithParameterAsync("gameplay_start", new Hashtable {
                {
                    "id_screen","gameplay"
                }
            });
    }
    public void BuyNoAds()
    {
        btnNoAds.gameObject.SetActive(false);
    }
    public void UpdateStar(int value)
    {
        starHub.AddStarFake(value);
    }
    public void ResetStarHub()
    {
        starHub.UpdateCurrentStar();
    }
    public void Start()
    {
        Observer.Instance.AddObserver(ObserverKey.UpdateCountBooster, UpdateCountBooster);
        Debug.Log("number magic: " + UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.MagicWand));
    }
    public void OnDestroy()
    {
        Observer.Instance.RemoveObserver(ObserverKey.UpdateCountBooster, UpdateCountBooster);
    }

    private void UpdateCountBooster(object data)
    {
        hintButton.UpdateCountBooster();
        freezerButton.UpdateCountBooster();
        refreshButton.UpdateCountBooster();
        changeButton.UpdateCountBooster();
    }

    public void OnClickHintButton()
    {
        
        if (LevelManager.Instance.IsLoseLevel || LevelManager.Instance.IsWinLevel||
            hintButton.ItemBooster_GamePlay.isLock) return;
        int numberMatchItem = UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.SmallHammer);
        if(numberMatchItem <= 0)
        {
            BuyBoosterPopup.SetUpPopup(ItemGameplay.SmallHammer);
            BuyBoosterPopup.gameObject.SetActive(true);
        }
        else
        {
            GameController.Instance.IsStartLevel = true;
            LevelManager.Instance.AutoMatchItem();
            UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.SmallHammer, -1);
            hintButton.UpdateCountBooster();
            LevelManager.Instance.CurrentLevel.UseHint++;
        }
    }

    public void OnClickChangeButton()
    {
        if (LevelManager.Instance.IsLoseLevel || LevelManager.Instance.IsWinLevel
            || changeButton.ItemBooster_GamePlay.isLock) return;
        int numberMagicWand = UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.MagicWand);
        if (numberMagicWand <= 0)
        {
            BuyBoosterPopup.SetUpPopup(ItemGameplay.MagicWand);
            BuyBoosterPopup.gameObject.SetActive(true);
        }
        else
        {
            GameController.Instance.IsStartLevel = true;
            LevelManager.Instance.OnClickMagicWand();
            UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.MagicWand, -1);
            changeButton.UpdateCountBooster();
        }
        LevelManager.Instance.CurrentLevel.UseMagicWand++;
    }

    public void OnClickFreezerButton()
    {
        if (LevelManager.Instance.IsLoseLevel || LevelManager.Instance.IsWinLevel
            ||freezerButton.ItemBooster_GamePlay.isLock) return;
        int freezerNumber = UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.Freeze);
        if(freezerNumber <= 0)
        {
            BuyBoosterPopup.SetUpPopup(ItemGameplay.Freeze);
            BuyBoosterPopup.gameObject.SetActive(true);
        }
        else
        {
            GameController.Instance.IsStartLevel = true;
            GameController.Instance.SetTimeFree();
            UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Freeze, -1);
            freezerButton.UpdateCountBooster();
            LevelManager.Instance.CurrentLevel.UseFreezer++;
        }
    }
    public void UpdateValueProgressBar()
    {
        progressBar.CurrentTime = GameController.Instance.GetTimeCombo();
        progressBar.ActionDone = () => 
        {
            GameController.Instance.ResetCombo();
            ResetSliderCombo();
        };
       
    }

    public void ResetSliderCombo()
    {
        progressBar.ResetSlider();
    }
    public void OnClickRefreshButton()
    {
        if (LevelManager.Instance.IsLoseLevel || LevelManager.Instance.IsWinLevel
            || refreshButton.ItemBooster_GamePlay.isLock) return;
        int numberRefresh = UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.Refresh);
        if (numberRefresh <= 0)
        {
            BuyBoosterPopup.SetUpPopup(ItemGameplay.Refresh);
            BuyBoosterPopup.gameObject.SetActive(true);
        }
        else
        {

            GameController.Instance.IsStartLevel = true;
            LevelManager.Instance.SortItem();
            UserDatas.Instance.AddItem(CommonEnum.ItemGameplay.Refresh, -1);
            refreshButton.UpdateCountBooster();
            LevelManager.Instance.CurrentLevel.UseRefresh++;
        }
    }
    public void ActiveEffecWin()
    {
        effectLeft.SetActive(true);
        effectRight.SetActive(true);
    }
    public void ActivePopupSettings(bool isActive)
    {
        settingInGamePlay.ActivePopup(isActive);
    }
    public void ActivePopupWin(bool isActive)
    {
        PopupWin.ActivePopup(isActive);
    }
    public void ActivePopuplose(bool isActive)
    {
        Debug.Log("ActivePopuplose");
        PopUpLose.ActivePopup(isActive);
    }
    public void UpdateLevelText()
    {
        levelDisplay.UpdateLevelText();
    }

    public void TurnOnSettingInGame() => settingInGamePlay.gameObject.SetActive(true);
    public void TurnOffSettingInGame() => settingInGamePlay.gameObject.SetActive(false);

    public void TurnOnNoAds()
    {
        UIController.Instance.ActivePopupAds(true);
    }

    public void TurnOnRefreshPopup() => refreshPopup.gameObject.SetActive(true);
    public void TurnOffRefreshPopup() => refreshPopup.gameObject.SetActive(false);

    public void TurnOnStuckPopup() => stuckPopup.gameObject.SetActive(true);
    public void TurnOffStuckPopup() => stuckPopup.gameObject.SetActive(false);

    public void Set_Object(bool show)
    {
        ui_Bottom.SetActive(show);
        ui_Head.SetActive(show);
    }
    public void SetInfoItem()
    {

        foreach (ShopQuyNX.ItemData.Item item_data in itemData.items)
        {
            if (item_data.itemId == CommonEnum.ItemGameplay.SmallHammer)
            {
                lstItemBooster[0].SetInfo(item_data);
            }

            if (item_data.itemId == CommonEnum.ItemGameplay.Refresh)
            {
                lstItemBooster[1].SetInfo(item_data);
            }
            if (item_data.itemId == CommonEnum.ItemGameplay.MagicWand)
            {
                lstItemBooster[2].SetInfo(item_data);
            }
            if (item_data.itemId == CommonEnum.ItemGameplay.Freeze)
            {
                lstItemBooster[3].SetInfo(item_data);
            }
        }

    }
}
