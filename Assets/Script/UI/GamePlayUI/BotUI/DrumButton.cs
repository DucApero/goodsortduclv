using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DrumButton : MonoBehaviour
{
    [SerializeField] protected Button drumButton;
    public Button DrumButtonn
    {
        get => drumButton;
        set => drumButton = value;
    }
    [SerializeField] protected TextMeshProUGUI drumCount;
    [SerializeField] protected Image addIcon;
    [SerializeField] ItemBooster_GamePlay itemBooster_GamePlay;
    public ItemBooster_GamePlay ItemBooster_GamePlay => itemBooster_GamePlay;
    // Start is called before the first frame update
    void Start()
    {
        UpdateCountBooster();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateCountBooster()
    {
        if (!itemBooster_GamePlay.isLock)
        {
            int numberBooster = UserDatas.Instance.GetCurrentItemData(CommonEnum.ItemGameplay.SmallHammer);
            if (numberBooster > 0)
            {
                drumCount.gameObject.SetActive(true);
                drumCount.text = numberBooster.ToString();
                addIcon.gameObject.SetActive(false);
            }
            else
            {
                drumCount.gameObject.SetActive(false);
                addIcon.gameObject.SetActive(true);
            }
        }
        else
        {
            drumCount.gameObject.SetActive(false);
            addIcon.gameObject.SetActive(false);
        }
    }
}
