using CommonEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CurrentItemData
{
    public ItemGameplay id;
    public int amount;
}
